{!include file="inc/header.tpl"!}

<script type="text/javascript">
    //Ext.require(['*']);

    Ext.onReady(function() {
    
        //var ajax_url = 'http://love.sinaapp.com/admin.php/';

        Ext.QuickTips.init();

        // NOTE: This is an example showing simple state management. During development,
        // it is generally best to disable state management as dynamically-generated ids
        // can change across page loads, leading to unpredictable results.  The developer
        // should ensure that stable state ids are set for stateful components in real apps.
        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider'));

//        //tree列表
//        var tree = Ext.create('Ext.data.TreeStore', {
//            root: {
//                expanded: true,
//                children: [
//                    { text: "detention", leaf: true },
//                    { text: "homework", expanded: true, children: [
//                        { text: "book report", leaf: true },
//                        { text: "alegrbra", leaf: true}
//                    ] },
//                    { text: "buy lottery tickets", leaf: true}
//                ]
//            }
//        });

        //通过ajax将tree的数组查出来
        var tree = Ext.create('Ext.tree.TreePanel', {
            id: 'treemenu',
            border: 'false false',
            title: '模块',
            rootVisible: false,
            lines: true,
            scroll: 'both',
            //baseCls: 'x-panel-bwrap',
            hideHeaders: true,
            bodyStyle: 'border-bottom:1px solid #99bbe8',
            store: Ext.create('Ext.data.TreeStore',{
                proxy: {
                    type: 'ajax',
                    url: a_global.a_weburl+'welcome/tree'
                }
            })
        });

        //创建主面板
        var viewport = Ext.create('Ext.Viewport', {
            id: 'border-example',
            layout: 'border',
            items: [
            // create instance immediately
            Ext.create('Ext.Component', {
                region: 'north',
                height: 32, // give north and south regions a height
                autoEl: {
                    tag: 'div',
                    html:'<p>north - generally for menus, toolbars and/or advertisements</p>'
                }
            }), {
                // lazily created panel (xtype:'panel' is default)
                region: 'south',
                contentEl: 'south',
                split: true,
                height: 100,
                minSize: 100,
                maxSize: 200,
                collapsible: true,
                collapsed: true,
                title: 'South',
                margins: '0 0 0 0'
            },  {
                region: 'west',
                stateId: 'navigation-panel',
                id: 'west-panel', // see Ext.getCmp() below
                title: 'West',
                split: true,
                width: 200,
                minWidth: 175,
                maxWidth: 400,
                collapsible: true,
                animCollapse: true,
                margins: '0 0 0 5',
                layout: 'accordion',
                items: [tree
                //{
//                    contentEl: 'west',
//                    title: 'Navigation',
//                    iconCls: 'nav' // see the HEAD section for style used
//                }, {
//                    title: 'Settings',
//                    html: '<p>Some settings in here.</p>',
//                    iconCls: 'settings'
//                }, {
//                    title: 'Information',
//                    html: '<p>Some info in here.</p>',
//                    iconCls: 'info'
//                }, 
//                treeObj = Ext.create('Ext.tree.Panel', {
//                    store: store,
//                    hideHeaders: true,
//                    rootVisible: true,
//                    viewConfig: {
//                        plugins: [{
//                            ptype: 'treeviewdragdrop'
//                        }]
//                    },
//                    height: 350,
//                    width: 400,
//                    title: 'Directory Listing',
////                    renderTo: 'tree-example',
//                    collapsible: true
//                })
                ]
            },
            // in this instance the TabPanel is not wrapped by another panel
            // since no title is needed, this Panel is added directly
            // as a Container
            tabs = Ext.create('Ext.tab.Panel', {
                region: 'center', // a center region is ALWAYS required for border layout
                deferredRender: false,
                activeTab: 0,     // first tab initially active
                items: [{
                    contentEl: 'center1',
                    title: 'Close Me',
                    closable: true,
                    autoScroll: true
                }, {
                    contentEl: 'center2',
                    title: 'Center Panel',
                    autoScroll: true
                }]
            })]
        });

        // get a reference to the HTML element with id "hideit" and add a click listener to it
        Ext.get("hideit").on('click', function(){
            // get a reference to the Panel that was created with id = 'west-panel'
            var w = Ext.getCmp('west-panel');
            // expand or collapse that Panel based on its collapsed property state
            w.collapsed ? w.expand() : w.collapse();
        });


        //add tab 按钮
//        Ext.create('Ext.button.Button', {
//            text    : 'New tab asdasd',
//            scope   : this,
//            handler : function() {
//                var tab = tabs.add({
//                    // we use the tabs.items property to get the length of current items/tabs
//                    title: 'Tab ' + (tabs.items.length + 1),
//                    html : 'Another one',
//                    closable: true
//                });
//
//                tabs.setActiveTab(tab);
//            },
//            renderTo : Ext.getBody()
//        });

        //点击tree的各项，来创建tab 
        tree.on("itemclick",function(e, args) { 
//alert(args.raw.url);
            if (args.isLeaf()) {    //判断不是文件夹就创建
                var url = args.raw.url;
                var tabName = args.raw.text;
                var id = args.raw.id;
                var tabId = '#'+id;
                var itab = tabs.child(tabId);   //todo:这里是判断tab是否存在，如果存在的话，就直接显示

                if (itab) {
                    itab.show();

                } else {

                    var itab = tabs.add({
                        // we use the tabs.items property to get the length of current items/tabs    (tabs.items.length + 1)
                        title: tabName,
                        html : '<iframe src="'+a_global.a_weburl+url+'" width="100%" height="100%">frame:</iframe>',
                        id: id,
                        closable: true
                    });
                    //在创建tab之后,设置选中新创建的tab
                    tabs.setActiveTab(itab);
                }
            }
        });

        //定义的数据字段
        Ext.define('users', {
            extend: 'Ext.data.Model',
            fields: ['id', 'name']
        });

        Ext.define('System.Grid', {
            extend: 'Ext.grid.Panel',
            alias: 'widget.writergrid',
            requires: [
            'Ext.form.field.Text',
            'Ext.toolbar.TextItem',
            'Ext.toolbar.Paging',
            'Ext.data.*'
            ],
            initComponent: function(){

                Ext.apply(this, {
                    iconCls: 'icon-grid',
                    frame: true,
                    columns: [
                    {
                        header: 'test id ',
                        sortable: false,
                        dataIndex: 'id',
                        flex: 1
                    },{
                        header: 'test name',
                        dataIndex: 'name',
                        flex: 1
                    }
                    ],
                    selModel: new Ext.selection.CheckboxModel(),
                    columnLines: true   //为true是，复选框只能选一行
                });
                this.callParent();
                this.getSelectionModel().on('selectionchange', this.onSelectChange, this);
            }
            
        });

        //ajax加载数据
        store = Ext.create('Ext.data.Store', {
            model: 'users',
            autoLoad: true,
            autoSync: true,
            params: {
                start:0,
                limit:25
            },
            proxy: {
                type: 'ajax',
                api: {
                    read: a_global.a_weburl+'welcome/datas'
                },
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    successProperty: 'success',
                    root: 'data',
                    messageProperty: 'message'
                },
                listeners: {
                    exception: function(proxy, response, operation){
                        Ext.MessageBox.show({
                            title: '错误信息',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            }
        });

        //将数组展示
        var itemsPerPage = 25;
        main = Ext.create('Ext.container.Container', {
            layout:'fit',
            renderTo: 'center1',
            items: [{
                itemId: 'grid',
                xtype: 'writergrid',
                title: '统计',
                flex: 1,
                store: store,
                bbar: Ext.create('Ext.PagingToolbar', {
                    store: store,
                    displayInfo: true,
                    displayMsg: '显示 {0} - {1} of {2}',
                    emptyMsg: "无数据显示"
                }),
                listeners: {
                    selectionchange: function(selModel, selected) {
                    }
                }
            }]
        });

});



</script>


<body id="b">
    <!-- use class="x-hide-display" to prevent a brief flicker of the content -->
    <div id="west" class="x-hide-display">
        <p>Hi. I'm the west panel. -kxd : extjs4 </p>
    </div>
    <div id="center2" class="x-hide-display">
        <a id="hideit" href="#">Toggle the west region</a>
        <p>My closable attribute is set to false so you can't close me. The other center panels can be closed.</p>
        <p>The center panel automatically grows to fit the remaining space in the container that isn't taken up by the border regions.</p>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed metus nibh, sodales a, porta at, vulputate eget, dui. Pellentesque ut nisl. Maecenas tortor turpis, interdum non, sodales non, iaculis ac, lacus. Vestibulum auctor, tortor quis iaculis malesuada, libero lectus bibendum purus, sit amet tincidunt quam turpis vel lacus. In pellentesque nisl non sem. Suspendisse nunc sem, pretium eget, cursus a, fringilla vel, urna. Aliquam commodo ullamcorper erat. Nullam vel justo in neque porttitor laoreet. Aenean lacus dui, consequat eu, adipiscing eget, nonummy non, nisi. Morbi nunc est, dignissim non, ornare sed, luctus eu, massa. Vivamus eget quam. Vivamus tincidunt diam nec urna. Curabitur velit. Quisque dolor magna, ornare sed, elementum porta, luctus in, leo.</p>
        <p>Donec quis dui. Sed imperdiet. Nunc consequat, est eu sollicitudin gravida, mauris ligula lacinia mauris, eu porta dui nisl in velit. Nam congue, odio id auctor nonummy, augue lectus euismod nunc, in tristique turpis dolor sed urna. Donec sit amet quam eget diam fermentum pharetra. Integer tincidunt arcu ut purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla blandit malesuada odio. Nam augue. Aenean molestie sapien in mi. Suspendisse tincidunt. Pellentesque tempus dui vitae sapien. Donec aliquam ipsum sit amet pede. Sed scelerisque mi a erat. Curabitur rutrum ullamcorper risus. Maecenas et lorem ut felis dictum viverra. Fusce sem. Donec pharetra nibh sit amet sapien.</p>
        <p>Aenean ut orci sed ligula consectetuer pretium. Aliquam odio. Nam pellentesque enim. Nam tincidunt condimentum nisi. Maecenas convallis luctus ligula. Donec accumsan ornare risus. Vestibulum id magna a nunc posuere laoreet. Integer iaculis leo vitae nibh. Nam vulputate, mauris vitae luctus pharetra, pede neque bibendum tellus, facilisis commodo diam nisi eget lacus. Duis consectetuer pulvinar nisi. Cras interdum ultricies sem. Nullam tristique. Suspendisse elementum purus eu nisl. Nulla facilisi. Phasellus ultricies ullamcorper lorem. Sed euismod ante vitae lacus. Nam nunc leo, congue vehicula, luctus ac, tempus non, ante. Morbi suscipit purus a nulla. Sed eu diam.</p>
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras imperdiet felis id velit. Ut non quam at sem dictum ullamcorper. Vestibulum pharetra purus sed pede. Aliquam ultrices, nunc in varius mattis, felis justo pretium magna, eget laoreet justo eros id eros. Aliquam elementum diam fringilla nulla. Praesent laoreet sapien vel metus. Cras tempus, sapien condimentum dictum dapibus, lorem augue fringilla orci, ut tincidunt eros nisi eget turpis. Nullam nunc nunc, eleifend et, dictum et, pharetra a, neque. Ut feugiat. Aliquam erat volutpat. Donec pretium odio nec felis. Phasellus sagittis lacus eget sapien. Donec est. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
        <p>Vestibulum semper. Nullam non odio. Aliquam quam. Mauris eu lectus non nunc auctor ullamcorper. Sed tincidunt molestie enim. Phasellus lobortis justo sit amet quam. Duis nulla erat, varius a, cursus in, tempor sollicitudin, mauris. Aliquam mi velit, consectetuer mattis, consequat tristique, pulvinar ac, nisl. Aliquam mattis vehicula elit. Proin quis leo sed tellus scelerisque molestie. Quisque luctus. Integer mattis. Donec id augue sed leo aliquam egestas. Quisque in sem. Donec dictum enim in dolor. Praesent non erat. Nulla ultrices vestibulum quam.</p>
        <p>Duis hendrerit, est vel lobortis sagittis, tortor erat scelerisque tortor, sed pellentesque sem enim id metus. Maecenas at pede. Nulla velit libero, dictum at, mattis quis, sagittis vel, ante. Phasellus faucibus rutrum dui. Cras mauris elit, bibendum at, feugiat non, porta id, neque. Nulla et felis nec odio mollis vehicula. Donec elementum tincidunt mauris. Duis vel dui. Fusce iaculis enim ac nulla. In risus.</p>
        <p>Donec gravida. Donec et enim. Morbi sollicitudin, lacus a facilisis pulvinar, odio turpis dapibus elit, in tincidunt turpis felis nec libero. Nam vestibulum tempus ipsum. In hac habitasse platea dictumst. Nulla facilisi. Donec semper ligula. Donec commodo tortor in quam. Etiam massa. Ut tempus ligula eget tellus. Curabitur id velit ut velit varius commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla facilisi. Fusce ornare pellentesque libero. Nunc rhoncus. Suspendisse potenti. Ut consequat, leo eu accumsan vehicula, justo sem lobortis elit, ac sollicitudin ipsum neque nec ante.</p>
        <p>Aliquam elementum mauris id sem. Vivamus varius, est ut nonummy consectetuer, nulla quam bibendum velit, ac gravida nisi felis sit amet urna. Aliquam nec risus. Maecenas lacinia purus ut velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sit amet dui vitae lacus fermentum sodales. Donec varius dapibus nisl. Praesent at velit id risus convallis bibendum. Aliquam felis nibh, rutrum nec, blandit non, mattis sit amet, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam varius dignissim nibh. Quisque id orci ac ante hendrerit molestie. Aliquam malesuada enim non neque.</p>
    </div>
    <div id="center1" class="x-hide-display">
        <p><b>Done reading me? Close me by clicking the X in the top right corner.</b></p>
        <p>Vestibulum semper. Nullam non odio. Aliquam quam. Mauris eu lectus non nunc auctor ullamcorper. Sed tincidunt molestie enim. Phasellus lobortis justo sit amet quam. Duis nulla erat, varius a, cursus in, tempor sollicitudin, mauris. Aliquam mi velit, consectetuer mattis, consequat tristique, pulvinar ac, nisl. Aliquam mattis vehicula elit. Proin quis leo sed tellus scelerisque molestie. Quisque luctus. Integer mattis. Donec id augue sed leo aliquam egestas. Quisque in sem. Donec dictum enim in dolor. Praesent non erat. Nulla ultrices vestibulum quam.</p>
        <p>Duis hendrerit, est vel lobortis sagittis, tortor erat scelerisque tortor, sed pellentesque sem enim id metus. Maecenas at pede. Nulla velit libero, dictum at, mattis quis, sagittis vel, ante. Phasellus faucibus rutrum dui. Cras mauris elit, bibendum at, feugiat non, porta id, neque. Nulla et felis nec odio mollis vehicula. Donec elementum tincidunt mauris. Duis vel dui. Fusce iaculis enim ac nulla. In risus.</p>
        <p>Donec gravida. Donec et enim. Morbi sollicitudin, lacus a facilisis pulvinar, odio turpis dapibus elit, in tincidunt turpis felis nec libero. Nam vestibulum tempus ipsum. In hac habitasse platea dictumst. Nulla facilisi. Donec semper ligula. Donec commodo tortor in quam. Etiam massa. Ut tempus ligula eget tellus. Curabitur id velit ut velit varius commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla facilisi. Fusce ornare pellentesque libero. Nunc rhoncus. Suspendisse potenti. Ut consequat, leo eu accumsan vehicula, justo sem lobortis elit, ac sollicitudin ipsum neque nec ante.</p>
        <p>Aliquam elementum mauris id sem. Vivamus varius, est ut nonummy consectetuer, nulla quam bibendum velit, ac gravida nisi felis sit amet urna. Aliquam nec risus. Maecenas lacinia purus ut velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse sit amet dui vitae lacus fermentum sodales. Donec varius dapibus nisl. Praesent at velit id risus convallis bibendum. Aliquam felis nibh, rutrum nec, blandit non, mattis sit amet, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam varius dignissim nibh. Quisque id orci ac ante hendrerit molestie. Aliquam malesuada enim non neque.</p>
    </div>
    <div id="props-panel" class="x-hide-display" style="width:200px;height:200px;overflow:hidden;">
            <!-- class:x-hide-display ext在页面布局的使用用，默认是隐藏的，ext会将布局的每一块放到x-hide-display，然后显示 -->

    </div>
    <div id="south" class="x-hide-display">
        <p>south - generally for informational stuff, also could be for status bar</p>
    </div>

</body>
</html>