<?php
require_once('CommonCrud.php');
/**
 * 获取商品模型
 *
 */
class goodsmodel extends CommonCrud {
    private $labels = 'labels';

    public function __construct() {
        parent::__construct();
    }

    /**
     * 商品标签
     *
     */
    public function labels($labels) {
        //先查出有没有这些标签，有的话返回，没用的的话插入
        if(!is_array($labels)) {
            return false;
        }

        $this->db->where_in('label',$labels);
        $rs = $this->db->get($this->labels);
        $rs_label = $rs->result_array();

        foreach($rs_label as $key=>$value) {
            $have_label[] = $value['label'];
            $have_label_id[] = $value['l_id'];
        }

        if(isset($have_label) && !empty($have_label)) {
            foreach($labels as $k=>$v) {
                if(in_array($v,$have_label)) {
                    unset($labels[$k]);
                }
            }
        }

        if(count($labels) > 0) {
            foreach($labels as $key=>$value) {
                $this->db->insert($this->labels, array('label'=>trim($value)));
                $have_label_id[] = $this->db->insert_id();
            }
        }

        return $have_label_id;
    }

    /**
     * 商品批量抓取日志
     *
     */
    public function getGoodsLog($labels) {

    }


}
?>