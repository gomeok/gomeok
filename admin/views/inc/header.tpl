<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

{!config_load file="web.inc.conf"!}

    <script type="text/javascript" src="{!#ajs_path#!}jquery-1.5.2.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{!#ajs_path#!}extjs4/resources/css/ext-all.css">
    <script type="text/javascript" src="{!#ajs_path#!}extjs4/bootstrap.js"></script>
    <script type="text/javascript" src="{!#ajs_path#!}common.js"></script>

    <title>extjs - admin</title>
</head>