<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>lovego</title>

<link href="{!#css_path#!}index.css" rel="stylesheet" type="text/css" />
<script src="{!#js_path#!}jquery-1.6.2.min.js" type="text/javascript"></script>
<script src="{!#js_path#!}common.js" type="text/javascript"></script>
<script src="{!#js_path#!}all.js" type="text/javascript"></script>
<script src="{!#js_path#!}script.js" type="text/javascript" ></script>

<script src="{!#js_path#!}easyTemplate.js" type="text/javascript" ></script>

</head>

<style>
.blue{color:{!$color!};}
.menu ul li a.current,.menu ul li a:hover,.bluebg{ background:{!$color!}; }
.dt-plinput{border:{!$color!} solid 2px;}
.blue-border{border:{!$color!} solid 1px;}
</style>


<body>

<input type="hidden" id="uid" value="{!if isset($smarty.session.lovego.uid)!}{!$smarty.session.lovego.uid!}{!/if!}"/>
<input type="hidden" id="sex" value="{!if isset($smarty.session.lovego.sex)!}{!$smarty.session.lovego.sex!}{!/if!}"/>
<input type="hidden" id="nick" value="{!if isset($smarty.session.lovego.nick)!}{!$smarty.session.lovego.nick!}{!/if!}"/>

<!--top begin-->
<div class="in-width middlebg">
<div class="top1 middlebg" id="alltop">
<p class="logo fl"><a href="{!#weburl#!}"><img src="{!#img_path#!}logo.gif" height="57" width="160" border="0" /></a>
</p>
<div class="menu fr">
    <ul>
        <li><a class="current" href="/">首页</a></li>
        <li class="pulldown"><a href="{!#weburl#!}shopping/0" id="shop">逛街</a>
            <ul>
                <li><a href="{!#weburl#!}shopping/1">衣服</a></li>
                <li><a href="{!#weburl#!}shopping/2">鞋子</a></li>
                <li><a href="{!#weburl#!}shopping/3">包包</a></li>
                <li><a href="{!#weburl#!}shopping/4">配饰</a></li>
                <li><a href="{!#weburl#!}shopping/5">家居</a></li>
                {!if isset($smarty.session.lovego.sex) && $smarty.session.lovego.sex == 2!}
                <li><a href="{!#weburl#!}shopping/6">美妆</a></li>
                {!else!}
                <li><a href="{!#weburl#!}shopping/6">运动</a></li>
                {!/if!}
                <li><a href="{!#weburl#!}shopping/9">情侣</a></li>
            </ul>
        </li>
        <li><a href="{!#weburl#!}">搭配</a></li>
        <li><a href="{!#weburl#!}">晒货</a></li>
        <li><a href="{!#weburl#!}">联系我们=</a></li>
        <li><a href="{!#weburl#!}">发布=</a></li>
        <li><a href="{!#weburl#!}">邀请=</a></li>
        <li class="pulldown"><a href="">图标=====</a>
            <ul>
                <li><a href="">联系我们</a></li>
                <li><a href="">联系我们</a></li>
                <li><a href="">联系我们</a></li>
            </ul>
        </li>
    </ul>

{!if !empty($smarty.session.lovego.uid)!}
<div class="loginlost">
<a class="member-pic" href="{!#weburl#!}center/{!$smarty.session.lovego.uid!}"><img src="{!$head_img!}" height="34" width="34" border="0" /></a>
<font class="blue"><a href="{!#weburl#!}center/{!$smarty.session.lovego.uid!}">{!$smarty.session.lovego.nick!}</a></font>
    <ul>
        <li class="pulldown"><a href="">设置</a>
            <ul>
                <li><a href="{!#weburl#!}modinfo">基本信息</a></li>
                <li><a href="{!#weburl#!}modhead">修改头像</a></li>
                <li><a href="{!#weburl#!}modpswd">修改密码</a></li>
                <li><a href="{!#weburl#!}auth">授权</a></li>
                <li><a href="{!#weburl#!}pmsg">私信 </a></li>
            </ul>
        </li>
    </ul>

    <ul>
        <li class="pulldown"><a href="">消息</a>
            <ul>
                <li><a href="{!#weburl#!}atme" {!if !empty($smarty.session.lovego.atNum)!}class="blue"{!/if!}>@我的({!$smarty.session.lovego.atNum!})</a></li>
                <li><a href="{!#weburl#!}pmsg">私信</a></li>
            </ul>
        </li>
    </ul>
<a class="tuichu" href="{!#weburl#!}out">退出</a>
<div class="clear"></div>
</div>

{!else!}

<div class="index-login">
    <a class="blue blue-border" href="{!#weburl#!}login">立即登录</a>
    <a class="blue blue-border" href="{!#weburl#!}register">注册</a>
    <div class="other-list other-login1">
        <a class="other-bg1" href=""></a>
        <a class="other-bg2" href=""></a>
        <a class="other-bg3" href=""></a>
    </div>
</div>

{!/if!}

</div>
</div>
<div class="clear"></div>




<script type="text/javascript">
$(function () {
/*
    $('#shop').hover(function () {

        var pos = $(this).offset();
        var str = '<div stle="position:absolute;z-index:233;top:'+pos.top+';left:'+pos.left+'" id="sm"><ul><li><a href="{!#weburl#!}">衣服</a></li><li><a href="{!#weburl#!}">鞋子</a></li><li><a href="{!#weburl#!}">包包</a></li><li><a href="{!#weburl#!}">饰品</a></li><li><a href="{!#weburl#!}">家居</a></li><li><a href="{!#weburl#!}">美妆</a></li></ul></div>';
        if (!$(this).children().is('#sm')) {
            $('body').append(str);
//            $('#sm').css({top:pos.top,left:pos.left});
        }
    });
    */
});
</script>

<!-- js 模板 -->
<textarea id="jstpl" class="hidden">

    <#et userL userInfo>
        <div class="man-ceng1 showInfo" style="position:absolute;">

            <img src="'+global.weburl+'/public/images/wait.gif" id="wait" class="hidden" />

            <div class="clearfix userInfo hidden">
                <p class="ceng1-img"><img src="'+head_img+'" height="60" width="60" border="0"/></p>
                <div class="ceng1-word">    
                    <h3 class="blue nick">${userInfo.nick}</h3>
                    <p class="ceng1-p">
                        <span class="pro">${userInfo.province}</span>
                        <span class="city">${userInfo.city}</span>
                    </p>
                    <p class="ceng1-fx">
                        <b class="blue">6</b>分享
                        <b class="blue mar5">0</b>粉丝
                        <b class="blue mar5">111111</b>喜欢
                    </p>
                </div>
                <input type="button" class="att_btn" rel="${userInfo.uid}"/>
                <div class="ceng-admin info">${userInfo.sign}</div>
            </div>

        </div>
    </#et> 

</textarea>
<!-- js 模板 end -->
