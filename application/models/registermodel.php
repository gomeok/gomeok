<?php
require_once('CommonCrud.php');
/**
 * 注册模型
 * @author  michael
 */
class RegisterModel extends CommonCrud {
    private $lg_user = 'user';
    private $lg_user_info = 'user_info';
    private $lg_at = 'at';
    private $lg_user_detail = 'user_detail';


    public function __construct() {
        parent::__construct();
    }

    /**
     * 注册检查邮箱
     * @author  michael
	 * @param   array
	 * @param   array
	 * @return	array
     */
    public function regCkEmail($email) {
        return $this->db->select('email')->where($email)->get($this->lg_user)->num_rows();
    }

    /**
     * 注册检查昵称是否已被使用
     * @author  michael
	 * @param   array
	 * @param   array
	 * @return	array
     */
    public function regCkNick($nick) {
        return $this->db->select('nick')->where($nick)->get($this->lg_user_info)->num_rows();
    }

    /**
     * 用户登录
     * @author  michael
	 * @param	string
	 * @param 	string
	 * @return	int
     */
    public function login($email, $password) {
        return $this->db->select('user.uid,email,nick,password,sex,head_time,head_crop_time')->join($this->lg_user_info, 'user_info.uid = user.uid')->join($this->lg_user_detail, 'user_detail.uid = user.uid')->get_where($this->lg_user, array('email' => $email,'password'=>$password))->row_array();
    }

    /**
     * 查询最新的@我的内容 的数量        （ 这个是在登录的是时候做的，其他的地方可能也需要，在访问网站的同时，可能有人@了，要即时提示）
     * @author  michael
	 * @param	string
	 * @param 	bool
	 * @return	array
     */
    public function getAtNum($uid) {
        return $this->db->select('at_id')->get_where($this->lg_at, array('uid'=>$uid, 'is_look'=>'1'))->num_rows();
    }

}
?>