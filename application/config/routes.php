<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//首页
$route['wel'] = 'welcome/welcomes';
//注册
$route['register'] = 'Register/register';
//上传头像（注册后）
$route['uphead'] = 'Register/uploadHead';
//登录
$route['login'] = 'Register/login';
//退出登录
$route['out'] = 'Register/loginOut';
/////////////////////////////////////////////////////////////
//用户中心
$route['center/(:num)'] = 'UserCenter/center/$1';
//发布动态 --用户中心
$route['writed'] = 'UserCenter/writeDynamic';
//我的商品 --用户中心 1.num = uid  2.num = goodsType
$route['mygoods/(:num)/(:num)'] = 'UserCenter/myGoods/$1/$2';
//我喜欢的商品 --用户中心 1.num = uid  2.num = goodsType
$route['likedgoods/(:num)/(:num)'] = 'UserCenter/myLikeGoods/$1/$2';


//测试用的淘宝demo
$route['taobao1'] = 'GoodsApi/taobaoApi';


####
//抓取单个商品
$route['addgoods'] = 'TaobaoApi/getGoodsid';
//将商品入库
$route['insgoods_'] = 'GoodsApi/insertGoods';

//批量获取数据
$route['getdatas'] = 'GoodsApi/getDatas';
########


//逛街
$route['shopping/:num'] = 'Shopping/shopping';
//逛街-分页   == 有问题，用了两个路由
$route['shopping/:num/:num'] = 'Shopping/shopping';

//ajax加载数据
$route['shopaj/:num/:num'] = 'Shopping/shopAjax';


//单个商品
$route['goods/:num'] = 'Shopping/goods';
//发表商品评论
$route['comments'] = 'Comments/writeComments';
//发表商品评论
$route['commentsaj'] = 'Comments/commentsPage';

//获取用户信息
$route['getUInfo'] = 'UserInfo/getUserInfo';
//喜欢商品
$route['like'] = 'Shopping/likeGoods';

//加关注
$route['attention'] = 'UserInfo/attention';
//取消关注
$route['delattention'] = 'UserInfo/delAttention';






//////////////////////////////////////
//修改基本资料
$route['modinfo'] = 'UserInfo/userInfo';
//修改头像
$route['modhead'] = 'UserInfo/userHeadPhoto';
//修改密码
$route['modpswd'] = 'UserInfo/userPassword';
//授权
$route['auth'] = 'UserInfo/authorize';
//私信
$route['pmsg'] = 'UserInfo/showpmsg';
//@我的 (传统分页)
$route['atme'] = 'UserInfo/showAtMe';

///////////////////////做商品分发，晒或，搭配   /(:num)
//获取商品
$route['getgoods'] = 'Goods/getGoods';
//商品入库
$route['insgoods'] = 'Goods/insertGoods';
//上传图片
$route['uploads'] = 'Goods/uploadImg';
//晒货入库
$route['insshow'] = 'Goods/showGoods';
//搭配入库
$route['insda'] = 'Goods/daGoods';



//test测试
$route['tt'] = 'Goods/index';

##############################################


/**************************默认************************/
$route['default_controller'] = "welcome/welcomes";
$route['404_override'] = '';



/* End of file routes.php */
/* Location: ./application/config/routes.php */