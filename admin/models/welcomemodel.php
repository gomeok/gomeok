<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed @');
require_once('CommonCrud.php');

/**
 * 模型
 *
 */
class WelcomeModel  extends CommonCrud {
    private $lg_adminer = 'adminer';

    public function __construct() {
        parent::__construct();
    }

    /**
     * 用户登录
     * @author  michael
	 * @param	string
	 * @param 	string
	 * @return	int
     */
    public function login($a_nick, $a_password) {
        $rs = $this->db->get_where($this->lg_adminer, array('a_nick' => $a_nick,'a_password'=>$a_password));
        return $rs->row_array();
    }


}
?>