<?php

    /**
	 * 验证邮件地址
	 */
	function ckEmail($email) {
        if(empty($email)) {
            return 1;
        } else if(!preg_match('/\w+([-+.]\w+)*(@|#)\w+([-.]\w+)*\.\w+([-.]\w+)*/',$email)) {
            return 2;
        }
        return trim($email);
    }

	/**
	 * 验证昵称
     * 支持中文，不能以数字开头，最多20个字符，中文算2个字符
	 */
	function ckNick($nick) {
        if(empty($nick)) {
            return 1;
        } //else if(!preg_match('/\w+([-+.]\w+)*(@|#)\w+([-.]\w+)*\.\w+([-.]\w+)*/',$email)) {
           // return 2;
        //}
        return trim($nick);
    }

    /**
	 * 验证性别
     * 只能是 1 or 2
	 */
	function ckSex($sex) {
        $sex = trim($sex);
        return in_array($sex, array('1','2'))?$sex:false;
    }

	/**
     * 验证密码
     * 大于等于6位 , 小于等于20位
	 */
    function ckPassword($password) {
        if(empty($password)) {
            return 1;
        }
        $passwordLen = strlen($password);
        return ($passwordLen >= 6 && $passwordLen <= 20)?true:2;
    }

	/**
     * 数组验证处理,也做空格过滤，成功后返回经过trim的数组
     *
	 */
    function ckNum($str) {

        //检查是否是数字或数字字符串
        if(is_array($str)) {
            foreach($str as $key=>$value) {
                $val = trim($value);
                if(empty($val) || !is_numeric($val)) {
                    return false;
                } else {
                    $str[$key] = $val;
                }
            }

        } else {
            $str = trim($str);
            if(empty($str) || !is_numeric($str)) {
                return false;
            }
        }

        return $str;
    }


	/**
     * 验证字符串
     * 字符既验证，也做空格过滤，成功后返回经过trim的字符串
     *
	 *  //todo:对于字符串 还验证？？ 都需要做哪些验证呢？？？
	 */
    function ckStr($str) {

    }










	//验证邮件地址
	function fun_email($str) {
	     return (preg_match('/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i',$str))?true:false;
    }


    //解析xml函数
	function getXmlData ($strXml) {
		$pos = strpos($strXml, 'XML');
		if ($pos) {
			$xmlCode=simplexml_load_string($strXml,'SimpleXMLElement', LIBXML_NOCDATA);
			$arrayCode=get_object_vars_final($xmlCode);
			return $arrayCode ;
		} else {
			return '';
		}
	}

	function get_object_vars_final($obj){
		if(is_object($obj)){
			$obj=get_object_vars($obj);
		}
		if(is_array($obj)){
			foreach ($obj as $key=>$value){
				$obj[$key]=get_object_vars_final($value);
			}
		}
		return $obj;
	}

?>