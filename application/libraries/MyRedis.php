<?php
/**
 * redis k-v,db
 */
require_once('rediska/Rediska.php');
//require_once('/application/config/redis.conf.php');
//echo '<pre>';var_dump($redis_config);die;

class MyRedis {

	protected $redis;
	protected $redisconfig;

	//protected $key_hash;
	protected static $className = __CLASS__;

	public function __construct() {
        require_once('/application/config/redis.conf.php');

    	//$this->redisconfig = $redisconfig;
    	$this->redis = new Rediska($redis_config);
        return $this->redis;
    }

	public function key_hash($keyname) {
		return new Rediska_Key_Hash($keyname);
	}

    public function set($keyanme, $data, $timeout = 3600) {
   		$key = $this->setredskakey($keyanme);
		$key->setValue($data);
		$key->expire($timeout);
    }

    public function get($keyanme) {
    	$key = $this->setredskakey($keyanme);
    	return $key->getValue();
    }

    public function del($keyanme) {
        $key = $this->setredskakey($keyanme);
    	return $key->delete();
    }

    /**
     * 这里不能用单例模式，操作一个键，就要实例化一次Rediska_Key，否则只能产生一个键
     *
     */
    private function setredskakey($key) {
        return new Rediska_Key($key);
    }

}
?>