<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
/**
 * 评论类
 *
 */
class Comments extends Common {
    public $lg_comment = 'comment';
    public $lg_at = 'at';

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 默认入口
     */
    public function index() {
        die('teste');
    }

	/**
	 * 发表评论
	 * 用户评论必须登录
     */
	public function writeComments() {
        //判断是否登录
        if(empty($this->user_info['uid'])) {
            echo 0;die;
        }

        $c_type = $this->input->post('c_type');
        $g_id = $this->input->post('g_id');
        $c_content = $this->input->post('c_content');


        //这里要验证评论的长度
//************************

        //在这里要先将<img>反匹配回去
        $i = strpos($c_content,'<img src="/public/smilies/smileys/');
        if(is_int($i)) {
            $c_content = $this->getSmileys($c_content,true);
        }
//        echo '<pre>';var_dump($c_content);die;

        //匹配评论回复（拼成完整的评论回复）
        $c_content = strip_tags($c_content);
        //这个是要回复的内容
        if(strpos($c_content,'//@')) {
            $c_content= strstr($c_content,'//@',true);
        }
        $c_content = str_replace('#@', '//@', $c_content);   //匹配掉‘#@’

        //将@匹配出来，查该@的nick是否存在，存在就将被@的人的uid存入at表中

        //$reg = '/@(\w+)/';
        $reg = '/@([\x{4e00}-\x{9fa5}A-Za-z0-9_-—-]+)/u';    //支持中英下的 下滑线和减号
        $r = preg_match_all($reg,$c_content,$matches);

        if($r) {
            //匹配结果 0是加@+nick了，1是nick
            $this->load->model('CommentsModel');
            $matches_nick = array_unique($matches['1']);
            $nicks = $this->CommentsModel->selNicks($matches_nick); //一次全部nick查出

            //判断nick是否存在，存在判断sex,然后加<a>,class
            foreach($matches_nick as $key => $m_value) {
                $i_uid = '';

                if($nicks) {
                    foreach($nicks as $k => $v) {

                        if($m_value == $v['nick']) {    //nick存在
                            if($v['sex'] == 1) {
                                //$class = 'boy';
                                $class = 'GG';
                            } else if($v['sex'] == 2) {
                                //$class = 'girl';
                                $class = 'MM';
                            }

                            $i_uid = $v['uid'];
                            break;

                        } else {    //nick不存在
                            //$class = 'nopeople';
                            $class = 'NP';
                        }

                    }

                } else {
                    $class = 'NP';
                    //$class = 'nopeople';
                }

                $nick = '@'.$m_value;
                $rel = empty($i_uid)?'':'rel="'.$i_uid.'"';     //如果该用户存在，就将uid放到html标签里，不存在就是空
                $n = '<a class="'.$class.'" '.$rel.'>'.$nick.'</a> ';   //todo: <a>标签没有加href,会不会出现不兼容的情况，需要好好测试下
                //$n = '<font class="'.$class.'" '.$rel.'>'.$nick.'</font> ';
                $c_content = str_replace($nick, $n, $c_content);   //匹配完之后的评论内容 color="red"
            }

        }

        $data = array(
            'uid'=>$this->user_info['uid'],
            'c_content'=>$c_content,
            'c_type'=>$c_type,
            'x_id'=>$g_id-10000
        );

        if($this->db->insert($this->lg_comment, $data)) {
            $c_id = $this->db->insert_id();

            //如果有@XX匹配成功，就存at表
            if(isset($nicks) && !empty($nicks)) {
                foreach($nicks as $key => $value) {
                    $this->db->insert($this->lg_at, array('uid'=>$value['uid'],'x_id'=>$c_id));
                }
            }

            //在这里将表情匹配出来,并且将评论内让返回
            echo $this->getSmileys($c_content);

            /*
            $this->config->load('smileys');
            $smileys_config = $this->config->item('smileys');

            foreach($smileys_config as $key => $value) {
                $i = strpos($c_content,$key);
                if(is_int($i)) {
                    $sm = '<img src="/public/smilies/smileys/'.$value['0'].'"/>';  //'.base_url().'
                    $c_content = str_replace($key, $sm, $c_content);
                }
            }*/

            //echo $c_content;

        }
	}

	/**
	 * 评论ajax 分页
	 * 用户评论必须登录
     */
	public function commentsPage() {

        //判断是否登录
        if(empty($this->user_info['uid'])) {
            echo 0;die;
        }

        $per_page = 8;

        $c_type = $this->input->post('c_type');
        $x_id = $this->input->post('x_id');
        $x_id-=10000;
        $page_id = $this->input->post('pn');//页码

//        $c_total = $this->ShoppingModel->totalComment('1',$g_id);

        $offset = ($page_id-1)*$per_page;

        $this->load->model('ShoppingModel');
        //查评论
        $comment = $this->ShoppingModel->selComment($c_type, $x_id, $offset, $per_page);
        echo json_encode($comment);
	}

}
?>