<?php
require_once('CommonCrud.php');
/**
 * 用户信息
 * 1，关注处理
 *
 * @author  michael
 */
class UserInfoModel extends CommonCrud {
    private $lg_user = 'user';
    private $lg_user_detail = 'user_detail';
    private $lg_user_info = 'user_info';
    private $lg_comment = 'comment';
    private $lg_at = 'at';
    private $lg_dongtai = 'dongtai';

//    private $lg_attention = 'attention';
//    private $lg_be_attention = 'be_attention';


    public function __construct() {
        parent::__construct();
    }

    /**
     * 加关注 (lg_attention)
     * @author  michael
	 * @param	$uid        当前用户uid
     * @param	$att_uid    对方uid
	 * @return
     */
    public function attention($uid, $att_uid) {
        //uid % 10判断用户在那张表中
        $my_tb = 'lg_attention_'.$uid%10;
        $he_tb = 'lg_attention_'.$att_uid%10;

        //关注用户
        if($this->db->insert($my_tb, array('uid'=>$uid, 'att_uid'=>$att_uid))) {
            $this->db->insert($he_tb, array('uid'=>$uid, 'att_uid'=>$att_uid)); //要是数据库执行失败，后面的代码为什么就不执行了？？？
            return true;
        }
        return false;
    }

    /**
     * 取消关注 (lg_attention)
     * @author  michael
	 * @param	$uid        当前用户uid
     * @param	$att_uid    对方uid
	 * @return
     */
    public function delAttention($uid, $att_uid) {
        //uid % 10判断用户在那张表中
        $my_tb = 'lg_attention_'.$uid%10;
        $he_tb = 'lg_attention_'.$att_uid%10;

        //取消关注
        $this->db->delete($he_tb, array('uid'=>$uid, 'att_uid'=>$att_uid));
        if($this->db->delete($my_tb, array('uid'=>$uid, 'att_uid'=>$att_uid))) {
            return true;
        }
        return false;
    }

    /**
     * 查我关注的人
     * @author  michael
	 * @param	string
	 * @param 	bool
	 * @return	array
     */
    public function getAttentioners($uid) {
        $lg_attention = 'lg_attention_'.$uid%10;
        $myAttention = $this->db->select('att_uid')->get_where($lg_attention, array('uid' => $uid))->result_array();

        $attention = array();
        foreach($myAttention as $att_uid) {
            $attention[] = $att_uid['att_uid'];
        }
        return $attention;
    }

    /**
     * 查询@我的内容   因为@的内容是在3张表中，根据@表分页，然后去那3张表中查动态或者评论     todo: 动态的评论也需要添加上去
     * @author  michael
	 * @param	$uid        当前用户uid
	 * @param 	$offset     查询偏移量
     * @param 	$num        查询条数
	 * @return	array
     */
    public function getAtMe($uid, $offset=0, $num=10) {

//        $newComments = $this->db->select('at_id,at_type,comment.uid,at.x_id,c_type,c_content as content,c_time as time,nick')->join($this->lg_comment, 'comment.c_id=at.x_id', 'left')->join($this->lg_user_info, 'comment.uid=user_info.uid', 'left')->get_where($this->lg_at, array('at.uid'=>$uid,'at_type'=>'1'), $num, $offset)->result_array();
//
//        $newDynamic = $this->db->select('at_id,at_type,dongtai.uid,at.x_id,content,d_time as time')->join($this->lg_dongtai, 'dongtai.did=at.x_id', 'left')->join($this->lg_user_info, 'dongtai.uid=user_info.uid', 'left')->get_where($this->lg_at, array('at.uid'=>$uid,'at_type'=>'2'), $num, $offset)->result_array();

        //去查@表， 分页查
        $atList = $this->db->select('at_id,at_type,x_id')->order_by('at_id','DESC')->get_where($this->lg_at, array('uid'=>$uid), $num, $offset)->result_array();
//echo '<pre>';var_dump($atList);die;
//echo $this->db->last_query();die;

        $commentIds = array();
        $dynamicIds = array();

        foreach($atList as $key => $val) {
            if($val['at_type'] == '1') {        //找出虽有评论的id
                $commentIds[] = $val['x_id'];
            } else if($val['at_type'] == '2') { //找出所有动态的id
                $dynamicIds[] = $val['x_id'];
            }
        }

        //判断如果 commentIds 不为空， 去查评论表
        $comments= array();
        if(!empty($commentIds)) {
            $comments = $this->db->select('c_id,comment.uid,c_type,c_content as content,c_time as time,nick')->join($this->lg_user_info, 'comment.uid=user_info.uid', 'left')->where_in('comment.c_id', $commentIds)->get($this->lg_comment)->result_array();
        }

        //判断如果 dynamicIds 不为空， 去查动态表
        $dynamics = array();
        if(!empty($dynamicIds)) {
            $dynamics = $this->db->select('da_id,dongtai.uid,content,d_time as time,nick')->join($this->lg_user_info, 'dongtai.uid=user_info.uid', 'left')->where_in('dongtai.da_id', $dynamicIds)->get($this->lg_dongtai)->result_array();
        }

        foreach($atList as $kA => $at) {

            if(!empty($comments) && in_array($at['x_id'], $commentIds)) {

                foreach($comments as $kC => $comment) {
                    //判断at.x_id 是否等于 评论的主键
                    if($at['x_id'] == $comment['c_id']) {
                        $atList[$kA]['content'] = $comment['content'];
                        $atList[$kA]['time'] = $comment['time'];
                        $atList[$kA]['uid'] = $comment['uid'];
                        $atList[$kA]['nick'] = $comment['nick'];
                        //表示评论的类型， 这个评论是商品，晒货，搭配的
                        $atList[$kA]['type'] = $comment['c_type'];
                    }
                }

            } else if(!empty($dynamics) && in_array($at['x_id'], $dynamicIds)) {

                foreach($dynamics as $kD => $dynamic) {
                    //判断at.x_id 是否等于 动态的主键
                    if($at['x_id'] == $dynamic['da_id']) {
                        $atList[$kA]['content'] = $dynamic['content'];   //todo: 动态的内容是序列化存储的，这里需要反序列化
                        $atList[$kA]['time'] = $dynamic['time'];
                        $atList[$kA]['uid'] = $dynamic['uid'];
                        $atList[$kA]['nick'] = $dynamic['nick'];
                        //表示动态
                        $atList[$kA]['type'] = '0';
                    }
                }

            }

        }
        return $atList;
    }

    /**
     * 查询@我的总数 @我的总条数，分页使用
     * @params  $uid    当前用户uid
     * @return  int
     */
    public function totalAtMe($uid) {
        return $this->db->select('at_id')->where('uid', $uid)->count_all_results($this->lg_at);
    }

    /**
     * 用户进入@我的信息列表后，将所有的未查看@修改为已查看
     * @params  $uid    当前用户uid
     * @return  int
     */
    public function updateAtMe($uid) {
        return $this->db->update($this->lg_at, array('is_look'=>'0'), array('uid'=>$uid));
    }

}
?>