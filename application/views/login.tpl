{!config_load file="web.inc.conf"!}
{!include file="inc/header.tpl"!}
<link href="{!#css_path#!}login.css" rel="stylesheet" type="text/css" />

<!--
<div class="banner in-width1">
<img src="images/banner1.jpg"  height="301" width="955" border="0" />
</div>
  -->
<div class="footbg in-width"></div>
</div>
<!--top end-->

<script type="text/javascript">
$(function() {
    $('#id_password1').focus(function(){
        $(this).css("color","black");
    	if($(this).val()=="输入密码"){
        	$(this).attr("value","");
    	}
    	this.type = 'password';
    });
    $('#id_password1').blur(function(){
        $(this).css("color","#ccc");
     	if($(this).val()==""){
    		//$(this).removeAttr("type");
    		this.type = 'text';
    		$(this).attr("value","输入密码");
    	}
     });
    $('#id_email').focus(function() {
        $(this).css("color","black");
    	if($(this).val()=="输入邮箱"){
        	$(this).attr("value","");
    	}else if($(this).val()==""){
    		$(this).attr("value","输入邮箱");
    	}
    });
    $('#id_email').blur(function() {
        $(this).css("color","#ccc");
    	if($(this).val()==""){
    		$(this).attr("value","输入邮箱");
    	}
    });
});


</script>

<!--content begin-->
<div class="in-width">
<div class="topbg"></div>
<div class="middlebg over-hidden">
<div class="reg_sign">
<div class="thirdPart">选择合作网站账号登录<br>
   <a target="_blank" title="新浪微博" class="isina" href="/auth/request/sina_weibo/"></a>
   <a target="_blank" title="腾讯QQ" class="iqq" href="/auth/request/qzone/"></a>
   <a target="_blank" title="腾讯微博" class="itengxun" href="/auth/request/qq_weibo/"></a>
</div>

<form class="registerForm" action="{!#weburl#!}login" method="post" id="signup_form_main">
    <fieldset class="inlineLabels reg_inlineLabels">
            <p>
            <input type="text" class="input1" name="email" manual="true" value="输入邮箱" id="id_email">
                {!if isset($error['emailpwd'])!}
                <span color='red'>邮箱或者密码错误!</span>
                {!/if!}
            </p>

            <div class="itempassword">
            <input type="text" class="input1" name="password"  value="输入密码" id="id_password1">
            </div>

        <p class="user_sub"><a href="{!#weburl#!}register">注册</a><input type="submit" name='submit' value="登录" class="o2-button ui-state-default ui-corner-all regBtn"></p>
    </fieldset>
</form>
<div class="clear"></div>
</div>
</div>
<div class="footbg"></div>
</div>
<!--content end-->

{!include file="inc/footer.tpl"!}
