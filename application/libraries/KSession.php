<?php
class KSession {

	/**
	* 命名空间
	*
	* @var string
	*/
	protected $_namespace = "lovego";

	/**
	* 默认会话开启状态
	*
	* @var boolean
	*/
	protected $_sessionStarted = false;

	/**
	* 默认会话变量
	*
	* @var boolean
	*/
	protected $_sessionDestroyed = false;

	/**
	* <code>
	* $session = new RcSession('mywebsite', $mySessionId)
	* </code>
	*/
	public function __construct($namespace = 'lovego', $sessionId = null) {

		// should not be empty
		if ($namespace === '') {
			throw new RcSessionException('命名空间不能为空.');
		}
		// should not start with underscore
		if ($namespace[0] == "_") {
			throw new RcSessionException('不能启动会话.');
		}
		// should not start with numbers
		if (preg_match('#(^[0-9])#i', $namespace[0])) {
			throw new RcSessionException('命名空间键值不能是数字.');
		}

		$this->_namespace = $namespace;
		if ($sessionId != null) $this->setId($sessionId);
		$this->start();
	}

	/**
	* Start session
	*
	* @return void
	*/
	public function start() {
		// 检查会话是否已经开始
		if ($this->_sessionStarted) {
			return;
		}
		session_start();
		$_SESSION[$this->_namespace]['session_id'] = $this->getId();

		$this->_sessionStarted = true;
	}

	/**
	* 检查session是否已经开始
	*
	* @return boolean
	*/
	public static function isStarted() {
		if (isset($_SESSION)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 关闭写session
	 */
	public function stop() {
		if ($this->_sessionStarted) {
			session_write_close();
			$this->_sessionStarted = false;
		}
	}

	/**
	* Set variable into session
	* @param string $name Name of key
	* @param mixed $value Value for keyname ($name)
	*/
	public function set($name, $value=null) {
        if ($name === "")
            throw new RcSessionException("键不能是空");
        if (!$this->_sessionStarted)
            throw new RcSessionException("session 没有启动");

        if(is_array($name)) {
            foreach($name as $key => $value) {
                $_SESSION[$this->_namespace][$key] = $value;
            }

        } else {
            if($value===null)
                unset ($_SESSION[$this->_namespace][$name]);
            else
                $_SESSION[$this->_namespace][$name] = $value;
        }

	}

	/**
	* 销毁所有会话数据
	*/
	public function destroy() {
		if (!$this->_sessionStarted) {
			throw new RcSessionException("Session 没有启动.");
		}
		if ($this->_sessionDestroyed) {
			return;
		}
		if (isset($_SESSION[$this->_namespace])) unset($_SESSION[$this->_namespace]);
		session_destroy();
		$this->_sessionStarted = false;
		$this->_sessionDestroyed = true;
	}

	/**
	* 检查session是否被销毁
	*
	*/
	public function isDestroyed() {
		return $this->_sessionDestroyed;
	}

	/**
	*  删除命名空间
	*/
	public function namespaceUnset($name = null) {
		if (!$this->_sessionStarted) {
			throw new RcSessionException("Session not started, use RcSession::start()");
		}
		if (empty($name)) {
			unset($_SESSION[$this->_namespace]);
		} else {
			unset($_SESSION[$this->_namespace][$name]);
		}
	}

	/**
	* 获得session id
	*/
	public static function getId() {
		if (!isset($_SESSION)) {
			throw new RcSessionException("Session not started, use RcSession::start()");
		}
		return session_id();
	}

	/**
	* 设置session id
	*
	*/
	public function setId($id) {
		if (isset($_SESSION))
			throw new RcSessionException("会话开始id必须存在.");
		if (!is_string($id) || $id === '')
			throw new RcSessionException("id不能是字符串也不可以为空.");
		session_id($id);
	}

	/**
	* 把所有局部变量命名空间在一个数组中输出
	*/
	public function getAll() {
		$sessionData  = (isset($_SESSION[$this->_namespace]) && is_array($_SESSION[$this->_namespace])) ?
			$_SESSION[$this->_namespace] : array();
		return $sessionData;
	}

	/**
	* 变量命名空间得到,以供参考
	*/
	public function &get($name) {
		if (!$this->_sessionStarted) {
			throw new RcSessionException("Session not started, use RcSession::start()");
		}
		if ($name == '')
			throw new RcSessionException("Name should not be empty");
		if (!isset($_SESSION[$this->_namespace][$name])) {
			$result = null;
            return $result;
        } else {
            return $_SESSION[$this->_namespace][$name];
        }
	}
/*
	public function & __get($name) {
		if (!$this->_sessionStarted) {
			throw new RcSessionException("Session not started, use RcSession::start()");
		}
		return $this->get($name);
	}*/


	public function sessIsset($name) {
		if (!$this->_sessionStarted) {
			throw new RcSessionException("Session not started, use RcSession::start()");
		}
		if (isset($_SESSION[$this->_namespace][$name])) {
			return true;
        } else {
            return false;
        }
	}


	public function sessUnset($name) {
		if (!$this->_sessionStarted) {
			throw new RcSessionException("Session not started, use RcSession::start()");
		}
		if (isset($_SESSION[$this->_namespace][$name])) {
			unset($_SESSION[$this->_namespace][$name]);
			return true;
		}
		return false;
	}

}

class RcSessionException extends Exception {

}
