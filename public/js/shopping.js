
$(function(){
    head_img = $('#head_img').val();

    //ajax分页偏移量
    var page_offset = $('#page_offset').val();
    //商品类型
    var goods_type = $('#goods_type').val();

    //页面加载进来直接ajax 40条数据
    getgoods(page_offset, goods_type);

    //窗口的高度
    var hw = $(window).height();
    //滚动查询数据
    $(window).scroll(function () {
//        var tbody = $('body').scrollTop();        //body ie下不行
        var tbody = $(window).scrollTop();

        $('#show').css('top',eval(tbody)+200);//测试用

        //当前document的高度
        var hd = $(document).height();

        //将缓存中document的高度取出来（存的上一次的document的高度）
        var doc_height = $('body').data('doc');
        var doc_h = typeof(doc_height)=='undefine'?0:doc_height;

        var c = hd-tbody;

        if (c <= 4*hw && doc_h != hd) {
            $('#show').html(c);

            var pt = $('#page_time').val();
            if (pt<=7) {
                //ajax分页偏移量
                var page_offset = $('#page_offset').val();

                getgoods(page_offset, goods_type);
                //将document的高度存起来
                var hd = $(document).height();
                $('body').data('doc',hd);
            }
        }
    });



    //喜欢(这里不能删除喜欢，在用户中心删除喜欢)
    $('.like').live('click', function() {
        var h_uid = $('#uid').val();
        if (h_uid != '') {  //判断是否登录

            var me = $(this);

            var obj = $(this);
            while (obj.attr('class') != 'g_parent') {
                obj = obj.parent();
            }

            //判断喜欢的商品是否是自己的
            var uid = obj.children('.uid').val();
            if (h_uid != uid) {

                var is_like = obj.children('.is_like').val();
                if (is_like == '' || is_like == 0) {
                    var gid = obj.children('.g_id').val();//获取当前点击商品的gid

                    //ajax
                    global.ajaxget('like', {'gid':gid,'uid':uid}, function(like) {
                        if (like == 0) {
                            alert('请登录');
                        } else {
                            me.css('background-position','0 -35px');
                            //me.next().children('.likenum').html(like);
                            obj.find('.likenum').html(like);
                            obj.children('.is_like').val('1');
                        }
                    });

                } else {
                    alert('已经喜欢过了！');
                }

            } else {
                alert('自己的啊！');
            }

        } else {
            alert('请登录!!!');
        }
    });

});




//ajax获取商品数据 和 分页
function getgoods(page_offset, goods_type) {

    global.ajaxget('shopaj/'+goods_type+'/'+page_offset,'',function(msg) {

        if (msg != '') {
            var div_id;

            $.each(msg, function(i,n) {    //msg.data
                $.each(n, function(k,v) {
                    if (i==0) {
                        div_id = 'goods1';
                    } else if (i==1) {
                        div_id = 'goods2';
                    } else if (i==2) {
                        div_id = 'goods3';
                    } else if (i==3) {
                        div_id = 'goods4';
                    }
                    goodsdiv(div_id, v.pic, v.g_url, v.price, v.g_like_num, v.uid, v.nick, v.g_intro, v.g_id, v.like, v.head_img);
                });
            });
        } else {
            alert('商品数据为空！');
        }

    },'json');

    //偏移量
    $('#page_offset').val(eval(page_offset)+20);

    //加载次数
    var page_time = $('#page_time').val();
    $('#page_time').val(eval(page_time)+1);
}


//js展示商品    //head_img 是smarty抛出的头像图片，以后要去掉
function goodsdiv(id, g_pic, g_url, price, g_like_num, uid, nick, g_intro, g_id, is_like, head_img) {
    var gid = eval(g_id)+10000; //商品的id都是加10000
    //var is_like = in_array(g_id, like_arr);   //循环数组确定是否被喜欢，在ie下的速度不行，要改成在服务器上的循环
    var weburl = global.weburl;

    var div_str = '<div class="intro1a"><div class="good-top1"></div><div class="good-lc2"><div class="g_parent"><div class="intro-img"><a href="'+weburl+'goods/'+gid+'"><img src="'+g_pic+'" border="0" /></a><a style="position:absolute; border:1px red solid;left:170;right:100;" href="'+g_url+'" target="__blank">￥'+price+'</a></div><div class="intro-word clearfix"><span class="good-star fl like '+is_like+'"></span><font class="blue fl">(<span class="likenum">'+g_like_num+'</span>)</font><span class="good-tl fr"></span></div><dl class="intro-man clearfix"><dt class="man-img fl"><img src="'+head_img+'" height="33" width="33" border="0" class="getuserinfo"  rel="'+uid+'"/><span class="hidden">'+uid+'</span></dt><dd class="man-word fl"><p><a class="blue" href=""><b class="getuserinfo" rel="'+uid+'">'+nick+'</b><span class="hidden">'+uid+'</span></a>商品id:'+g_id+'</p><p><a href="">'+g_intro+'</a></p></dd></dl><input type="hidden" class="g_id" value="'+gid+'"/><input type="hidden" class="uid" value="'+uid+'"/><input type="hidden" class="is_like" value="'+is_like+'"/></div><div class="good-foot1"></div></div>';

    $('#'+id).append(div_str);
}

//判断一个值是否在数组中
//function in_array(needle, like_arr) {
////    var type = typeof(needle) ;
//
////    if(type == 'string' || type == 'number') {    //这里需要判断类型吗？？？
//        for(var i in like_arr) {
//            if(like_arr[i] == needle) {
//                return 'islike';
//            }
//        }
////    }
//    return 0;
//}
