<?php
require_once('CommonCrud.php');
/**
 * 用户修改模型
 * @author  xingpeidong
 */
class ModModel extends CommonCrud {
    private $lg_user = 'user';
    private $lg_user_detail = 'user_detail';


    public function __construct() {
        parent::__construct();
    }

    /**
     * 修改用户信息
	 * @param   array $data
	 * @param   int $id
	 * @return	array
     */
    public function updateUserInfo($uid,$data){
		$this->db->where('uid', $uid);
		$rs = $this->db->update($this->lg_user, $data);//var_dump($data);die;
        return $rs;
    }
    
    
 
    /**
     * 修改用户详细信息
	 * @param   array $data
	 * @param   int $id
	 * @return	array
     */
    public function updateUserDetail($uid,$data){
		$this->db->where('uid', $uid);//var_dump($data);die;
		$rs = $this->db->update($this->lg_user_detail, $data);
		//var_dump($this->db->last_query());
        return $rs;
    }
    
    
    
    /**
     * 根据用户名搜索用户信息条数
     * @author  xingpeidong
	 * @param	string
	 * @param 	string
	 * @return	int
     */
    public function selNick($nick) {
    	$query = $this->db->get_where($this->lg_user, array('nick' => $nick));
    	return $query->num_rows();
    }

    
    
    /**
     * 用户信息搜索
     * @author  xingpeidong
	 * @param	string
	 * @param 	string
	 * @return	array
     */
    public function selUserInfo($data) {
    	$query = $this->db->get_where($this->lg_user, $data);
    	return $query->row_array();
    }


}
?>