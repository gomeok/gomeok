<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('ShoppingAndCenter.php');

/**
 * 用户中心类
 *  1, 动态
 * @author  michael
 *
 */
class UserCenter extends ShoppingAndCenter {

    public $lg_dongtai = 'lg_dongtai';
//    static public $itemcatsGet;


    /**
     *  构造
     */
    public function __construct() {
        parent::__construct();
//        $this->isLogin();

        //加载表情
        $this->makeSmileys();
        $datetime = date('Y-m-d H:i:s');
    }

    /**
     * 默认入口
     */
    public function index() {
        die('teste');
    }

    /**
     * 用户中心
     * 用户中心默认进入的是动态页
     * 1. 动态最多可以发3张图片
     * 2. 动态最多可以同时发3个商品
     * 3. 文字140（最好最中英文的判断）
     * 4. 小图片得按照高度缩略，大图片按照宽度缩略（特殊比例的情况需要好好考虑考虑）
     * 5. 数据库存储格式，将内容存到数组里面，序列化后存到数据库
            array(
                'write'=>'',
                'img'=> array()
            );
       6. 图片：动态里2张，商品页3张

         动态分类：
        1. 发布文字
        2. 发文字+图片
        3. 发商品
        4. 喜欢商品
        5. 晒货，搭配
        （6. 电一下  7. 求推荐，求指导，求帮助 8. 朋友给点穿着意见【明日约会，or 去同学聚会】9.求关注）
     */
    public function center($uid) {

        //**************** 参数验证 $uid

        $this->load->model('CenterModel');

        $dynamic = $this->CenterModel->getDynamic($uid);

        foreach($dynamic as $key=>$d) {
            $content = unserialize($d['content']);
            $dynamic[$key]['content'] = $content['write'];
            //判断动态中是否有图片
            if(isset($content['img'])) {
                $dynamic[$key]['content'] = $content['img'];
            }
        }

        //动态数组
        $this->smart->assign('dynamic',$dynamic);

        //查我的关注的人
        $myAttention = array();
        if($uid != $this->user_info['uid']) {
            $myAttention = $this->redisAttention($this->user_info['uid']);
        }

        //查用户资料
        $userInfo = $this->CenterModel->getUserInfo($uid, $this->user_info['uid'], $myAttention);
        $this->smart->assign('userInfo', $userInfo);

        $this->smart->assign('viewedUid', $uid);
        $this->smart->assign('img_date',date('Y-m-d H:i:s'));   //这个不能删，发布商品。，晒货使用
        $this->smart->display('dongtai.tpl');
    }

    /**
     * 发布动态  ******需要加数据验证
     * 1. 文字
     */
    public function writeDynamic() {

        //************yz
        $dynamic = $this->input->post('dynamic');

        $d_arr = array('write'=>$dynamic);
        $ser_d = serialize($d_arr);
//        echo '<pre>';var_dump($ser_d);die;
        $arr = array(
            'uid'=>$this->user_info['uid'],
            'content'=>$ser_d,
            'd_time'=>time()
        );
        if($this->db->insert($this->lg_dongtai, $arr)) {
            $this->center();
        } else {
            die('dy !');
        }

    }

    /**
     * 我的商品（宝贝）
     * @params  两个参数都是从uri路由段获取的
     */
    public function myGoods($viewedUid, $goodsType) {
        //如果访问的是自己的用户中心，检查登录了没有。访问别人的用户中心，不需要登录也可以访问
        if($this->user_info['uid'] == $viewedUid) {
            $this->isLogin();
        }

        $this->load->model('CenterModel');

        //偏移量
        $offset = $this->input->get('page');       //todo:数据验证
        $perPageNum = 3;    //每页条数

        $myGoods = $this->CenterModel->getMyGoods($viewedUid, $goodsType, $offset, $perPageNum);
        $myGoods = $this->splitArr($myGoods);
        $this->smart->assign('myGoods', $myGoods);

        $goodsTotal = $this->CenterModel->totalMyGoods($viewedUid, $goodsType);
        $pageStr = $this->pages('mygoods/'.$viewedUid.'/'.$goodsType.'?', $goodsTotal, $perPageNum, true);
        $this->smart->assign('pageStr', $pageStr);

        //获取用户信息

        //uri
        $this->smart->assign('uri', 'mygoods');
        //被访问者的uid
        $this->smart->assign('viewedUid', $viewedUid);
        $this->smart->display('mygoods.tpl');
    }

    /**
     * 我喜欢的商品（喜欢）
     * @params  两个参数都是从uri路由段获取的
     */
    public function myLikeGoods($viewedUid, $goodsType) {
        //如果访问的是自己的用户中心，检查登录了没有。访问别人的用户中心，不需要登录也可以访问
        if($this->user_info['uid'] == $viewedUid) {
            $this->isLogin();
        }

        $this->load->model('CenterModel');

        //偏移量
        $offset = $this->input->get('page');       //todo:数据验证
        $perPageNum = 3;    //每页条数

        $likeGoodsGid = $this->redisLikeGoods($viewedUid);
        $myGoods = $this->CenterModel->getMyLikeGoods($goodsType, $likeGoodsGid, $offset, $perPageNum);

        $myGoods = $this->splitArr($myGoods);
        $this->smart->assign('myGoods', $myGoods);

        $totalLikeNum = $this->CenterModel->totalMyLikeGoods($goodsType, $likeGoodsGid);
//        $totalLikeNum = count($likeGoodsGid);
        $pageStr = $this->pages('likedgoods/'.$viewedUid.'/'.$goodsType.'?', $totalLikeNum, $perPageNum, true);
        $this->smart->assign('pageStr', $pageStr);

        //获取用户信息

        //uri
        $this->smart->assign('uri', 'likedgoods');
        //被访问者的uid
        $this->smart->assign('viewedUid', $viewedUid);
        $this->smart->display('mygoods.tpl');
    }

}
?>