<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends CI_Controller {
	/**
	 * 公共属性
     */
    public $aweburl;
    public $user_info;

	/**
	 * 公共控制器的构造函数  common __construct
	 *
     */
    public function __construct() {
        header("Content-type:text/html;charset=utf-8");
        parent::__construct();

        $this->user_info = $this->ksession->getAll();//不基本的用户信息从session中取出来存到了user_info中，下面所有的子类都用user_info取用户信息

        if(!empty($this->user_info['a_id'])) {
//            redirect($this->aweburl, 'location');die;
        }
    }

	/**
	 * Index Page for this controller.
	 *
     */
	public function index() {
        echo('index!');
	}

	/**
	 * 商品图片缩略以及存贮
	 * sae
     */
    public function resizeGoodsImg($img_path, $datetime, $extfilename='', $uid='') {
        //存图片  SaeStorage
        $s = new SaeStorage();

        $f = new SaeFetchurl();
        $img_data = $f->fetch($img_path);
        $img = new SaeImage();
        $img->setData( $img_data );

        //生成图片路径+名称
        $user_dir = $this->makeUserDir($uid, $datetime, $extfilename);

        $img->resize(320); // 等比缩放到320宽
        $img_b = $img->exec(); // 执行处理并返回处理后的二进制数据
        $filename_b = $user_dir.'_b.jpg';     //320*……
        $s->write('lovegoimg' , $filename_b , $img_b);  //320*……的存了

        $img->resize(200); // 等比缩放到200宽
        $img_m = $img->exec();
        $filename_m = $user_dir.'_m.jpg';      //200*……
        $s->write('lovegoimg' , $filename_m , $img_m);  //200*……的存了

        $img->resize(100,100); // 等比缩放到100宽
        $img_s = $img->exec();
        $filename_s = $user_dir.'_s.jpg';     //100*100
        $s->write('lovegoimg' , $filename_s , $img_s);  //将100*100的存了

        if(empty($uid)) {
            return $s->getUrl( 'lovegoimg' , $filename_s );
        }
    }

	/**
	 * 将用户散列到不同的目录中
	 *
     */
    public function makeUserDir($uid, $datetime, $extfilename='', $type='') {
        /*散列目录/uid/year/day/XXX.jpg*/

        $user_dir = $uid{'0'};
        $user_dir .= substr(md5(substr($uid, strlen($uid)-2, 2)), 0, 6).'/';
        if($type == '1') {
            return $user_dir;die;
        }

        $year = substr($datetime, 0, 4);
        $day = substr($datetime, 0, 10);
        $file = md5($uid.$datetime);
        if(!empty($extfilename)) {
            $file .= $extfilename;
        }
        $user_dir .= $uid.'/'.$year.'/'.$day.'/'.$file;

        return $user_dir;
    }
    
    
    
    /**
	 * 分页
	 *
     */
    public function pages($uri, $total, $per_page, $num_links='5', $uri_segment='3') {
        $this->load->library('pagination');

        $config['uri_segment'] = $uri_segment;     //分页所属uri段
        $config['base_url'] = base_url().$uri;
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $config['num_links'] = $num_links;
//        $config['use_page_numbers'] = TRUE;

        $config['prev_link'] = '上一页';
        $config['next_link'] = '下一页';

        $config['first_link'] = '第一页';
        $config['last_link'] = '末页';

        //自定义“当前页”链接
        $config['cur_tag_open'] = '<b style="color:red;">';
        $config['cur_tag_close'] = '</b>';

        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }
    

}

/* End of file common.php */
/* Location: ./application/controllers/common.php */