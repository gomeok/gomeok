    Ext.onReady(function() {
        var ajax_url = 'http://test.me/ext/';

        //定义的数据字段
        Ext.define('users', {
            extend: 'Ext.data.Model',
            fields: ['id', 'name']
        });

        Ext.define('System.Grid', {
            extend: 'Ext.grid.Panel',
            alias: 'widget.writergrid',
            requires: [
            'Ext.form.field.Text',
            'Ext.toolbar.TextItem',
            'Ext.toolbar.Paging',
            'Ext.data.*'
            ],
            initComponent: function(){

                Ext.apply(this, {
                    iconCls: 'icon-grid',
                    frame: true,
                    columns: [
                    {
                        header: 'test id ',
                        sortable: false,
                        dataIndex: 'id',
                        flex: 1
                    },{
                        header: 'test name',
                        dataIndex: 'name',
                        flex: 1
                    }
                    ],
                    selModel: new Ext.selection.CheckboxModel(),
                    columnLines: true   //为true是，复选框只能选一行
                });
                this.callParent();
                this.getSelectionModel().on('selectionchange', this.onSelectChange, this);
            }

        });

    //ajax加载数据
        store = Ext.create('Ext.data.Store', {
            model: 'users',
            autoLoad: true,
            autoSync: true,
            params: {
                start:0,
                limit:25
            },
            proxy: {
                type: 'ajax',
                api: {
                    read: ajax_url+'ajax1.php'
                },
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    successProperty: 'success',
                    root: 'data',
                    messageProperty: 'message'
                },
                listeners: {
                    exception: function(proxy, response, operation){
                        Ext.MessageBox.show({
                            title: '错误信息',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            }
        });

//        var store = Ext.get('json');
//        alert(store);

        //将数组展示
        var itemsPerPage = 25;
        main = Ext.create('Ext.container.Container', {
            layout:'fit',
            renderTo: 'center1',
            items: [{
                itemId: 'grid',
                xtype: 'writergrid',
                title: '统计',
                flex: 1,
                store: store,
                bbar: Ext.create('Ext.PagingToolbar', {
                    store: store,
                    displayInfo: true,
                    displayMsg: '显示 {0} - {1} of {2}',
                    emptyMsg: "无数据显示"
                }),
                listeners: {
                    selectionchange: function(selModel, selected) {
                    }
                }
            }]
        });


    });
