<?php
/**
 * 公共模型类
 *
 * @author  michael
 */
class CommonCrud extends CI_Model {
    private $lg_comment = 'comment';
    private $lg_user = 'user';
    private $lg_user_info = 'user_info';
    private $lg_statistics = 'statistics';
    private $lg_user_detail = 'user_detail';
    private $lg_like = 'like';

    public function __construct() {
        parent::__construct();
    }

    /**
     * 查商品评论
     * 以后晒货，搭配 都需要查评论，所有放到顶层
     * @params  $type   评论类型，标示是商品，晒货，还是搭配
     * @params  $x_id   该类型中的主键，例如：晒货($type)中的那条, 商品($type)中的那条
     * @params  $offset   分页偏移量
     * @params  $num   每页多少条
     * @return  array
     */
    public function selComment($type, $x_id, $offset='0', $num='8') {
        $where = array('c_type' => $type, 'x_id' => $x_id, 'c_show'=>'1');

        return $this->db->select('lg_comment.uid,c_content,sex as c_sex,c_time,nick')->where($where)->join($this->lg_user_info,'lg_user_info.uid=lg_comment.uid')->order_by('c_id','desc')->get($this->lg_comment, $num, $offset)->result_array();
    }

    /**
     * 查询商品评论总数
     * @params  $type   评论类型，标示是商品，晒货，还是搭配
     * @params  $x_id   该类型中的主键，例如：晒货($type)中的那条, 商品($type)中的那条
     * @return  array
     */
    public function totalComment($type, $x_id) {
        $where = array('c_type' => $type, 'x_id' => $x_id);
        return $this->db->where($where)->select('c_id')->count_all_results($this->lg_comment);
    }

    /**
     * 用户信息查询(user_info表)   （很多模块都用，所以放到顶层）
     * 查用户信息，以及是否关注该用户
     * @author  michael
	 * @param	string $uid
	 * @return	array
     */
    public function getUserInfo($uid, $my_uid, $myAttention) {
        $lg_attention = 'lg_attention_'.$my_uid%10;

        //查 user_info 中的uid = 2 ,并且 lg_attention_中 att_uid =2 and uid = 30 的数据， 即使lg_attention_ 中没有符合条件的也要查出user_info的数据

    	$att_user_info = $this->db->select('*')->join($this->lg_statistics, 'user_info.uid=statistics.uid', 'left')->get_where($this->lg_user_info, array('user_info.uid' => $uid))->row_array();

        //确定这个用户是否是我关注的人
        $att_user_info['att_uid'] = in_array($uid, $myAttention)?'1':null;

    	return $att_user_info;
    }

    /**
     * 用户积分+1
     * @author  michael
	 * @param	string
	 * @param 	bool
	 * @return	array
     */
    public function addIntegral($uid) {
        $sql = "UPDATE `lg_user_detail` SET `integral` = integral+1 WHERE `uid` = '".$uid."'";
        return $this->db->query($sql);
    }

    /**
     * 修改防止用户头像出现浏览器缓存的时间戳
     * @author  michael
	 * @param	string
	 * @param 	bool
	 * @return	array
     */
    public function updateHeadTimestamp($uid, $field, $time) {
        return $this->db->update($this->lg_user_detail, array($field=>$time), array('uid'=>$uid));
    }

    /**
     * 获取头像时间戳
     * @params  $uid    当前用户uid
     * @return  int
     */
    public function getHeadTimestamp($uid) {
        return $this->db->select('head_time,head_crop_time')->where('uid', $uid)->get($this->lg_user_detail)->row_array();
    }



















    /**
     * 查询一张表的全部数据
     * @author  michael
	 * @param	string
	 * @param 	bool
	 * @return	array
     */
    public function selects($tb) {
        $rs = $this->db->get($tb);
        return $rs->result_array();
    }

    /**
     * 插入数据
     * @author  michael
	 * @param	string
	 * @param 	array
	 * @return	bool
     */
    public function inserts($tb, $data) {
        return $this->db->insert($tb, $data);
    }

    /**
     * 删除数据
     * @author  michael
	 * @param	string
	 * @param 	array or null
	 * @return	bool
     */
    public function deletes($tb, $condition = null) {
        return $this->db->delete($tb, $condition);
    }


    /**
     * 修改数据
     * @author  michael
	 * @param	string
	 * @param 	array or null
     * @param 	array or null
	 * @return	bool
     */
    public function updates($tb, $data = null, $condition = null) {
        return $this->db->update($tb, $data, $condition);
    }

}
?>