<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
require_once('TaobaoApi.php');

/**
 * 发布商品，晒或，秀搭配类
 * 1，商品分好多商城，在这里做分发，判读是那个商城的
 * 2，晒或
 * 3，搭配
 * question：
 *   1，taobaoapi类引入并且实例化后，model不能载入，原因：引入了自己的子类
 */
class Goods extends Common {
     public $taobao;
     private $dapei_goods = 'dapei_goods';
     private $show_goods = 'show_goods';

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
        if(empty($this->user_info['a_id'])) {
            die('login common!');
            redirect('admin.php/login', 'location');die;
        }
    }

    /**
     * 默认入口
     */
    public function index() {
        die('goods index!');
    }

    /**
     * 默认入口
     */
    public function indexs() {

        $this->smart->display('taobao/ad.tpl');
    }

    /**
     *
     */
    public function te() {
        echo('asdf!');
    }

	/**
	 * 获取商品（商品分发）
	 *
     */
	public function getGoods() {
        $this->benchmark->mark('code_start');
        //根据链接判断是那个商家
        $url = $this->input->post('goodsurl');

        if(strpos($url,'taobao.com') || strpos($url,'tmall.com')) {
            $reg = '/id=(\d+)/';
            preg_match($reg,$url,$goodsid);

            if( isset($goodsid['1']) && !empty($goodsid['1']) && is_numeric($goodsid['1']) ) {
                TaobaoApi::taobaoApiGet($goodsid['1']);

            } else {
                die('获取id有误');
            }

        } else {
            die('其他的dianshang');
        }
	}

	/**
	 *  晒或
	 *
     */
	public function showGoods() {
        //晒货数据
        $data['s_img_num'] = $this->input->post('s_img_num');
        if($data['s_img_num'] > 0) {
            $data['s_addtime'] = $this->input->post('s_img_date');
            $data['s_info'] = $this->input->post('show_info');
            $data['uid'] = $this->user_info['uid'];

            $arr = $this->insertGoods('1','1');
            if(!is_null($arr)) {
                $data['s_label'] = $arr['label'];
                $ds['g_id'] = $arr['insert_id'];
                $ds['ds_tb'] = empty($arr['label'])?2:1;
            }

        } else {
            die('请上传晒货图片!');
        }

        if($this->db->insert($this->show_goods, $data)) {
            if(isset($ds['g_id'])) {
                $ds['ds_id'] = $this->db->insert_id();
                $ds['g_uid'] = $ds['ds_uid'] = $this->user_info['uid'];
                $ds['ds_type'] = '1';

                $this->db->insert($this->dapei_goods, $ds);
            }

            redirect('center', 'location');die;
        }
    }

	/**
	 *  搭配
	 *
     */
	public function daGoods() {
        //搭配商品的数量
        $da_goods_num = $this->input->post('da_goods_num');
        $da_img = $this->input->post('da_img_num');
        if($da_img <= 0) {
            die('没有搭配!');
        }

        if($da_goods_num <= 6) {
            for($i=1; $i<$da_goods_num; $i++) {

                $arr = $this->insertGoods($i,'1');

                if(!is_null($arr)) {

                    $data['g_id'][] = $arr['insert_id'];
                    $data['label'][] = $arr['label'];

                    if(empty($arr['label'])) {
                        $laji_gid[] = $arr['insert_id'];
                    }
                }
            }

            $da['uid'] = $this->user_info['uid'];
            $da['da_info'] = $this->input->post('da_info');
            $da['da_img_num'] = $da_img;
            $da['da_lable'] = empty($data['label'])?'':implode('',array_unique($data['label']));
            $da['da_addtime'] = $this->input->post('da_img_date');

            if($this->db->insert('dapei', $da)) {

                $daf['ds_id'] = $this->db->insert_id();
                $daf['ds_uid'] = $this->user_info['uid'];
                $daf['g_uid'] = $this->user_info['uid'];
                $daf['ds_type'] = '2';
                //把空值过滤掉

                if(isset($data['g_id'])) {
                    foreach($data['g_id'] as $key=>$value) {
                        if(isset($laji_gid) && in_array($value,$laji_gid)) {
                            $daf['ds_tb'] = '2';
                        }
                        $daf['g_id'] = $value;
                        $this->db->insert($this->dapei_goods, $daf);
                    }
                }

                redirect('center', 'location');die;
            }

        } else {
            die('商品过多！!');
        }

    }

	/**
	 * 添加商品，将商品插入数据库
	 *
     */
    public function insertGoods($n='1', $flag='') {
//        $this->benchmark->mark('code_start');

        $goods_info = $this->input->post('goods_info_'.$n);
        $datetime = $this->input->post('datetime_'.$n);
        $cid = $this->input->post('cid_'.$n);
        $title = $this->input->post('title_'.$n);
        $num_iid = $this->input->post('numiid_'.$n);
        $price = $this->input->post('price_'.$n);
        $url = $this->input->post('url_'.$n);

        if(!empty($num_iid) && !empty($cid) && !empty($title) && !empty($datetime) && !empty($price)) {

            //没有淘宝客连接，将商品的直接链接存起来
            if(empty($url)) {
                $url = $this->input->post('detail_url_1');
            }

            //对数据过滤
            $itemcatsGet = TaobaoApi::getBigclass($cid,$title);

            if(isset($itemcatsGet['label'])) {
                $tb = 'laji_goods';
                //没有正常返回大类数据，也要将商品信息插入到数据库
                $data = array(
                    'uid' => $this->user_info['uid'],
                    'seller' => 'taobao',
                    'num_iid' => $num_iid,
                    'la_title' => $title,
                    'la_intro' => $goods_info,
                    'la_label' => $itemcatsGet['label'],
                    'la_url' => $url,
                    'la_addtime'=> $datetime
                );

            } else {
                //确定lable
                $labels = substr($itemcatsGet['name'],0,-1);
                $labels = explode('#',$labels);

                $this->load->model('GoodsModel');
                $label_id = $this->GoodsModel->labels($labels);

                $label_idstr = '';
                foreach($label_id as $key=>$value) {
                    $label_idstr .= '#'.$value;
                }

                //如果不是垃圾，就判断标签，去label表中将label查出，如果存在就获取label的id，不存在的就插入数据库并且返回id
                $tb = 'goods';
                $data = array(
                    'uid' => $this->user_info['uid'],
                    'seller' => 'taobao',
                    'num_iid' => $num_iid,
                    'g_title' => $title,
                    'g_intro' => $goods_info,
                    'g_label' => $label_idstr,
                    'g_type' => $itemcatsGet['g_type'],
                    'g_sex' => $itemcatsGet['g_sex'],
                    'price' => $price,
                    'g_url'=> $url,
                    'g_addtime'=> $datetime
                );
            }

            if($this->db->insert($tb, $data)) {

    //            $this->benchmark->mark('code_end');
    //            $ttiem = $this->benchmark->elapsed_time('code_start', 'code_end');

                if($flag == '1') {
                    $arr['insert_id'] = $this->db->insert_id();
                    $arr['label'] = isset($label_idstr)?$label_idstr:'';
                    return $arr;
                } else {
                    if($tb == 'laji_goods') {
                        echo "<script>alert('ok,到laji了');window.location.href='http://lovego.sinaapp.com'</script>";die;
                    } else {
                        echo "<script>alert('商品添加ok');window.location.href='http://lovego.sinaapp.com'</script>";die;
                    }
                }

            }

        } else if(!empty($flag)) {
            return null;
        } else {
            die('没有商品!');
        }

    }

	/**
	 *  上传图片
	 *
     */
	public function uploadImg() {
//        $this->benchmark->mark('code_start');

        $s = new SaeStorage();

        if($date = $this->input->post('show_img_date')) {
            //晒货
            $img_num = $this->input->post('img_num');
            $ext = '_s'.$img_num;

        } else if($date = $this->input->post('d_img_date')) {
            //搭配
            $img_num = $this->input->post('d_img_num');
            $ext = '_d'.$img_num;
        }

        //将文件存到临时目录下，生成缩略图后将临时文件删除
        /**/
        $fdir = $this->makeUserDir($this->user_info['uid'], '', '', '1');
        $tmp_img = $fdir.$this->user_info['uid'].'/tmp_img/'.md5($date).'.jpg';
        $rs = $s->upload('lovegoimg', $tmp_img, $_FILES['fileimg']['tmp_name']);
        $tmp_url = $s->getUrl('lovegoimg', $tmp_img);

//            $arr = array('tmp_file'=>$_FILES['fileimg']['tmp_name'],'ext'=>'_s'.$img_num,'date'=>$date);
        if($tmp_url) {
            echo $this->resizeGoodsImg($tmp_url, $date, $ext);
        }
        $s->delete('lovegoimg',$tmp_img);


//        $rs = $s->upload('lovegoimg',$img_name,$_FILES['fileimg']['tmp_name']);

//        echo $s->getUrl( 'lovegoimg' , $img_name );

//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
/*
        $config['upload_path'] = '/upload';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '100';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';var_dump($error);die;

        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            echo '<pre>';var_dump($data);die;
        }*/


    }

}
?>