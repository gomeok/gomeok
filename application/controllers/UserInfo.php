<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Common.php');
/**
 *  查询 or 用户资料修改
 */
class UserInfo extends Common {
    public $lg_attention = 'attention';
    public $lg_be_attention = 'be_attention';

    /**
     *  构造
     */
    public function __construct() {
        parent::__construct();

    }

    /**
     *
     */
    public function index() {
        die('test');
    }

    /**
     * 修改用户基本资料         todo:加验证
     */
    public function userInfo() {
        //判断用户是否登录
        $this->isLogin();
//$this->db->insert('lg_user_detail',array('uid'=>30));
//die('teste!');

   		$error=array();
		$this->load->model('ModModel');

    	if($this->input->post('submit')) {
			$nick = $this->input->post('nick');         //todo：加验证
			$sex = $this->input->post('sex');

			$province = $this->input->post('province');//省，市
			$city = $this->input->post('city');//市，县

			$year = $this->input->post('year');
            $month = $this->input->post('month');
            $day = $this->input->post('day');
            $data['birthday'] = $year.'-'.$month.'-'.$day;  //出生年月日

			$data['age'] = date('Y')-$year;//年龄

			//$data['constellation'] = $this->input->post('constellation');//星座
			$data['high'] = $this->input->post('high');//身高
			$data['weight'] = $this->input->post('weight');//体重
			$data['chest'] = $this->input->post('chest');//胸围
			$data['waist'] = $this->input->post('waist');//腰围
			$data['hip'] = $this->input->post('hip');//臀围
			$data['sina_weibo'] = $this->input->post('sina_weibo');//个人博客sina
			$data['career'] = $this->input->post('career');//职业

			$lableStr = $this->input->post('label');//个人标签
			$lableArr = explode(',',$lableStr);
			$labelid = '';
			$label = array();
			foreach ($lableArr as $key => $value){
				if(isset($k)){$k=$k+1;}else {$k=$key+1;}
				if($value==''){$k-=1;continue;}
				if($k>0){//label有值
					$l_id = $this->ModModel->selLabel($value);
					if(!isset($l_id)){
						$label[$k]['label']=$value;
						$label[$k]['type']=1;
					}else {
						$labelid.=$l_id.",";
					}
				}
			}//echo 'end';die;
			$labelid .= $this->ModModel->insertLabels($label);
			$label2 = substr($labelid,0,-1);
			//var_dump($data);die;

			$sign = $this->input->post('sign');//自我介绍
			//var_dump($this->user_info);die;

			$rows = $this->ModModel->selNick(array('nick' => $nick));
			if($rows>0 && $nick!=$this->user_info['nick']){//用户昵称已存在
				$error['nick']='nick';
			}else{
				$rs = $this->ModModel->updateUserInfo($this->user_info['uid'],array('nick'=>$nick,'sex'=>$sex,'province'=>$province,'city'=>$city,'label'=>$label2,'sign'=>$sign));//更新用户信息
				$rsdetail = $this->ModModel->updateUserDetail($this->user_info['uid'],$data);//更新用户详细信息
				//var_dump($rsdetail);die;
				$rsinfo = $this->ModModel->selUserInfo(array('email' => $this->user_info['email'],'password'=>$this->user_info['password']));
				//var_dump($rsinfo);die;
				$this->user_info = $rsinfo;
				$this->ksession->set($rsinfo);
				redirect('/', 'location');die;
			}
		}//if end

		$userallinfo = $this->ModModel->selAllInfo($this->user_info['uid']);

		$ymd = explode('-',$userallinfo["birthday"]);
		$userallinfo['year'] = $ymd[0];
		$userallinfo['month'] = $ymd[1];
		$userallinfo['day'] = $ymd[2];

		$lbid = explode(',',$userallinfo['label']);
		$labelname="";
		foreach ($lbid as $key => $value){
			$labelname .= $this->ModModel->selLabel($value,'label').",";
		}
		$userallinfo['label'] = substr($labelname,0,-1);
		//var_dump($lbid,$userallinfo);die;
		$this->smart->assign('error',$error);
		$this->smart->assign('userinfo',$this->user_info);
		$this->smart->assign('userallinfo',$userallinfo);//var_dump($this->ModModel->selAllInfo($this->user_info['uid']));die;
        $this->smart->display('user_info.tpl');
    }

    /**
     * 修改用户头像
     * 修改头像的页面是否可以和上传的放到一起
     */
    public function userHeadPhoto() {
        //只有等了才能访问
        $this->isLogin();

        //获取用户头像目录
        $fdir = $this->makeUserDir($this->user_info['uid']);
        $path = 'upload/'.$fdir;

        $this->load->model('UserInfoModel');
        $headTimeArr = $this->UserInfoModel->getHeadTimestamp($this->user_info['uid']);

        if(isset($_FILES['userfile'])) {    //上传
            $newHeadTime = time();

            $fileName = $this->user_info['uid'].'_'.$newHeadTime.'.jpg';
            //上传图片
            $imgUrl = $this->uploadImg($path, $fileName);
            if(!is_array($imgUrl)) {    //不是数组，说明上传成功

                //将旧的头像照删除
                unlink($path.$this->user_info['uid'].'_'.$headTimeArr['head_time'].'.jpg');

                //更新头像时间戳
                $this->UserInfoModel->updateHeadTimestamp($this->user_info['uid'], 'head_time', $newHeadTime);

            } else {
                die('修改头像上传失败!');
            }

            $this->smart->assign('img_url',$imgUrl);

        } else if($this->input->post('crop_head')) {   //裁剪并缩略图片

            //要截的图的宽和高（在原始大小的基础上）                              //todo:数据验证
            $w = $this->input->post('w');
            $h = $this->input->post('h');
            //左上角的x,y点
            $mleft = $this->input->post('mleft');
            $mtop = $this->input->post('mtop');

            $newHeadCropTime = time();

            $headPath =  $path.$this->user_info['uid'].'_'.$headTimeArr['head_time'].'.jpg';   //原始图
            $cropImgName = $path.$this->user_info['uid'].'_'.$newHeadCropTime;

            $cropOk = $this->imgDealWith('crop', $headPath, $cropImgName.'_c.jpg', false, $w, $h, $mleft, $mtop);

            if(!is_array($cropOk)) {
                $this->imgDealWith('resize', $cropImgName.'_c.jpg', $cropImgName.'_100.jpg', true, 100, 100);
                $this->imgDealWith('resize', $cropImgName.'_c.jpg', $cropImgName.'_50.jpg', true, 50, 50);

                //将旧的头像截图删除
                $tmepCropPath = $path.$this->user_info['uid'].'_'.$headTimeArr['head_crop_time'];
                unlink($tmepCropPath.'_50.jpg');
                unlink($tmepCropPath.'_100.jpg');
                unlink($tmepCropPath.'_c.jpg');

                //图片路径写入session
                $this->ksession->set('head_img', $this->config->item('base_url').$cropImgName.'_50.jpg');

                //更新头像时间戳
                $this->UserInfoModel->updateHeadTimestamp($this->user_info['uid'], 'head_crop_time', $newHeadCropTime);

                redirect('/center/'.$this->user_info['uid'], 'location');

            } else {
                die('头像截图失败!');
            }

        } else {
            //获取用户的头像图片(原始图)
            $headImg = $path.$this->user_info['uid'].'_'.$headTimeArr['head_time'].'.jpg';
            if(file_exists($headImg)) {
                $this->smart->assign('img_url',$headImg);
            }
        }

        $this->smart->display('user_head_photo.tpl');
    }

    /**
     * 修改密码
     */
    public function userPassword() {
        //判断用户是否登录
        $this->isLogin();

    	$error=array();
    	if($this->input->post('submit')){
    		$oldpwd = $this->input->post('oldpwd');
    		$newpwd = $this->input->post('newpwd');
    		$repeatpwd = $this->input->post('repeatpwd');
    		if($newpwd==$repeatpwd){
				$this->load->model('ModModel');
	    		$userinfo = $this->ModModel->selUserInfo(array('uid'=>$this->user_info['uid'],'password'=>$oldpwd));
	    		//var_dump($userinfo);echo 'hello';
	    		if($userinfo){
	    			$rs = $this->ModModel->updateUserInfo($this->user_info['uid'],array('password'=>$newpwd),'user');
	    			$rsinfo = $this->ModModel->selUserInfo(array('uid'=>$this->user_info['uid'],'password'=>$newpwd));
					$this->user_info = $rsinfo;
					$this->ksession->set('password',$rsinfo['password']);
					redirect('/', 'location');die;
	    		}else {//原密码输入错误
	    			$error['oldpwd']='oldpwd';
	    		}
    		}else {//两次密码不一致
    			$error['repeatpwd']='repeatpwd';
    		}
    	}
		//$this->smarty->assign('p',$p);
		$this->smart->assign('error',$error);
        $this->smart->display('user_password.tpl');
    }

    /**
     * 合作登录
     */
    public function authorize() {
        //判断用户是否登录
        $this->isLogin();

//        $this->smarty->assign('p',$p);
        $this->smart->display('auth.tpl');
    }

    /**
     * 用户资料查询 ajax(不登录可以访问)
     * @author  michael
	 * @echo    json
     */
    public function getUserInfo() {
        //要查的用户的uid
        $uid = $this->input->get('uid');    //todo:验证

        //关注的uid ，查出来放到redis
        $myAttention = $this->redisAttention($this->user_info['uid']);

        $this->load->model('UserInfoModel');
		$userallinfo = $this->UserInfoModel->getUserInfo($uid, $this->user_info['uid'], $myAttention);
        //获取用户头像
        $userDir = $this->makeUserDir($uid);
        $userallinfo['head_img'] = $this->config->item('base_url').'upload/'.$userDir.$uid.'_50.jpg';

        echo json_encode($userallinfo);
    }

    /**
     * 处理加关注 ajax(不登录不可以访问)
     *
     * @author  michael
	 * @echo    int 0 未登录 1 关注成功 2 不能关注自己
     */
    public function attention() {

        //判断用户是否登录
        if(!$this->isLogin(true)) {
            echo 0;die;
        }

        //要关注的好友
        $att_uid = $this->input->get('uid');
        //自己不能关注自己
        if($this->user_info['uid'] == $att_uid) {
            echo 2;die;
        }

        $this->load->model('UserInfoModel');
        //关注
		echo $this->UserInfoModel->attention($this->user_info['uid'], $att_uid);
    }

    /**
     * 处理取消关注 ajax(不登录不可以访问)
     * @author  michael
	 * @echo    json
     */
    public function delAttention() {
        //判断用户是否登录
        if(!$this->isLogin(true)) {
            echo 0;die;
        }

        //要关注的好友
        $att_uid = $this->input->get('uid');

        $this->load->model('UserInfoModel');
        //关注
		echo $this->UserInfoModel->delAttention($this->user_info['uid'], $att_uid);
    }


    /**
     * 私信
     */
    public function showpmsg() {
//        $this->smart->assign('p',$p);
		$userinfo = $this->user_info;
		$this->load->model('PmsgModel');
		$this->load->model('ModModel');
		$pmsg = $this->PmsgModel->selMsg($userinfo['uid']);
    	foreach ($pmsg as $key => $value) {
    		$uinfo = $this->ModModel->selNick(array('uid' =>$value['uid']),false);
    		$pmsg[$key]['uname'] = $uinfo[0]['nick'];
    	}
		$this->smart->assign('userinfo',$userinfo);
		$this->smart->assign('pmsg',$pmsg);
        $this->smart->display('pmsg.tpl');
    }

    /**
     * 用户@我的
     *
     * @michael
     */
    public function showAtMe() {

        //获取分页查询的偏移量
        $pageOffset = $this->input->get('page');      //todo: 数据验证
        if(empty($pageOffset)) {
            $pageOffset = 0;
        }

        $this->load->model('UserInfoModel');

        $at = $this->UserInfoModel->getAtMe($this->user_info['uid'], $pageOffset);
        $this->smart->assign('atList',$at);

        //查总数
        $totalAtMe = $this->UserInfoModel->totalAtMe($this->user_info['uid']);

        $pageStr = $this->pages('atme?', $totalAtMe, 10, true);
        $this->smart->assign('pageStr', $pageStr);

        //将该用户的is_look修改为0 已看, 修改成功之后，将session里的值修改掉
        if($this->UserInfoModel->updateAtMe($this->user_info['uid'])) {
            $this->ksession->set('atNum', '0');
        }

        $this->smart->display('atme.tpl');
    }

}
?>