<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
/**
 * 入口类
 */
class Welcome extends Common {
    //public $sm;

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
        //判断是否登录，没有登录就去登录窗口
        $this->user_info = $this->ksession->getAll();
        if(empty($this->user_info['a_id'])) {
            $this->loginWindow();
//            redirect('login', 'location');die;
        }

    }

	/**
	 * 首页展示
	 *
     */
	public function welcomes() {
        $this->benchmark->mark('code_start');   //ci基准测试

        $this->smart->display('index.tpl');

        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
	}

    /**
     * tree
     * //todo:将tree存成静态文件
     */
    public function tree() {
        //tree的数组必须是固定格式的
        $json = array(
            array(
                'text'=>'商品管理',
                "expanded"=>true,
                //'cls'=>"folder",
                'id'=>'test',
                "children"=>array(
                    array('id'=>'menu_1','text'=>'商品列表', 'leaf'=>true, 'url'=>'GoodsAction/goodsList'),
                    array('id'=>'menu_2','text'=>'this is a test', 'leaf'=>true, 'url'=>'welcome/addTab')
                )
            )
        );
        echo json_encode($json);
    }

    /**
     * data
     */
    public function datas() {
        $json = array(
                'data'=>array(array('id'=>'1','name'=>'2'), array('id'=>'2','name'=>'3'),array('id'=>'4','name'=>'5')),
                'total'=>'12',
                'message'=>'ok',
                'success'=>'1'
                );
        echo json_encode($json);
    }

    /**
     * add tab test
     */
    public function addTab() {

        if($this->input->is_ajax_request()) {
            $json = array(
                'data'=>array(array('id'=>'1','name'=>'www'), array('id'=>'2','name'=>'asdf'),array('id'=>'4','name'=>'wewewe')),
                'total'=>'12',
                'message'=>'ok',
                'success'=>'1'
                );
            echo json_encode($json);die;
        }

        $this->smart->display('t.html');
    }

    /**
     * login
     */
    public function loginWindow() {

        $this->smart->display('login.tpl');die;
    }






    /**
     * 默认入口
     */
    public function index() {
        $this->smart->display('gl.tpl');
    }

	/**
	 * 首页展示
	 *
     */
	public function top() {
        $this->smart->display('top.tpl');
    }

    /**
	 *
	 *
     */
	public function left() {
        $this->smart->display('left.tpl');
    }

    /**
	 *
	 *
     */
	public function right() {
        $this->smart->display('right.tpl');
    }

	/**
	 * 登录
	 *
     */
    public function login() {

        $error = array();

        if($this->input->post('submit')) {

            $a_nick = $this->input->post('a_nick');
            $a_password = $this->input->post('a_password');

            //正则验证

            //查数据库验证
            $this->load->model('WelcomeModel');
            $rs = $this->WelcomeModel->login($a_nick,$a_password);

            if($rs) {
                $this->ksession->set($rs);
                $this->index();die;
//                redirect($this->aweburl.'index', 'location');die;
            } else {
                $error = 'error';
            }
        }

        $this->smart->assign('error',$error);
        $this->smart->display('login.tpl');
    }

	/**
	 * 退出登录
	 *
     */
    public function loginOut() {
        $this->ksession->sessUnset('uid');
        redirect($this->aweburl, 'location');die;
    }


}
?>