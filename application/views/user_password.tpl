{!config_load file="web.inc.conf"!}
{!include file="inc/header.tpl"!}

{!include file="inc/other.tpl"!}

<div class="clear"></div>
<div class="banner in-width1">
<img src="{!#img_path#!}banner1.jpg"  height="301" width="955" border="0" />
</div>
<div class="footbg in-width"></div>
</div>
<!--top end-->

<!--content begin-->
<div id="content">
<div class="topbg"></div>
<div class="content1-m middlebg clearfix">
<div class="ziliao-l blue-borderl fl">
<ul>
<li><a href="{!#weburl#!}modinfo">基本资料</a></li>
<li><a href="{!#weburl#!}modhead">修改头像</a></li>
<li><a class="bluebg" href="{!#weburl#!}modpswd">修改密码</a></li>
<li><a href="{!#weburl#!}auth">应用授权</a></li>
<li><a href="">买件认证</a></li>
<li><a href="">手机认证</a></li>
</ul>
</div>

<form class="updateForm" action="{!#$weburl#!}modpswd" method="post" id="userinfock">
<div class="ziliao-r fr">
<table width="500" class="pass-c1" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="42" align="right">旧密码<font class="red">*</font>：</td>
    <td height="42"><input class="pass-width" name="oldpwd" type="text" />
	{!if isset($error['oldpwd'])!}
	<span class='red'>对不起，您的原密码输入有误！</span>
	{!/if!}
	</td>
  </tr>
  <tr>
    <td height="42" align="right">新密码<font class="red">*</font>：</td>
    <td height="42"><input class="pass-width" name="newpwd" type="text" />
	{!if isset($error['repeatpwd'])!}
	<span class='red'>对不起，您两次输入的密码不一致！</span>
	{!/if!}
	</td>
  </tr>
  <tr>
    <td height="42" align="right">重复新密码<font class="red">*</font>：</td>
    <td height="42" align="left"><input class="pass-width" name="repeatpwd" type="text" /></td>
  </tr>
</table>
<div class="baocun"><input value="保存设置" class="baocuninput bluebg" name="submit" type="submit" /></div>
</form>
</div>
</div>
<div class="footbg"></div>
</div>
<!--content end-->

<!--footer begin-->
{!include file="inc/footer.tpl"!}
<!--footer end-->
</body>
</html>