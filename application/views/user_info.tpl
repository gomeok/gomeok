{!config_load file="web.inc.conf"!}
{!include file="inc/header.tpl"!}

{!include file="inc/other.tpl"!}

<div class="banner in-width1">
<img src="{!#img_path#!}banner1.jpg"  height="301" width="955" border="0" />
</div>
<div class="footbg in-width"></div>
</div>
<!--top end-->
<script type="text/javascript">
$(function() {
    birthdate('#year', '#month', '#day');
    provlist('#province', '#city');

    publicselect([
		{idn:'#job', data:'JOB'}
	]);

    //userinfo 表单验证
    ckform('userinfock',
        [
        { name:'nick',msg:'昵称',min:6,max:12},
        { name:'year',msg:'年'},
        { name:'month',msg:'月'},
        { name:'day',msg:'天'},
        { name:'high',type:'number',msg:'身高'},
        { name:'weight',type:'number',msg:'体重'},
        { name:"province",msg:"省" },
        { name:"city",msg:"城市" }
        ]
    );
});
</script>
<!--content begin-->
<div id="content">
<div class="topbg"></div>
<div class="content1-m middlebg clearfix">
<div class="ziliao-l blue-borderl fl">
<ul>
<li><a class="bluebg" href="{!#weburl#!}modinfo">基本资料</a></li>
<li><a href="{!#weburl#!}modhead">修改头像</a></li>
<li><a href="{!#weburl#!}modpswd">修改密码</a></li>
<li><a href="{!#weburl#!}auth">应用授权</a></li>
<li><a href="">买件认证</a></li>
<li><a href="">手机认证</a></li>
</ul>
</div>
<div class="ziliao-r fr">

<form class="updateForm" action="{!#weburl#!}modinfo" method="post" id="userinfock" >
<div class="ziliaor1">昵　　称：<input class="input2" name="nick" type="text" value="{!$userinfo.nick!}" /><font class="red">*</font>
	{!if isset($error['nick'])!}
	<span class='red'>该昵称太受欢迎了哦，请您重新输入</span>
	{!/if!}
</div>
<div class="ziliaor2">邮　　箱：{!$userinfo.email!}</div>
<div class="ziliaor3">性　　别：
    <input name="sex" type="radio" value="1" {!if $userinfo.sex==1 !} checked {!/if!}/>男
    <input name="sex" type="radio" value="2" {!if $userinfo.sex==2 !} checked {!/if!} />女
    <!-- <input name="sex" type="radio" value="2" {!if $userinfo.sex==2 !} checked {!/if!} />保密 -->
</div>
<div class="ziliaor4">生　　日：
    <select name="year" id="year" rel="{!$userallinfo.year!}">
        <option value="">请选择年</option>
    </select>
    <select name="month" id="month" rel="{!$userallinfo.month!}">
        <option value="">请选择月</option>
    </select>
    <select name="day" id="day" rel="{!$userallinfo.day!}">
        <option value="">请选择天</option>
    </select>
</div>
<div class="ziliaor4">所 在 地：
    <select name="province" id="province" rel="{!$userallinfo.province!}">
        <!--<option></option>-->
    </select>

    <select name="city" id="city" rel="{!$userallinfo.city!}">
        <option>请选择市/区</option>
    </select>
    <!--
    <select name="" id="area">
        <option>罗山县</option>
    </select>  -->
</div>
<!-- 
<div class="ziliaor4">星　　座：
    <select name="constellation">
        <option>摩羯座</option>
    </select>
</div>
 -->
<div class="ziliaor4 clearfix">
<div class="fl">身　　高：<input class="input3" name="high" type="text" maxlength="3" value="{!$userallinfo.high!}" />cm <font></font></div>
<!-- 
<div class="fr">
    <select name="">
    <option>关注我的人可见</option>
    </select>
</div>
 -->
</div>
<div class="ziliaor4 clearfix">
<div class="fl">体　　重：<input class="input3" name="weight" type="text" maxlength="3" value="{!if !empty($userallinfo.weight)!}{!$userallinfo.weight!}{!/if!}" />kg <font></font></div>
<!--
<div class="fr">
    <select name="">
        <option>关注我的人可见</option>
    </select>
</div>
  -->
</div>
<div class="ziliaor4 clearfix">
<div class="fl">胸　　围：
        <input class="input4" name="chest" type="text" maxlength="3" value="{!if !empty($userallinfo.chest)!}{!$userallinfo.chest!}{!/if!}" />cm&nbsp;&nbsp;
        <input class="input4" name="waist" type="text" maxlength="3" value="{!if !empty($userallinfo.waist)!}{!$userallinfo.waist!}{!/if!}"/>cm&nbsp;&nbsp;
        <input class="input4" name="hip" type="text" maxlength="3" value="{!if !empty($userallinfo.hip)!}{!$userallinfo.hip!}{!/if!}"/>cm</div>
<!-- 
<div class="fr">
    <select name="">
        <option>关注我的人可见</option>
    </select>
</div>
 -->
</div>
<div class="ziliaor2">个人博客：<input class="input2" name="sina_weibo" type="text" value="{!$userallinfo.sina_weibo!}" /></div>
<div class="ziliaor4 ">职　　业：
    <select name="career" id='job' rel="{!$userallinfo.career!}">
        <option>填写的职业</option>
    </select>
</div>
<div class="ziliaor5">
个人标签：<input type="text" name="label" value="{!$userallinfo.label!}" class="input2">
<p class="geren-word"><span class="blue">上网</span><span class="blue">八卦</span><span class="blue">甜美</span></p>
</div>
<div class="ziliaor6 clearfix">
<p class="fl">自我介绍：</p>
<div class="fl"><textarea class="textarea1" name="sign" cols="" rows="">{!if isset($userallinfo.sign)!}{!$userallinfo.sign!}{!else!}写点东西，让别人更加了解你把！{!/if!}</textarea></div>
</div>
<div class="ziliaor6"><input class="input5 bluebg" value="确定" name="submit" type="submit" /></div>
</form>


</div>
</div>
<div class="footbg"></div>
</div>
<!--content end-->

<!--footer begin-->
{!include file="inc/footer.tpl"!}
<!--footer end-->
</body>
</html>