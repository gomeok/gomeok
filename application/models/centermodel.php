<?php
require_once('CommonCrud.php');

/**
 * 用户中心 动态模型
 * @author  michael
 */
class CenterModel extends CommonCrud {

    public $lg_dongtai = 'lg_dongtai';
    public $lg_goods = 'lg_goods';
    public $lg_like = 'lg_like';

    public function __construct() {
        parent::__construct();
    }

    /**
     * 查用户动态
     *
     */
    public function getDynamic($uid) {
//        echo '<pre>';var_dump($uid);die;
        //这里要获取当前登录用户的关注，然后再去查关注的人的动态
    	$rs = $this->db->select('did,uid,content,d_time,zf_num,comment_num')->order_by('did','desc')->get_where($this->lg_dongtai, array('uid'=>$uid))->result_array();
        //echo $this->db->last_query();die;//'<pre>';var_dump($r);die;

         return $rs;
    }

    /**
     * 我的商品
     *
     */
    public function getMyGoods($uid, $goodsType, $offset=0, $limit=2) {
        //当查全部我的全部商品数据时，goodsType = 0
        if(!empty($goodsType)) {
            $this->db->where('g_type', $goodsType);
        }
    	return $this->db->select('*')->order_by('g_id','desc')->get_where($this->lg_goods, array('uid'=>$uid), $limit, $offset)->result_array();
    }

    /**
     * 我的商品的总数
     *
     */
    public function totalMyGoods($uid, $goodsType) {
        //当查全部我的全部商品数据时，goodsType = 0
        if(!empty($goodsType)) {
            $this->db->where('g_type', $goodsType);
        }
    	return $this->db->select('g_id')->where('uid',$uid)->count_all_results($this->lg_goods);
    }

    /**
     * 我喜欢的商品
     *
     */
    public function getMyLikeGoods($goodsType, $likeGoodsGid, $offset=0, $limit=2) {
        //当查全部我的全部商品数据时，goodsType = 0
        if(!empty($goodsType)) {
            $this->db->where('g_type', $goodsType);
        }
    	return $this->db->select('*')->order_by('g_id','desc')->where_in('g_id',$likeGoodsGid)->limit($limit, $offset)->get($this->lg_goods)->result_array();
    }

    /**
     * 我喜欢的商品总数，可以按类型分查总数
     *
     */
    public function totalMyLikeGoods($goodsType, $likeGoodsGid) {
        //当查全部我的全部商品数据时，goodsType = 0
        if(!empty($goodsType)) {
            $this->db->where('g_type', $goodsType);
        }
    	return $this->db->select('g_id')->where_in('g_id',$likeGoodsGid)->count_all_results($this->lg_goods);
    }

}
?>