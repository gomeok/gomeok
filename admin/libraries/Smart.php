<?php
require(APPPATH.'libraries/Smarty-3.1.7/libs/Smarty.class.php');

class Smart extends Smarty {

    public function __construct() {
        parent::__construct();


//        $path="saemc://templates_c";//使用MC Wrapper
//        mkdir($path);
        $this->left_delimiter = "{!";
        $this->right_delimiter = "!}";
        $this->compile_dir = APPPATH.'views/templates_c/';
        $this->template_dir = APPPATH.'views/';
        $this->cache_dir = APPPATH.'cache/';
        $this->config_dir = APPPATH.'config/';

    }

}
?>