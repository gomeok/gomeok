{!config_load file="web.inc.conf"!}
{!include file="inc/header.tpl"!}
<link href="{!#css_path#!}login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{!#js_path#!}globalform.js"></script>
<script type="text/javascript" src="{!#js_path#!}jquery.ckform.js"></script>

<link href="{!#css_path#!}jqxjcss/jquery.yitip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{!#js_path#!}jqxj/jquery.yitip.js"></script>

<!--
<div class="banner in-width1">
<img src="images/banner1.jpg"  height="301" width="955" border="0" />
</div>
  -->
<div class="footbg in-width"></div>
</div>
<!--top end-->
<script type="text/javascript">
$(function() {

    var error = {!$jsonError!}, msg, obj;
    $.each(error, function (i,v) {
        if (i=='email') {
            if (v==1) {
                msg = '邮箱不可以为空';
            } else if (v==2) {
                msg = '邮箱格式不正确';
            } else if (v==3) {
                msg = '邮箱已被注册';
            }

        } else if (i=='nick') {
            if (v==1) {
                msg = '昵称为空';
            } else if (v==2) {
                msg = '昵称格式不正确';
            } else if (v==3) {
                msg = '昵称已存在';
            }

        } else if (i=='sex') {
            msg = '您是同志吗？';

        } else if (i=='password1') {
            if (v==1) {
                msg = '密码不可以为空';
            } else if (v==2) {
                msg = '密码长度不正确';
            } 

        } else if (i=='password2') {
            if (v==1) {
                msg = '确认密码不可以为空';
            } else if (v==2) {
                msg = '两次密码输入不一致';
            } 

        } else if (i=='birth') {
            i = 'day';
            msg = '生日没有选择'; 

        } else if (i=='place') {
            i = 'city';
            msg = '所在地没有选择'; 

        }
        obj = $('[name='+i+']');
        if (!obj.next().is('.cor')) {
            obj.after('<font class="cor"><font></font><span class="cor1"><span class="cor2"></span></span></font>');
        }

        obj.next().show();
        obj.next().find('font').html(msg).css('color','red');
    });


    birthdate('#year', '#month', '#day');
    provlist('#province', '#city');
    /*
    publicselect([
		{idn:'#job', data:'JOB'}
	]);
*/
        $('#id_password1').focus(function(){
            if($(this).val()=="输入密码"){
                $(this).val('');
            }
            this.type = 'password';
        });
        $('#id_password1').blur(function(){
            if($(this).val()==""){
                this.type = 'text';
                $(this).val("输入密码");
            }
        });
        $('#id_password2').focus(function(){
            if($(this).val()=="输入确认密码"){
                $(this).val('');
            }
            this.type = 'password';
        });
        $('#id_password2').blur(function(){
            if($(this).val()==""){
                this.type = 'text';
                $(this).val("输入确认密码");
            }
        });

        $('.diqu').find('select').hover(
            function () {
                $(this).find('option').css('color','black');
            }
        );

    //userinfo 表单验证
    ckform('signup_form_main',
        [
            { name:'email',msg:'邮箱',type:'email', defMsg:'输入邮箱,格式如下 XXXX@XX.XX', defInput:'输入邮箱',sp:'cor'},
            { name:'nick',msg:'昵称',min:6,max:20, defMsg:'支持中文，不能以数字开头，最多20个字符，中文算2个字符', defInput:'输入昵称',sp:'cor'},
            { name:'password1',msg:'密码',min:6,max:20, defMsg:'6~20位之间', defInput:'输入密码',sp:'cor'},
            { name:'password2',msg:'确认密码',to:'password1', defInput:'输入确认密码',sp:'cor'},
            { name:'day',msg:'天',sp:'cor'},
            { name:'month',msg:'月',sp:'cor'},
            { name:'year',msg:'年',sp:'cor'},
            { name:"city",msg:"城市",sp:'cor'},
            { name:"province",msg:"省",sp:'cor'}
        ]
    );
/**/

/*  以下是文字提示插件的使用
    //$('[title]').yitip();

	var demo2Tip = $("[title]").yitip("api");
	demo2Tip.setColor("red");

    demo2Tip.options.showEvent = "click";
    demo2Tip._bindShowHideEvent();

	demo2Tip.options.offest = {"left":50,"top":110};
	demo2Tip.setPosition("rightMiddle");

	demo2Tip.options.effect.show = {"speed":"slow","offest":110,"startOpacity":0,"endOpacity":1};
    demo2Tip.options.effect.hide = {"speed":"fast","offest":10,"startOpacity":1,"endOpacity":0};

	$("#id_email").yitip({"showEvent" :"click","hideEvent" : null});
	var demo6Tip = $("#id_email").yitip("api");*/
});
</script>
<!--content begin-->
<div class="in-width">          <a href="#" color="green" title="asfasdfasdf" id="tt">wwwwwwwwwwwwwwwwwwwwww</a>
<div class="topbg"></div>
<div class="middlebg over-hidden">
<div class="reg_sign">
<div class="thirdPart">选择合作网站账号登录<br>
   <a target="_blank" title="新浪微博" class="isina" href="/auth/request/sina_weibo/"></a>
   <a target="_blank" title="腾讯QQ" class="iqq" href="/auth/request/qzone/"></a>
   <a target="_blank" title="腾讯微博" class="itengxun" href="/auth/request/qq_weibo/"></a>
</div>

<form class="registerForm" action="{!#weburl#!}register" method="post" id="signup_form_main">
    <fieldset class="inlineLabels reg_inlineLabels">
            <p>
            <input type="text" class="input1" name="email" manual="true" value="{!if isset($post.email)!}{!$post.email!}{!else!}输入邮箱{!/if!}" title="请输入邮箱" id="id_email"/>
              <!--  <font class="cor top_email {!if isset($error['email']) or isset($error['emailType'])!}show{!/if!}">
                    <font>
                        {!if isset($error['email'])!}
                        邮箱已被注册
                        {!elseif isset($error['emailType'])!}
                        邮箱格式不正确
                        {!/if!}
                    </font>
                	<span class="cor1"><span class="cor2"></span></span>
                </font> --> 
            </p>

            <p class="login-sex">性 &nbsp;&nbsp;&nbsp;别：
                <input name="sex" type="radio" value="2" checked/>女<input name="sex" type="radio" value="1" {!if isset($post.sex) and $post.sex eq 1!}checked{!/if!}/>男
            </p>

            <p>
            <input type="text" id="id_username" class="input1" value="{!if isset($post.nick)!}{!$post.nick!}{!else!}输入昵称{!/if!}" maxlength="20" name="nick" manual="true" title="请输入昵称" />
             <!--    <font class="cor top_nick {!if isset($error['nick'])!}show{!/if!}">
                    <font>
                        {!if isset($error['nick'])!}
                        昵称已存在
                        {!/if!}
                    </font>
                	<span class="cor1"><span class="cor2"></span></span>
                </font> -->
            </p>

            <div class="itempassword">
            <input type="text" class="input1" name="password1" value="输入密码" id="id_password1" title="请输入密码">
              <!--   <font class="cor top_password1 {!if isset($error['pswd'])!}show{!/if!}">
                    <font>
                        {!if isset($error['password'])!}
                            密码长度不正确
                        {!/if!}
                    </font>
                	<span class="cor1"><span class="cor2"></span></span>
                </font> -->

            </div>

            <div class="itempassword">
            <input type="text" class="input1" name="password2" manual="true" value="输入确认密码" id="id_password2" title="输入确认密码">
                <!-- <font class="cor top_password2 {!if isset($error['pswd'])!}show{!/if!}">
                    <font>
                        {!if isset($error['password2'])!}
                            两次密码输入不一致
                        {!/if!}
                    </font>
                	<span class="cor1"><span class="cor2"></span></span>
                </font> -->

            </div>

            <div class="diqu">生 &nbsp;&nbsp;&nbsp;日：
                <select name="year" id="year" rel="{!if isset($post.year)!}{!$post.year!}{!/if!}">
                    <option value="">请选择年</option>
                </select> 

                <select name="month" id="month" rel="{!if isset($post.month)!}{!$post.month!}{!/if!}">
                    <option value="">请先选前一项</option>
                </select>

                <select name="day" id="day" rel="{!if isset($post.day)!}{!$post.day!}{!/if!}">
                    <option value="">请先选前一项</option>
                </select>
           </div>


           <div class="diqu" title="请选择地区">所在地：
                <select name="province" id="province" rel="{!if isset($post.province)!}{!$post.province!}{!/if!}">
                    <option value>请选择省/市</option>
                </select>
                <select name="city" id="city" rel="{!if isset($post.city)!}{!$post.city!}{!/if!}">
                    <option value="">请选择市/区</option>
                </select>
                <!--<select name="area">
                    <option value='88'>罗山县</option>
                </select>-->
            </div>


        <p class="user_sub"><!--  已有账号！--> <a href="/login">立即登录</a><input type="submit" name='submit' value="注册" class="o2-button ui-state-default ui-corner-all regBtn"></p>
    </fieldset>
</form>

<div class="clear"></div>
</div>
</div>
<div class="footbg"></div>
</div>
<!--content end-->

{!include file="inc/footer.tpl"!}
