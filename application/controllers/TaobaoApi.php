<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
require_once(APPPATH.'third_party/taobao-sdk-php/TopSdk.php');

/**
 * taobaoAPI 获取商品类,对商品制作缩略图
 * @author michael
 */
class TaobaoApi extends Common {
    static private $t_appkey = '12490529';
    static private $t_secret = '516dfc0892d70d61062fee6b53fea70f';
    static private $t_nick = 'kxd0608';

    static public $top;
    static public $taobaokeItemsDetailGet;
    static public $itemcatsGet;

    /**
     * 构造函数
     */
    public function __construct() {
//        parent::__construct();
//        $this->instanceTop();    //淘宝公共属性
    }

    /**
     * 默认入口
     */
    public function index() {
        echo('teste');
    }


	/**
	 * 实例化淘宝公共类
	 *
     */
    private static function instanceTop() {
        if(!self::$top instanceof TopClient) {
            self::$top = new TopClient;
            self::$top->appkey = self::$t_appkey;
            self::$top->secretKey = self::$t_secret;
            self::$top->format = 'json';
        }
    }

	/**
	 * 实例化 taobao.taobaoke.items.detail.get
	 * 获取单个商品信息
     */
    private static function taobaokeItemsDetailGet($num_iid) {
        self::$taobaokeItemsDetailGet = new TaobaokeItemsDetailGetRequest;
        self::$taobaokeItemsDetailGet->setFields('detail_url,num_iid,title,cid,price,props,pic_url,click_url');   //props可以获取属性
        self::$taobaokeItemsDetailGet->setNumIids($num_iid);
        self::$taobaokeItemsDetailGet->setNick(self::$t_nick);
        return self::$top->execute(self::$taobaokeItemsDetailGet);
    }

	/**
	 * 实例化 taobao.taobaoke.items.get
	 * 用于批量获取淘宝数据

    private function taobaokeItemsGet() {
//        $req = new TaobaokeItemsGetRequest;

        TaobaoApi::$taobaokeItemsGet->setFields("num_iid,title,nick,pic_url,price,click_url,commission,commission_rate,commission_num,commission_volume,shop_click_url,seller_credit_score,item_location,volume");
        TaobaoApi::$taobaokeItemsGet->setNick($this->t_nick);
        TaobaoApi::$taobaokeItemsGet->setCid('16');
        TaobaoApi::$taobaokeItemsGet->setStartPrice("300");
        TaobaoApi::$taobaokeItemsGet->setEndPrice("400");
        return TaobaoApi::$top->execute(TaobaoApi::$taobaokeItemsGet);
    }*/

	/**
	 * 实例化 taobao.itemcats.get
	 * 获取大类的id,name
     */
    private static function itemcatsGet($cid) {
        self::$itemcatsGet = new ItemcatsGetRequest;
        self::$itemcatsGet->setFields("cid,parent_cid,name,is_parent,status");
        self::$itemcatsGet->setCids($cid);
        return self::$top->execute(self::$itemcatsGet);
    }

	/**
	 * 获取商品（商品分发）
	 *
	public function getGoodsid() {
        $this->benchmark->mark('code_start');
        //根据链接判断是那个商家
        $url = $this->input->post('goodsurl');

        if(strpos($url,'taobao.com') || strpos($url,'tmall.com')) {
            $reg = '/id=(\d+)/';
            preg_match($reg,$url,$goodsid);

            if( isset($goodsid['1']) && !empty($goodsid['1']) && is_numeric($goodsid['1']) ) {

                $this->taobaoApiGet($goodsid['1']);
            } else {
                die('获取id有误');
            }

        } else {
            die('dianshang');
        }
	}
     */

	/**
	 * 获取taobao商品数据,获取单个商品
	 * TaobaokeItemsDetail
     */
	public function taobaoApiGet($num_iid='15333940927') {

        self::instanceTop();
        $rs = self::taobaokeItemsDetailGet($num_iid);

        $json = json_encode($rs);   //将商品信息的stdClass Object转换成json字符串       get_object_vars()这个函数可以将对象直接转成数组
        $rs = json_decode($json,true);
        //$rs = get_object_vars($rs);

        if(isset($rs['code'])) {
            die('API数据有误');
        }

	    $itemdetail = $rs['taobaoke_item_details']['taobaoke_item_detail']['0']['item'];
        $itemdetail['click_url'] = $rs['taobaoke_item_details']['taobaoke_item_detail']['0']['click_url'];

        //制作商品图片缩略图
        $datetime = date('Y-m-d H:i:s');
        /*
        $resize_img = $this->resizeGoodsImg($itemdetail['pic_url'], $datetime, '_g');
        $itemdetail['resize_img'] = $resize_img;
        */
        //返回图片的地址，（路径+文件名）
        $imgPath = $this->makeUserDir($this->user_info['uid'], $datetime, '_g', false);
        $path = 'upload/';

        //将远程图片存到本地
        $imgStr = file_get_contents($itemdetail['pic_url']);
        $sourceImg = $path.$imgPath['imgdir'].'.jpg';
        file_put_contents($sourceImg, $imgStr);

        //todo:这里的缩略图是按宽度缩略的，但是高度也得写，谁小按谁来
        $this->imgDealWith('resize', $sourceImg, $path.$imgPath['big'], true, 320,500);
        $this->imgDealWith('resize', $sourceImg, $path.$imgPath['middle'], true, 200,500);
        $this->imgDealWith('resize', $sourceImg, $path.$imgPath['small'], true, 100,500);

        //$itemdetail['test_time'] = $sourceImg;

        $itemdetail['resize_img'] = $this->config->item('base_url').$path.$imgPath['small'];
        $itemdetail['datetime'] = $datetime;

        //时间测试
        $this->benchmark->mark('code_end');
        $test_time = $test_time = $this->benchmark->elapsed_time('code_start', 'code_end');
        $itemdetail['test_time'] = $test_time;
        //时间测试end

        //在这里直接返回ajax商品信息，下面继续进行大类cid信息获取
        echo json_encode($itemdetail);
	}

	/**
	 * 添加商品，将商品插入数据库
	 *

    public function insertGoods() {
        $goods_info = $this->input->post('goods_info');
        $datetime = $this->input->post('datetime');
        $cid = $this->input->post('cid');
        $title = $this->input->post('title');
        $num_iid = $this->input->post('numiid');
        $price = $this->input->post('price');
        $url = $this->input->post('url');

        //对数据过滤

        $itemcatsGet = $this->getBigclass($cid,$title);

        if(isset($itemcatsGet['label'])) {
            $tb = 'laji_goods';
            //没有正常返回大类数据，也要将商品信息插入到数据库
            $data = array(
                'uid' => $this->user_info['uid'],
                'seller' => 'taobao',
                'num_iid' => $num_iid,
                'la_title' => $title,
                'la_intro' => $goods_info,
                'la_label' => $itemcatsGet['label'],
                'la_url' => $url,
                'la_addtime'=> $datetime
            );

        } else {
            //确定lable
            $labels = substr($itemcatsGet['name'],0,-1);
            $labels = explode('#',$labels);

            $this->load->model('GoodsModel');
            $label_id = $this->GoodsModel->labels($labels);

            $label_idstr = '';
            foreach($label_id as $key=>$value) {
                $label_idstr .= '#'.$value;
            }

            //没有淘宝客连接，将商品的直接链接存起来
            if(empty($url)) {
                $url = $this->input->post('detail_url');
            }

            //如果不是垃圾，就判断标签，去label表中将label查出，如果存在就获取label的id，不存在的就插入数据库并且返回id
            $tb = 'goods';
            $data = array(
                'uid' => $this->user_info['uid'],
                'seller' => 'taobao',
                'num_iid' => $num_iid,
                'g_title' => $title,
                'g_intro' => $goods_info,
                'g_label' => $label_idstr,
                'g_type' => $itemcatsGet['g_type'],
                'g_sex' => $itemcatsGet['g_sex'],
                'price' => $price,
                'g_url'=> $url,
                'g_addtime'=> $datetime
            );
        }

        if($this->db->insert($tb, $data)) {
            if($tb == 'laji_goods') {
                echo "<script>alert('ok,到laji了');window.location.href='http://lovego.sinaapp.com'</script>";
            } else {
                echo "<script>alert('商品添加ok');window.location.href='http://lovego.sinaapp.com'</script>";
            }
        }

    }
     */

	/**
	 * 商品图片缩略以及存贮
	 * sae
    public function resizeGoodsImg($img_path) {
        $f = new SaeFetchurl();
        $img_data = $f->fetch($img_path);
        $img = new SaeImage();
        $img->setData( $img_data );

        //存图片  SaeStorage
        $uid = $this->ksession->get('uid');
        $microtime = explode(' ',microtime());
        $md5_date = md5(date('Y-m'));
        $datetime = date('Y-m-d H:i:s').'#'.$microtime['0'];
        $uid_datetime = $uid.$datetime;
        $resize_img = 'goods/'.$md5_date;

        $s = new SaeStorage();
        //用户的默认的图片存贮路径，  如果是抓取的商品，
//        $s->write('lovegoimg' , $uid.'/goods/'.$date.'/'.$filename.'.jpg' , $img_s);

        $img->resize(320); // 等比缩放到320宽
        $img_b = $img->exec(); // 执行处理并返回处理后的二进制数据
        $filename = md5($uid_datetime.'_b');     //320*……
        $resize_img_b = $resize_img.'/'.$filename.'.jpg';
        $s->write('lovegoimg' , $resize_img_b , $img_b);  //320*……的存了

        $img->resize(200); // 等比缩放到200宽
        $img_m = $img->exec(); // 执行处理并返回处理后的二进制数据
        $filename = md5($uid_datetime.'_m');     //200*……
        $resize_img_m = $resize_img.'/'.$filename.'.jpg';
        $s->write('lovegoimg' , $resize_img_m , $img_m);  //200*……的存了


        $img->resize(100,100); // 等比缩放到100宽
        $img_s = $img->exec(); // 执行处理并返回处理后的二进制数据
        $filename = md5($uid_datetime.'_s');     //100*100
        $resize_img_s = $resize_img.'/'.$filename.'.jpg';
        $s->write('lovegoimg' , $resize_img_s , $img_s);  //将100*100的存了


        $resize_img = $s->getUrl( 'lovegoimg' , $resize_img_s );
        return array('resize_img'=>$resize_img, 'datetime'=>$datetime);
    }
     */

	/**
	 * 获取淘宝大类cid，来确定 sex，type，label
	 * ItemcatsGet
     */
    public static function getBigclass($cid,$tit) {

        //允许的大类
        $parent = array('s1'=>'50010404','s2'=>'1625','s3'=>'50006842','50005700','50013864','50023282','50011699','30','1801','50020808','50008163','16','50012029','50010788','50011740','50011397','50006843','25','50016349','28','50010728');

        //分性别的数组
        $sex = array(
            'man'=>array('50011740','30'),  //男性的
            'woman'=>array('50011397','50010788','16','50008163','1801','50023282','50006843'), //女性的
            'all'=>array('50013864','50020808','50005700','50011699','50012029','25','50016349','28','50010728'),   //不分男女的
            'program'=>array('50010404','1625','50006842'), //需要程序分的
        );

        //分type的数组
        $type = array(
            'clothes'=> array('1625','50011699','16','30'), //衣服
            'shoes'=> array('50012029','50006843','50011740'),  //鞋
            'bag'=> array('50006842'),                          //包
            'ornament'=> array('50010404','50013864','50005700','50011397','28'),    //配饰
            'home'=> array('50020808','50008163','25','50016349','50010728'),              //居家
            'makeup'=> array('50023282','1801','50010788')      //美妆
        );

        self::instanceTop();
        //掉获取大类的api
        $cats = self::itemcatsGet($cid);

        $json = json_encode($cats);
        $rs = json_decode($json,true);

        if(isset($rs['code'])) {
            die('API掉数据有误大类的');
        }

        $label = trim($rs['item_cats']['item_cat']['0']['name']).'#';
        $parent_cid = $rs['item_cats']['item_cat']['0']['parent_cid'];

        //如果第一次查出的类id不是大类id,就循环再次查，直到查出该商品的大类id
        if($parent_cid != '0') {

            while($parent_cid != '0') {

                $cats = self::itemcatsGet($parent_cid);

                $json = json_encode($cats);
                $rs = json_decode($json,true);

                if(isset($rs['code'])) {
                    die('API掉数据有误xun');
                }

                $parent_cid = $rs['item_cats']['item_cat']['0']['parent_cid'];
                if($parent_cid != '0') {
                    $label .= trim($rs['item_cats']['item_cat']['0']['name']).'#';
                } else {
                    $topcid = $rs['item_cats']['item_cat']['0']['cid'];
                }
            }
        }

        //大类,性别 判断
        if(in_array($topcid, $parent)) {
            $title = ','.$tit;

            if(in_array($topcid, $sex['woman'])) {   //女性
                $g_sex = 2;
            } else if(in_array($topcid, $sex['man'])) {  //男性
                $g_sex = 1;
            } else if(in_array($topcid, $sex['all'])) {    //中性
                $g_sex = 3;
            } else if(in_array($topcid, $sex['program'])) {    //需要程序分性别的

                if(strpos($title,'女') && !strpos($title,'男')) {
                    $g_sex = 2;
                } else if(!strpos($title,'女') && strpos($title,'男')) {
                    $g_sex = 1;
                } else {
                    $g_sex = 3;
                }

            }

            //todo:考虑一下把运动分成一种类型，但是只要男的运动
            if(in_array($topcid, $type['clothes'])) {       //衣服
                $g_type = 1;
            } else if(in_array($topcid, $type['shoes'])) {    //鞋
                $g_type = 2;
            } else if(in_array($topcid, $type['bag'])) {    //包
                $g_type = 3;
            } else if(in_array($topcid, $type['ornament'])) {    //配饰
                $g_type = 4;
            } else if(in_array($topcid, $type['home'])) {    //家居
                $g_type = 5;
            } else if(in_array($topcid, $type['makeup'])) {    //美妆
                $g_type = 6;
            }

        } else {
            //不在允许的大类里面直接插入垃圾库laji_goods
            return array('label'=>$label);die;
        }

        if(strpos($title,'情侣')) {
            $g_sex = 9;
        }

        return array('g_sex'=>$g_sex,'g_type'=>$g_type,'name'=>$label);
    }

	/**
	 * 获取一批数据
	 *

    public function getDatas() {
        $this->instanceTop();    //淘宝公共属性

        $rs = $this->taobaokeItemsGet();//批量获取数据api

        $json = json_encode($rs);
        $rs = json_decode($json,true);

        //这是获取的40条数据
        $taobaoke_items = $rs['taobaoke_items']['taobaoke_item']; //这个接口不能返回cid，怎么确定标签

        foreach($taobaoke_items as $key => $value) {
            $data = array(
                'uid' => '2',
                'seller' => 'taobao',
                'num_iid' => $value['num_iid'],
                'g_title' => $value['title'],
                'g_intro' => 'shoe',
                'g_label' => $value['item_location'],
                'g_type' => '2',
                'g_sex' => '1',
                'price' => $value['price'],
                'from' => '1'
            );

        //echo '<pre>';var_dump($data);die;

            $q = $this->db->insert('goods', $data);
        }
        redirect('/','location');
    } */

}
?>