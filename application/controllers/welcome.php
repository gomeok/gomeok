<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
/**
 * 入口类
 */
class Welcome extends Common {
    //public $sm;

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 默认入口
     */
    public function index() {
        $this->smart->display('up/index.html');
    }

	/**
	 * 首页展示
	 *
     */
	public function welcomes() {
//sphinx 调用实例
/*
        $this->load->library('SphinxApi','','sphinxapi');

        $this->sphinxapi->setServer('127.0.0.1',9312);
        $keyWord = '商品';
        $result = $this->sphinxapi->query($keyWord);
        echo '<pre>';var_dump($result);die;
*/

//redis缓存 实例
//        $this->load->library('MyRedis','','redis');
//        $obj = $this->redis->initialization();
//        $this->redis->set('存在哪里','china 哈哈 a color');
//        $this->redis->set('we','china 哈哈 is a color');
//        $this->redis->set('1234',' 哈哈acolorsd');
//        $rs = $this->redis->get('red');

        //$this->smarty->assign('aa',$re);
        $this->benchmark->mark('code_start');   //ci基准测试

//CI文件缓存 实例
//        $this->load->driver('cache');
//        $this->cache->file->save('foo','bar');
//        var_dump($this->cache->file->cache_info());die;
//        var_dump($this->cache->file->get('foo'));die;
//        new FileCache();

        $this->smart->display('index.tpl');
        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */