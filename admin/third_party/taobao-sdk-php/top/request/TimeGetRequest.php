<?php
/**
 * TOP API: taobao.time.get request
 * 
 * @author auto create
 * @since 1.0, 2012-01-28 12:30:59
 */
class TimeGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "taobao.time.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
}
