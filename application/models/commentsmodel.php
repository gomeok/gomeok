<?php
require_once('CommonCrud.php');
/**
 * 评论模型
 *
 */
class CommentsModel extends CommonCrud {
    /*
    private $goods = 'goods';
    private $lg_comment = 'comment';*/
    private $lg_user_info = 'user_info';

    public function __construct() {
        parent::__construct();
    }

    /**
     * 查nick
     *
     */
    public function selNicks($nicks) {
        return $this->db->select('uid,nick,sex')->where_in('nick',$nicks)->get($this->lg_user_info)->result_array();
    }

    /**
     * 查询商品评论总数
     *

    public function totalComment($type,$x_id) {
        $where = array('c_type' => $type, 'x_id' => $x_id);
        return $this->db->where($where)->select('c_id')->count_all_results($this->lg_comment);
    } */

}
?>