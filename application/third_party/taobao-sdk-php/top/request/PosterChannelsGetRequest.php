<?php
/**
 * TOP API: taobao.poster.channels.get request
 * 
 * @author auto create
 * @since 1.0, 2012-01-28 12:30:59
 */
class PosterChannelsGetRequest
{
	
	private $apiParas = array();
	
	public function getApiMethodName()
	{
		return "taobao.poster.channels.get";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
	}
}
