<?php
require_once('CommonCrud.php');
/**
 * 逛街
 *
 */
class ShoppingModel extends CommonCrud {
    private $goods = 'goods';
    private $lg_comment = 'comment';
    private $lg_user = 'user';
    private $lg_labels = 'labels';
    private $lg_user_info = 'user_info';
    private $lg_like = 'like';
    private $lg_statistics = 'statistics';

    public function __construct() {
        parent::__construct();
    }

    /**
     * 逛街首页数据查询
     * 查20条商品
     *
     * redis缓存    sex:男，女，全部    goodsType:类型  page:   pageNum:
     * 将不同查询条件下的商品数据总数也做缓存
     * key: shopping_sex_goodsType_page_pageNum
     * ps:如果有新的商品数据添加进来，就加入到缓存第一页中
     *
     */
    public function selGoods($sexSelectArr, $type, $sex, $offset='0', $num='20') {
        //shopping redis-key
        $shoppingRedisKey = 'shopping_'.$sex.'_'.$type.'_'.$offset.'_'.$num;
        //get redis value
        $shoppingRedisRs = $this->redis->get($shoppingRedisKey);

        if($shoppingRedisRs === null) {
            if(!empty($type)) {
                $this->db->where('g_type', $type);
            }
            $this->db->select('goods.*,nick')->where_in('g_show', array('1','2','3'));
            $shoppingRedisRs = $this->db->where_in('g_sex', $sexSelectArr)->join($this->lg_user_info, 'goods.uid=user_info.uid', 'left')->order_by('g_id','desc') ->get($this->goods, $num, $offset)->result_array();
            //将逛街的商品数据缓存redis
            $this->redis->set($shoppingRedisKey, $shoppingRedisRs);
        }
        return $shoppingRedisRs;
    }

    /**
     * 查询商品总数据
     * 1，没有登录的时候（不分性别，混合显示），以后可以性别本地存cookie
     *      逛街查全部
     *      进入单个类型，根据单个‘类型’查总数
     * 2，登录了（分性别）
     *      根据‘性别’查全部
     *      进入单个类型，根据 ‘性别+类型’ 查
     * redis:将不同查询条件下的商品数据总数也做缓存
     */
    public function totalGoods($sexSelectArr='', $type='', $sex) {
        //shopping total redis-key
        $shoppingTotalRedisKey = 'shopping_total_'.$sex.'_'.$type;
        //get redis value
        $shoppingTotalRedisRs = $this->redis->get($shoppingTotalRedisKey);

        if($shoppingTotalRedisRs === null) {
            if(!empty($type)) {
                $this->db->where('g_type', $type);
            }
            $this->db->where_in('g_show', array('1','2','3'));
            $shoppingTotalRedisRs = $this->db->where_in('g_sex', $sexSelectArr)->select('g_id')->count_all_results($this->goods);
            //将逛街的商品数据缓存redis
            $this->redis->set($shoppingTotalRedisKey, $shoppingTotalRedisRs);
        }
        return $shoppingTotalRedisRs;
    }

    /**
     * 情侣（特殊类型）情侣部分具体类型，部分性别
     * 查20条商品
     *
     */
    public function getLoversGoods($offset='0', $num='20') {

        $this->db->select('goods.*,nick')->where_in('g_show', array('1','2','3'));
        return $this->db->where_in('g_sex', '9')->join($this->lg_user_info, 'goods.uid=user_info.uid', 'left')->order_by('g_id','desc') ->get($this->goods, $num, $offset)->result_array();
    }

    /**
     * 查询情侣商品总数
     */
    public function totalLoversGoods() {
        $this->db->where_in('g_show', array('1','2','3'));
        return $this->db->where_in('g_sex', '9')->select('g_id')->count_all_results($this->goods);
    }

    /**
     * 查单个商品
     *
     *
     */
    public function selOneGoods($g_id) {
        if(empty($g_id)) {
            die('!');
        }
        return $this->db->select('*')->where('g_id', $g_id)->get($this->goods)->row_array();
    }

    /**
     * 查商品评论
     *
     *
     */
    public function selLabel($label_arr) {
        if(empty($label_arr)) {
            return array();
        }
        return $this->db->select('l_id,label')->where_in('l_id',$label_arr)->get($this->lg_labels)->result_array();
    }

    /**
     * 根据标签检索商品         //todo:以后不能使用like查询，换做斯芬克斯
     * 以后改：推荐时，数量一定，偏移量随机，但是查的商品不能小于20个
     */
    public function selLabelGoods($g_id, $sexSelectArr, $lable_id, $num='20', $offset='0') {

        // 生成: WHERE title LIKE 'match%'
        foreach($lable_id as $key=>$value) {
            if($key == 0) {
                $this->db->like('g_label', '#'.$value, 'after');
            } else {
                $this->db->or_like('g_label', '#'.$value, 'after');
            }
        }

        return $this->db->select('g_id,uid,seller,g_title,g_intro,g_label,g_comment_num,g_like_num,g_sex,g_addtime,price,g_url')->where_not_in('g_id',$g_id)->where_in('g_sex', $sexSelectArr)->get($this->goods, $num, $offset)->result_array();
    }

    /**
     * 修改一条商品数据
     * @author  michael
     */
    public function updateGoodsLikeNum($g_id, $is_like_num=true, $is_plus=true) {
        $goods_arr = $this->db->select('g_comment_num,g_like_num')->where('g_id', $g_id)->get($this->goods)->row_array();

        if($is_like_num) {  //喜欢+1 or -1
            $num = $is_plus?$goods_arr['g_like_num']+1:$goods_arr['g_like_num']-1;
            $update_arr = array('g_like_num' => $num);
        } else {    //评论+1 or -1
            $num = $is_plus?$goods_arr['g_comment_num']+1:$goods_arr['g_comment_num']-1;
            $update_arr = array('g_comment_num' => $num);
        }
        //修改并返回当前数(喜欢数 or 评论数)
        if($this->db->update($this->goods, $update_arr, array('g_id' => $g_id))) {
            return $num;
        }
    }


    /**
     * 修改统计表
     * 修改了喜欢数， 和被喜欢数
     * @author  michael
     */
    public function updateTongJi($uid, $is_like=true) {
        $tongji = $this->db->select('like,be_like')->where('uid', $uid)->get($this->lg_statistics)->row_array();
        if(!empty($tongji)) {
            $num = $is_like?$tongji['like']+1:$tongji['be_like']+1;
            $update_arr = $is_like?array('like' => $num):array('be_like' => $num);
            return $this->db->update($this->lg_statistics, $update_arr, array('uid' => $uid));die;
        }
        return false;
    }

    /**
     * 查询是否喜欢这个=(商品,晒货或者搭配)
     * @author  michael
     */
    public function isLike($uid, $xId, $kType=1) {
        return $this->db->select('x_id')->where(array('uid'=>$uid, 'x_id'=>$xId, 'k_type'=>$kType))->get($this->lg_like)->num_rows();
    }

    /**
     * 获取该用户的喜欢的 商品g_id
     * @params $uid 用户的uid
     * @params $kType 喜欢的类型  1-商品  2-晒货  3-搭配
     * @author  michael
     */
    public function getLikeGoods($uid, $kType = 1) {
        $likeGoods = $this->db->select('x_id')->where(array('uid'=>$uid, 'k_type'=>$kType))->get($this->lg_like)->result_array();
        $like = array();
        foreach($likeGoods as $likeGid) {
            $like[] = $likeGid['x_id'];
        }
        return array_unique($like);
    }

}
?>