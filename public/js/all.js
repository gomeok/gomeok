// JavaScript Document
//首页菜单下拉
$(document).ready(function(){
	$('.pulldown').mouseenter(function(){
		$(this).children('ul').addClass('menuborder').fadeIn(500)
		})
	$('.pulldown').mouseleave(function(){
		$(this).children('ul').removeClass('menuborder').fadeOut(100)
		})
})
//goods用户简介弹出层
$(document).ready(function(){
	$('.man-img').mouseenter(function(){
		$(this).children('.man-ceng1').fadeIn(500);
		})
	$('.man-img').mouseleave(function(){
		$(this).children('.man-ceng1').fadeOut(100);
		})
})
//判断会员中心左右高度
$(document).ready(function(){
	var leftheight=$('.ziliao-l').height();
	var rightheight=$('.ziliao-r').height();
	if(leftheight<=rightheight){
		$('.ziliao-l').height(rightheight)
		}
	else if(rightheight<leftheight){
		$('.ziliao-l').height(leftheight)
		}
})
//留言
$(document).ready(function(){
	$('.huifu').click(function(){
		$(this).parent('.pl-liuyan').next('.liuyan').fadeIn(500).end()
		})
	$('.sixin-list li').mouseleave(function(){
		$(this).children('.sixin-r').children('.liuyan').fadeOut(200);
		})
})
//删除评论
$(document).ready(function(){
	$('.guanbi').click(function(){
		$(this).parent('.martop5').parent('.sixin-r').parent('li').remove();
	var lilot=$('.sixin-list').children('li').length;
	if(lilot==0){
		$('.sixin-list').html('<p style="text-align:center;font-size:14px;">暂无人给您评价</p>')
		}
		})
})
//导航头部
$(document).ready(function(){
	$('#alltop').after('<div style="height:60px;"></div>');
})
//动态
$(document).ready(function(){
	$('.smile-tp1 a').click(function(){
		$(this).next('.biaoxing').fadeIn(200)
		})
	$('.guanbi span').click(function(){
		$(this).parent('.guanbi').parent('.blue-border').parent('.biaoxing').fadeOut(200)
		})
})
//表情选项卡样式
   function nTabs(thisObj,Num){
        if(thisObj.className == "active")return;
            var tabObj = thisObj.parentNode.id;
            var tabList = document.getElementById(tabObj).getElementsByTagName("li");
            for(i=0; i <tabList.length; i++)
            {
            if (i == Num)
            {
                  thisObj.className = "active";
                  document.getElementById(tabObj+"_Content"+i).style.display = "block";
            }
			else
			{
               tabList[i].className = "normal";
               document.getElementById(tabObj+"_Content"+i).style.display = "none";
            }
        }
    }
//弹出遮罩层
$(document).ready(function(){
	$('.smile-tp2 a').click(function () {
		$('.zhezhaobg,.zhezhao,.zz-content').show();
		$('.zz-content1,.zz-content2').hide();
		return false;
	});
	$('.smile-tp3 a').click(function () {
		$('.zhezhaobg,.zhezhao,.zz-content1').show();
		$('.zz-content,.zz-content2').hide();
		return false;
	});
	$('.smile-tp4 a').click(function () {
		$('.zhezhaobg,.zhezhao,.zz-content2').show();
		$('.zz-content,.zz-content1,.zz-content3').hide();
		return false;
	});
		$('.xs-shop a').click(function(){
		$('.zz-content3').css('display','block');
		$('.zz-content2').css('display','none');
		});
	$('.sixin-a').click(function(){
		$('.zhezhaobg,.zhezhao').show();
		return false;
		})
	$('.zz-close span,.sixin-p a').click(function(){
	$('.zhezhaobg,.zhezhao').hide();
		return false;
		});
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();
	var zzheight=$(window).height();
	var dialogTop = (maskHeight/2) - ($('.zhezhao').height()/2) - 100;
	var dialogLeft = (maskWidth/2) - ($('.zhezhao').width()/2);
	$('.zhezhaobg').css({height:maskHeight, width:maskWidth});
	$('.zhezhao').css({top:dialogTop, left:dialogLeft});
})
//表情弹层和显示页面
$(document).ready(function(){
	$('.zz-tanceng span').click(function(){
		$(this).next('.zz-tclist').css('display','block');
    })
	$('.zz-tcgb span').click(function(){
		$(this).parent('.zz-tcgb').parent('.zz-tclist').css('display','none');
    })
})