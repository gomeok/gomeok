<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
/**
 * 注册类
 * @author  michael
 */
class Register extends Common {
    //public $sm;
    private $lg_statistics = 'statistics';

    /**
     *
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 默认入口
     */
    public function index() {
        die('teste');
    }

	/**
	 * 注册
	 * @author  michael         todo:数据验证
     */
	public function register() {
        $error = array();

        if($this->input->post('submit')) {

            //载入数据验证函数库
            $this->load->helper('dataVerify');
            //预定义值
            $post = array(
                'year'=>'',
                'month'=>'',
                'day'=>'',
                'province'=>'',
                'city'=>''
            );

            //密码验证
            $p1 = $this->input->post('password1');
            $p1 = ($p1=='输入密码')?'':$p1;
            $ckP1 = ckPassword($p1);
            if(!is_bool($ckP1)) {
                $error['password1'] = $ckP1;
            }
            //验证确认密码
            $p2 = $this->input->post('password2');
            $p2 = ($p2=='输入确认密码')?'':$p2;
            if(empty($p2)) {
                $error['password2'] = 1;
            } else if($p1 != $p2) {
                $error['password2'] = 2;
            }

            //用户注册数据
            $post['email'] = $this->input->post('email');
            $ckEmail = ($post['email']=='输入邮箱')?'':$post['email'];
            $ckEmail = ckEmail($ckEmail);
            //验证邮箱 返回1=邮箱为空 2=正则验证没有通过 正常返回过滤后的email
            if(is_numeric($ckEmail)) {
                $error['email'] = $ckEmail;
                if($ckEmail == 1) {
                    $post['email'] = '输入邮箱';
                }
            } else {
                $post['email'] = $ckEmail;
            }

            $post['nick'] = $this->input->post('nick');
            $ckNick = ($post['nick']=='输入昵称')?'':$post['nick'];
            //验证昵称长度，特殊字符，昵称支持中文
            $ckNick = ckNick($ckNick);
            if(is_numeric($ckNick)) {
                $error['nick'] = $ckNick;
                if($ckEmail == 1) {
                    $post['nick'] = '输入昵称';
                }
            } else {
                $post['nick'] = $ckNick;
            }


            $post['sex'] = ckSex($this->input->post('sex'));
            if( $post['sex'] == false ) {
                $error['sex'] = 1;
            }

            //年月日
            $d['year'] = $this->input->post('year');
            $d['month'] = $this->input->post('month');
            $d['day'] = $this->input->post('day');
            $dRs = ckNum($d);
            if($dRs == false) {
                $error['birth'] = 1;
            } else {
                $post['year'] = $dRs['year'];
                $post['month'] = $dRs['month'];
                $post['day'] = $dRs['day'];
            }

            //省市
            $p['province'] = $this->input->post('province');
            $p['city'] = $this->input->post('city');
            $pRs = ckNum($p);
            if($pRs == false) {
                $error['place'] = 1;
            } else {
                $post['province'] = $pRs['province'];
                $post['city'] = $pRs['city'];
            }

            //确认基本数据验证没有错误了，查数据库验证
            if(count($error) == 0) {

                $this->load->model('RegisterModel');
                //检查email和昵称是否被使用
                $e = $this->RegisterModel->regCkEmail(array('email'=>$post['email']));
                $n = $this->RegisterModel->regCkNick(array('nick'=>$post['nick']));
                if($e) {
                    $error['email'] = 3;
                }
                if($n) {
                    $error['nick'] = 'nick';
                }

                //$error= ''; //todo:需要改改
                //没有错误了，去提交数据库
                if(empty($error)) {
                    //没有错误，数据入库
                    $data = array(
                        'email'=>$post['email'],
                        'password'=>$p1
                    );

                    if($this->db->insert('user', $data)) {
                        //获取新插入数据的主键
                        $data['uid'] = $this->db->insert_id();

                        //user_info 数据
                        $info = array(
                            'uid'=>$data['uid'],
                            'nick'=>$post['nick'],
                            'sex'=>$post['sex'],
                            'province'=>$post['province'],
                            'city'=>$post['city']
                        );
                        $this->db->insert('user_info', $info);

                        $data['nick'] = $post['nick'];
                        $data['sex'] = $post['sex'];

                        //user_detail表 数据
                        $user_detail = array('uid'=>$data['uid']);
                        $this->db->insert('user_detail', $user_detail);

                        //插入统计表默认数据
                        $this->db->insert($this->lg_statistics, array('uid' => $data['uid']));

                        //用户积分写到session
                        $data['integral'] = 0;
                        //注册成功后用户信息存session
                        $this->ksession->set($data);

                        //在上传头像方法内要判断user_info中是否存在uid，判断是否登录
                        $this->user_info['uid'] = $data['uid'];

                        //注册成功后上传头像
                        $this->uploadHead();die;
                        //redirect('/', 'location');die;
                    }
                }

            }

            //如果出现注册失败，将信息带回页面
            $this->smart->assign('post',$post);
        }

        $this->smart->assign('jsonError', json_encode($error));

        //$this->smart->assign('error',$error);
        $this->smart->display('register.tpl');

        $this->output->enable_profiler(TRUE);
	}

	/**
	 * 上传用户头像
	 * @author  michael
     */
    public function uploadHead_todo() {
        //SaeStorage domain
        $domain = 'lovegoimg';
        $http = 'http://';

        if(isset($_FILES['head_photo'])) {          //$this->input->post('head_photo')

            $s = new SaeStorage();

            //这里要判断，如果是同一个用户上传，要将之前的图片删除再上传，要不sae不会把图片覆盖

            $fdir = $this->makeUserDir($this->user_info['uid'], '', '', '1');

            $head_img = $fdir.$this->user_info['uid'].'/'.$this->user_info['uid'].'.jpg';

            //判断文件是否存在，存在的话先删除
            if($s->fileExists($domain, $head_img)) {
                $b = $s->delete($domain, $head_img);
                if(!$b) {
                    die('del fail!');
                }
            }

            $rs = $s->upload('lovegoimg', $head_img, $_FILES['head_photo']['tmp_name']);

            $img_url = $http.$s->getUrl('lovegoimg', $head_img);



/*
            $img_info = getimagesize($img_url);
            //页面的宽度是280*280     实际625*800           上传一张图片，然后再上传一张，图片没有变，还是原来的那张显示？？？tmp_name吗？？？
            $bili = $img_info['0']/$img_info['1'];

            if($img_info['0'] < $img_info['1']) {   //宽小，求宽
                $size = 280*$bili;
                $this->smart->assign('w',$size);

            } else {        //高小，求高
                $size = 280/$bili;
                $this->smart->assign('h',$size);
            }
*/

            $this->smart->assign('img_url',$img_url);

        } else if($this->input->post('crop_head')) {
            //要截的图的宽和高
            $w = $this->input->post('w');
            $h = $this->input->post('h');
            //左上角的x,y点
            $mleft = $this->input->post('mleft');
            $mtop = $this->input->post('mtop');
            //原始图片的宽高
            $width = $this->input->post('width');
            $height = $this->input->post('height');

            //float $lx: x起点(百分比模式,1为原图大小,如0.25)
            //float $rx: x终点(百分比模式,1为原图大小,如0.75)
            //float $by: y起点(百分比模式,1为原图大小,如0.25)
            //float $ty: y终点(百分比模式,1为原图大小,如0.75)
            $x1 = $mleft/$width; //x起点
            $y1 = $mtop/$height; //y起点
            $x2 = ($mleft+$w)/$width; //x终点
            $y2 = ($mtop+$h)/$height; //y终点

            //echo $x1,'<br>',$y1,'<br>',$x2,'<br>',$y2,'<br>';

            $s = new SaeStorage();
            $fdir = $this->makeUserDir($this->user_info['uid'], '', '', '1');
            $head_img = $fdir.$this->user_info['uid'].'/'.$this->user_info['uid'].'.jpg';
            $img_url = $s->getUrl('lovegoimg', $head_img);

            //sae 截图
            $f = new SaeFetchurl();
            $img_data = $f->fetch($img_url);
            $img = new SaeImage();
            $img->setData( $img_data );

            $size = $img->crop($x1, $x2, $y1, $y2);

            $img_b = $img->exec(); // 执行处理并返回处理后的二进制数据
            $head_ = $fdir.$this->user_info['uid'].'/'.$this->user_info['uid'].'_c.jpg';
            $s->write('lovegoimg' , $head_ , $img_b);

            $img_url = $http.$s->getUrl('lovegoimg', $head_);

            if($size) {

                $this->ksession->set('head_img',$img_url);
                //跳到用户中心
                redirect('/center/'.$this->user_info['uid'], 'location');
                //header('location:'.base_url().'center/'.$this->user_info['uid']);die;

            }
        }

        $this->smart->display('reg_upload_head.tpl');
    }

	/**
	 * 上传用户头像
	 * @author  michael
     */
    public function uploadHead() {
        $this->isLogin();

        if(isset($_FILES['userfile'])) {    //上传

            $fdir = $this->makeUserDir($this->user_info['uid']);
            $path = 'upload/'.$fdir;

            $headTime = time();
            $imgUrl = $this->uploadImg($path, $this->user_info['uid'].'_'.$headTime.'.jpg');
            if(!is_array($imgUrl)) {
                $this->load->model('RegisterModel');
                //更新头像时间戳
                $this->RegisterModel->updateHeadTimestamp($this->user_info['uid'], 'head_time', $headTime);
            }
            $this->smart->assign('img_url',$imgUrl);

        } else if($this->input->post('crop_head')) {   //裁剪并缩略图片

            //要截的图的宽和高（在原始大小的基础上）                              //todo:数据验证
            $w = $this->input->post('w');
            $h = $this->input->post('h');
            //左上角的x,y点
            $mleft = $this->input->post('mleft');
            $mtop = $this->input->post('mtop');

            $fdir = $this->makeUserDir($this->user_info['uid']);
            $headPath =  'upload/'.$fdir;

            $this->load->model('RegisterModel');
            $headTimeArr = $this->RegisterModel->getHeadTimestamp($this->user_info['uid']);
            $headImg = $headPath.$this->user_info['uid'].'_'.$headTimeArr['head_time'].'.jpg';

            $cropImgTime = time();

            $cropImgName = $headPath.$this->user_info['uid'].'_'.$cropImgTime;

            //裁剪
            $cropOk = $this->imgDealWith('crop', $headImg, $cropImgName.'_c.jpg', false, $w, $h, $mleft, $mtop);

            if(!is_array($cropOk)) {
                //缩略
                $this->imgDealWith('resize', $cropImgName.'_c.jpg', $cropImgName.'_100.jpg', true, 100, 100);
                $this->imgDealWith('resize', $cropImgName.'_c.jpg', $cropImgName.'_50.jpg', true, 50, 50);

                //图片路径写入session
                $this->ksession->set('head_img', $this->config->item('base_url').$cropImgName.'_50.jpg');

                //更新头像截图时间戳
                $this->RegisterModel->updateHeadTimestamp($this->user_info['uid'], 'head_crop_time', $cropImgTime);

                redirect('/center/'.$this->user_info['uid'], 'location');
            }
        }

        $this->smart->display('reg_upload_head.tpl');
    }


    /**
	 * 登录         //********************todo: 加提交的数据验证
     *              //todo:因为数据部完整造成的查询数据为空怎么处理???????  连表查了三张表的数据，但是有一张表没有该用户的数据，造成前两张表的数据也差不出来。统计错误登录日志，查出登录失败的记录
     *
	 * @author  michael
     */
    public function login() {
        $error = array();

        if($this->input->post('submit')) {

            $email = $this->input->post('email');        //******************** 加提交的数据验证
            $password = $this->input->post('password');

            //正则验证
            //查数据库验证
            $this->load->model('RegisterModel');
            $userInfo = $this->RegisterModel->login($email,$password);

            if(!empty($userInfo)) {

                //去查@
                $atNum = $this->RegisterModel->getAtNum($userInfo['uid']);
                $userInfo['atNum'] = $atNum;
                //获取用户头像
                $userDir = $this->makeUserDir($userInfo['uid']);

                $userInfo['head_img'] = $this->config->item('base_url').'upload/'.$userDir.$userInfo['uid'].'_'.$userInfo['head_crop_time'].'_50.jpg';

                //todo:登录成功，根据用户性别存页面色调  //颜色存session呢？还是不存？？？

                //登录成功
                $this->ksession->set($userInfo);


                //todo:查私信

                //todo:将所有的粉丝的部分数据写到cookie，获取本地存贮


                redirect('/', 'location');die;
            } else {
                $error['emailpwd'] = 'error';
            }
        }

        $this->smart->assign('error',$error);
        $this->smart->display('login.tpl');
    }

	/**
	 * 退出登录
	 *
	 * @author  michael
     */
    public function loginOut() {
        $this->ksession->sessUnset('uid');      //todo：这里要把全部session值都清除
        redirect('/', 'location');die;
    }

}
?>