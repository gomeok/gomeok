<?php
require_once('CommonCrud.php');
/**
 * 用户修改模型
 * @author  xingpeidong
 */
class ModModel extends CommonCrud {
    private $lg_user = 'user';
    private $lg_user_detail = 'user_detail';
    private $lg_user_info = 'user_info';


    public function __construct() {
        parent::__construct();
    }

    /**
     * 修改用户信息
	 * @param   array $data
	 * @param   int $id
	 * @return	array
     */
    public function updateUserInfo($uid,$data,$table='user_info'){
		$this->db->where('uid', $uid);
		$rs = $this->db->update($table, $data);//var_dump($data);die;
        return $rs;
    }



    /**
     * 修改用户详细信息
	 * @param   array $data
	 * @param   int $id
	 * @return	array
     */
    public function updateUserDetail($uid,$data){
		$this->db->where('uid', $uid);//var_dump($data);die;
		$rs = $this->db->update($this->lg_user_detail, $data);
		//var_dump($this->db->last_query());die;
        return $rs;
    }



    /**
     * 根据(用户名)搜索用户信息条数
     * @author  xingpeidong
	 * @param	string $nick
	 * @return	int
     */
    public function selNick($condition,$mode=true) {
    	$query = $this->db->get_where($this->lg_user_info, $condition);
    	if ($mode) {
    		return $query->num_rows();
    	}else {
    		return $query->result_array();
    	}
    }



    /**
     * 用户信息搜索
     * @author  xingpeidong
	 * @param	array $data
	 * @return	array
     */
    public function selUserInfo($data) {
    	$query = $this->db->get_where($this->lg_user, $data);
    	//var_dump($this->db->last_query());die;
    	return $query->row_array();
    }


    /**
     * 关联搜索用户全部信息
     * @author  xingpeidong
	 * @param	int $uid
	 * @return	array
     */
    public function selAllInfo($uid){
    	$this->db->select('*');
		$this->db->from($this->lg_user);
		$this->db->where($this->lg_user.'.uid', $uid);
		$this->db->join($this->lg_user_detail, $this->lg_user.'.uid ='.$this->lg_user_detail.'.uid','left');
		$this->db->join($this->lg_user_info, $this->lg_user.'.uid ='.$this->lg_user_info.'.uid','left');
		$query = $this->db->get();
		return $query->row_array();
    }


    /**
     * 通过标签名搜索标签id，或者通过id搜名字
     *
     * @param 搜索的条件 $condition
     * @param 要搜索的内容 $field
     * @return int
     */
    public function selLabel($condition,$field=''){
    	$cname = ($field==''?'label':'l_id');
    	$field = ($field==''?'l_id':$field);//默认$field无值时为搜索标签id
    	$this->db->select($field);
		$this->db->from('labels');
    	$this->db->where(array($cname => $condition));
    	$query = $this->db->get();
    	$labelArr = $query->row_array();//var_dump($labelArr);//var_dump($this->db->last_query());
    	if($labelArr){
    		return $labelArr[$field];
    	}else {
    		return '';
    	}
    }


    /**
     * 插入标签
     *
     * @param array $labels
     */
    public function insertLabels($labels){
    	$idStr='';
    	if ($labels) {
	    	foreach ($labels as $key => $value){
	    		//var_dump($labels,$value);//die;
	    		$this->db->insert('labels',$value);//var_dump($this->db->last_query());die;
	    		$id = $this->db->insert_id();//var_dump($id);die;
	    		if($id>0){
	    			$idStr=$id.",";
	    		}
	    	}
    	}
	    //var_dump($idStr);die;
    	return $idStr;
    }

}
?>