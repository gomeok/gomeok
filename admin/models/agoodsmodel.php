<?php
require_once('CommonCrud.php');
/**
 * 后台商品model
 *
 */
class AGoodsModel extends CommonCrud {
    private $lg_goods = 'goods';
    private $lg_user = 'user';
    private $lg_labels = 'labels';

    public function __construct() {
        parent::__construct();
    }
    
    
    
    function selGoodsList($g_show,$type,$offset='0', $num='60'){
    	if ($g_show=='-1') {
        	$this->db->where_in('g_show', array('0','1','2','3'));
    	}else {
        	$this->db->where_in('g_show', array($g_show));
    	}
        if($type!='-1') {
            $this->db->where('g_type', $type);
        }
		//$this->db->where_in('g_sex', $sex);
        
        $query = $this->db->get($this->lg_goods,$num,$offset);
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
    
    
    


    /**
     * 根据状态，类型查询商品总数据
     */
    public function totalGoods($g_show,$type) {
        if($type!='-1') {
            $this->db->where('g_type', $type);
        }
    	if ($g_show=='-1') {
        	$this->db->where_in('g_show', array('0','1','2','3'));
    	}else {
        	$this->db->where_in('g_show', array($g_show));
    	}
        //echo $this->db->last_query();die;
        return $this->db->select('g_id')->count_all_results($this->lg_goods);
    }
    
    

    /**
     * 查单个商品
     */
    public function selOneGoods($g_id) {
        if(empty($g_id)) {
            die('!');
        }
        return $this->db->where('g_id', $g_id)->get($this->goods)->row_array();
    }

    /**
     * 查商品评论
     *
     *
     */
    public function selLabel($label_arr) {
        return $this->db->select('l_id,label')->where_in('l_id',$label_arr)->get($this->lg_labels)->result_array();
    }

    /**
     * 根据标签检索商品
     * 以后改：推荐时，数量一定，偏移量随机，但是查的商品不能小于20个
     */
    public function selLabelGoods($g_id, $sex, $lable_id, $num='20', $offset='0') {
        // 生成: WHERE title LIKE 'match%'
        foreach($lable_id as $key=>$value) {
            if($key == 0) {
                $this->db->like('g_label', '#'.$value, 'after');
            } else {
                $this->db->or_like('g_label', '#'.$value, 'after');
            }
        }

        return $this->db->select('g_id,uid,seller,g_title,g_intro,g_label,g_comment_num,g_like_num,g_sex,g_addtime,price,g_url')->where_not_in('g_id',$g_id)->where_in('g_sex', $sex)->get($this->goods, $num, $offset)->result_array();
//                echo $this->db->last_query();die;
    }

}
?>