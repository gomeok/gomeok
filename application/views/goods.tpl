{!config_load file="web.inc.conf"!}
{!include file="inc/header.tpl"!}

<script type="text/javascript" src="{!#js_path#!}jquery.pager.js"></script>
<link rel="stylesheet" type="text/css" href="{!#css_path#!}pager.css" />
<script type="text/javascript" src="{!#js_path#!}shopping.js"></script>

<style>
.comm{
    padding:5px;background-color:#fff;height:30px;border:1px #CCCCCC solid;
}
</style>
<!--
<div class="banner in-width1">
<img src="images/banner1.jpg"  height="301" width="955" border="0" />
</div>
 -->
<div class="footbg in-width"></div>
</div>
<!--top end-->

<script type="text/javascript">
$(function() {
    //发表评论 g_id+10000
    $('#comments').click(function() {
        var g_id = $('#g_id').html();
        var c = $('#c_content');
        var r_c = $('#re_content');
        var r_n = $('#re_nick');

        var c_content = c.val();

        var re_nick = r_n.html();

        //如果有回复，取出回复内容
        var re_content = $('#re_content').html();
        if (re_nick != '') {
            c_content = c_content+' #@'+re_nick+'：';
        }
        if (re_content != '') {
            c_content = c_content+re_content;
        }
        //去空格
//        c_content = trim(c_content);

        if (c_content != '') {
            
            global.ajaxpost('comments',{'c_type':'1','g_id':g_id,'c_content':c_content},function(msg) {
                //alert(msg); //msg是匹配之后的评论内容
                //这里要获取当前登录用户sex，nick
                var sex = $('#sex').val();
                var nick = $('#nick').val();

                if (msg == 0) {
                    alert('请登录');

                } else if (sex == 2) {  //f
                    var div_str = '<div class="pl-list clearfix" style="display:none;" id="show_comm"><p class="pl-pic fl"><img src="images/pic15.jpg" height="40" width="40" border="0" /></p><div class="pl-word fl"><p><font class="blue">'+nick+'</font> 刚刚</p><div class="pllist-text"><div class="comm">'+msg+'</div></div></div></div>';

                } else if (sex == 1) {  //m
                    var div_str = '<div class="pl-list clearfix" style="display:none;" id="show_comm"><div class="pl-word fl"><p style="text-align:right;"><font class="blue">'+nick+'</font> 刚刚</p><div class="pllist-text"><div class="comm">'+msg+'</div></div></div><p class="pl-pic fr"><img src="images/pic15.jpg" height="40" width="40" border="0"/></p></div>';
                }
                c.val('');  //将评论框设为空
                r_n.html('');
                r_c.html('');
                $('#c_list').prepend(div_str);
                $('#show_comm').show('slow');//缓慢的方式显示评论内容
            });

        } else {
            var c_area = $('#c_content');
            var i=0;
            //c_area.css('background-color','#F5D6D6');
            var t = setInterval(function() {
                i++;
                i%2==0?c_area.css('background-color','#F5D6D6'):c_area.css('background-color','#fff');
                if (i == 7) {
                    clearInterval(t);
                }
            },100);
        }

    });

    //分页
    if ($('#c_total').length > 0) {
        var c_total = $('#c_total').val();
        $('#page_list').pager({pagenumber: 1, pagecount: c_total, buttonClickCallback: PageClick});
    }

    //回复评论
    $('.reply').live('click', function() {
        var nick = $(this).next().html();
        $('#re_nick').html(nick);
        var re_content = $(this).parent().next().find('div').html();
        $('#re_content').html(re_content);
        $('#c_content').val('回复@'+nick+'：').focus();
    });

});

	//评论ajax分页
	PageClick = function(pageclickednumber) {
		var memct = $('#c_total').val();
        var g_id = $('#g_id').html();

        $("#page_list").pager({ pagenumber: pageclickednumber, pagecount: memct, buttonClickCallback: PageClick });

        global.ajaxpost('commentsaj', {'pn':pageclickednumber,'c_type':'1','x_id':g_id}, function(msg){
            //将原始列表中的内容清空
            $('#c_list').children().remove();

            $.each(msg,function(i,n) {
                if (n.c_sex == 2) {
                    var div_str = '<div class="pl-list clearfix" style="display:;" id="show_comm"><p class="pl-pic fl"><img src="images/pic15.jpg" height="40" width="40" border="0" /></p><div class="pl-word fl"><p><font class="blue">'+n.nick+'</font> '+n.c_time+'<a href="javascript:;" class="reply">回复</a><span class="hidden">'+n.nick+'</span></p><div class="pllist-text"><div class="comm">'+n.c_content+'</div></div></div></div>';
                } else if (n.c_sex == 1) {
                    var div_str = '<div class="pl-list clearfix" style="display:;" id="show_comm"><div class="pl-word fl"><p style="text-align:right;"><font class="blue">'+n.nick+'</font> '+n.c_time+'<a href="javascript:;" class="reply">回复</a><span class="hidden">'+n.nick+'</span></p><div class="pllist-text"><div  class="comm">'+n.c_content+'</div></div></div><p class="pl-pic fr"><img src="images/pic15.jpg" height="40" width="40" border="0"/></p></div>';
                }
                $('#c_list').append(div_str);
            });

        },'json');
	}

</script>

<!--content begin-->
<div id="content">
<!--clearfix begin-->
<div class="clearfix">
<div class="good-left fl">
<div class="good-top"></div>
<div class="good-lc1">
<div class="clearfix">
<div class="goodl-l fl">
<div class="goodpic"><img src="{!$goods.pic!}" border="0" /></div>
<div class="goodpiclist">
    <ul>
    <li><img src="images/pic14.jpg" height="50" width="50" border="0" /> </li>
    <li><img src="images/pic14.jpg" height="50" width="50" border="0" /> </li>
    <li><img src="images/pic14.jpg" height="50" width="50" border="0" /> </li>
    </ul>
</div>
</div>
<div class="goodl-r fr">
<h3>{!$goods.g_title!}</h3>     <span id="g_id" class="hidden">{!$goods.g_id+10000!}</span>
<p>价格：{!$goods.price!}元</p>
<a class="font14 blue" href="{!$goods.g_url!}" target="__blank">去淘宝购买东西>></a>
    
    <span class="g_parent">
        <p class="blue-xin1 blue like {!if !empty($isLike)!}islike{!/if!}"><span><span class="likenum">{!$goods.g_like_num!}</span></span></p>
        <!-- hidden 数据区 -->
        <input type="hidden" class="g_id" value="{!$goods.g_id+10000!}"/>
        <input type="hidden" class="uid" value="{!$goods.uid!}"/>
        <input type="hidden" class="is_like" value="{!$isLike!}"/>
        <!-- hidden 数据区 -->
    </span>

<div class="good-xx"><b>时尚元素：</b>
{!foreach key=k item=lab from=$label!}
    <a href="#">{!$lab!}</a>
{!/foreach!}
</div>
</div>
</div>
<div class="pinglun">

<div class="pl-kuang"><textarea name="" cols="" rows="" id="c_content"></textarea></div>
<!-- 回复使用 -->
<span id="re_content" class="hidden"></span>
<span id="re_nick" class="hidden"></span>
<!-- 回复使用 -->

<div class="pl-btn clearfix">
    <div class="pl-smile fl">

    <!-- bq -->
    <div class="biaoqing clearfix">
    <ul class="bq-list2">
        <li class="smile-tp1"><a>表情</a>



            <div class="biaoxing" style="display:none;">
                <div class="sanjiao"><img src="{!#img_path#!}tancengbg1.gif" height="7" width="11" border="0" /></div>
            <div class="blue-border biaoqing-w">
            <div class="guanbi"><span>关闭</span></div>
                <ul id="myTab2">
                    <li class="active" onclick="nTabs(this,0);"><a href="javascript:void(0)">默认</a></li>
                    <li class="normal" onclick="nTabs(this,1);"><a href="javascript:void(0)">心情</a></li>
                    <li class="normal" onclick="nTabs(this,2);"><a href="javascript:void(0)">休闲</a></li>
                    <li class="normal" onclick="nTabs(this,3);"><a href="javascript:void(0)">搞怪</a></li>
                </ul>
            <div class="clear"></div>


            <div id="myTab2_Content0" class="bq-list1" style="display:block;">
                {!include file='inc/smileys.tpl'!}
                <!--
                <ul>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                </ul>
                <div class="clear"></div> -->
            </div>
          <!--  <div id="myTab2_Content1" class="bq-list1">
                <ul>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div id="myTab2_Content2" class="bq-list1">
                <ul>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div id="myTab2_Content3" class="bq-list1">
                <ul>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                <li><a href=""><img src="images/biaoqing1.gif" height="21" width="21" border="0" /></a></li>
                </ul>
                <div class="clear"></div>
            </div>-->

            </div>
            </div>
        </li>
        </ul>
        </div>
        <!-- bq -->

    </div>
    <div class="pl-zf fl"><input name="" type="checkbox" value="" />&nbsp;转发给我的粉丝</div>
    <div class="pl-anniu fl"><input class="bluebg button1" name="" type="button" value="确定" id="comments"/></div>
</div>

{!nocache!}
{!if !empty($comment)!}
<!--评论列表-->
<div class="pl-listall" id="comment_list">
<span id='c_list'>
{!foreach key=k item=c from=$comment!}
    {!if $c.c_sex eq 2!}
        <!-- 女 -->
        <div class="pl-list clearfix">
            <p class="pl-pic fl"><img src="{!$head_img!}" height="40" width="40" border="0" class='getuserinfo' rel='{!$c.uid!}'/></p>
            <div class="pl-word fl">
                <p><font class="blue getuserinfo" rel='{!$c.uid!}'>{!$c.nick!}</font> {!$c.c_time!} <a href="javascript:;" class="reply">回复</a><span class="hidden">{!$c.nick!}</span></p>
                <div class="pllist-text">
                    <div class="comm">{!$c.c_content!}</div>
                </div>
            </div>
        </div>
        <!-- 女 -->
    {!elseif $c.c_sex eq 1!}
        <!-- 男 -->
        <div class="pl-list clearfix">
            <div class="pl-word fl">
                <p style="text-align:right;"><font class="blue getuserinfo" rel='{!$c.uid!}'>{!$c.nick!}</font> {!$c.c_time!} <a href="javascript:;" class="reply">回复</a><span class="hidden">{!$c.nick!}</span></p>
                <div class="pllist-text">
                    <div class="comm">{!$c.c_content!}</div>
                </div>
            </div>
            <p class="pl-pic fr"><img src="{!$head_img!}" height="40" width="40" border="0" class='getuserinfo' rel='{!$c.uid!}'/></p>
        </div>
        <!-- 男 -->
    {!/if!}
{!/foreach!}
</span>
    <!-- 分页 -->
    <div class="page-list" id="page_list"></div>
    <input type="hidden" value="{!$c_total!}" id="c_total"/>
    <!-- 分页 -->
</div>
<!--评论列表结束-->
{!/if!}
{!/nocache!}

</div>
</div>
<div class="good-foot"></div>
</div>


<!--     <div class="good-right fr">
        <div class="good-top1"></div>
        <div class="good-lc2 padding10">
            <p class="beaut-pic"><img src="images/pic12.jpg" height="128" width="129" border="0" /></p>

            <ul class="beaut-name">
            <li class="admin-img"><img src="images/icons1.gif" height="16" width="16" border="0" /></li>
            <li>{!$userInfo.nick!}</li>
            <li class="admin-img"><img src="images/icons2.gif" height="22" width="21" border="0" /></li>
            </ul>

            <ul class="beaut-gz">
            <li><p>分享</p><p class="blue">{!$userInfo.share!}</p></li>
            <li><p>粉丝</p><p class="blue">{!$userInfo.pink!}</p></li>
            <li><p>关注</p><p class="blue">{!$userInfo.attention!}</p></li>
            <li class="noborder"><p class="img-line"><img src="images/icons5.gif" height="13" width="16" border="0" /></p><p class="blue">{!$userInfo.like!}</p></li>
            </ul>
            <div class="clear"></div>
            <div class="add-guanz clearfix">
                <a class="fl bluebg {!if empty($userInfo.att_uid)!}attention{!else!}del_attention{!/if!}" href="javascript:;" rel='{!$userInfo.uid!}'>{!if empty($userInfo.att_uid)!}＋加关注{!else!}取消关注{!/if!}</a>
                <a class="fr" href="">发私信</a>
            </div>
            <p class="bluebg gowhere"><a href="">他的美丽空间</a></p>
        </div>
        <div class="good-foot1"></div>
    </div> -->
    {!include file="inc/userinfo.tpl"!}

</div>
<!--clearfixr end-->


<!--相关推荐-->
<div class="aboutactr clearfix">
<h2>相关推荐</h2>
<div class="clearfix">

<div class="introduce1 fl">
{!if !empty($rec_goods.0)!}
{!foreach key=k item=g from=$rec_goods.0!}
<div class="intro1a">
<div class="good-top1"></div>
<div class="good-lc2">
<div class="intro-img">
    <a href="{!#weburl#!}goods/{!$g.g_id+10000!}"><img src="{!$g.pic!}" border="0" /></a>
</div>
<div class="intro-word clearfix">
<span class="good-star fl"></span><font class="blue fl">({!$g.g_like_num!})</font><span class="good-tl fr"></span>
</div>
    <dl class="intro-man clearfix">
    <dt class="man-img fl">
    <img src="images/pic17.jpg" height="33" width="33" border="0" />
    <div class="man-ceng1 clearfix">
    <div class="clearfix">
    <p class="ceng1-img"><img src="images/pic18.jpg" height="60" width="60" border="0" /></p>
    <div class="ceng1-word">
    <h3 class="blue">牙牙</h3>
    <p class="ceng1-p"><span>浙江</span>杭州</p>
    <p class="ceng1-fx"><b class="blue">好多好多</b>分享<b class="blue mar5">0</b>粉丝<b class="blue mar5">111111</b>喜欢</p>
    </div>
    </div>
    <div class="ceng-admin"><a href="">用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用..</a></div>
    </div>
    </dt>
    <dd class="man-word fl">
    <p><a  class="blue" href=""><b>{!$g.uid!}</b></a></p>
    <p><a href="">{!$g.g_intro!}</a>{!$g.g_title!}</p>
    </dd>
    </dl>
</div>
<div class="good-foot1"></div>
</div>
{!/foreach!}
{!/if!}
</div>

<div class="introduce1 fl">
{!if !empty($rec_goods.1)!}
{!foreach key=k item=g from=$rec_goods.1!}
<div class="intro1a">
<div class="good-top1"></div>
<div class="good-lc2">
<div class="intro-img">
    <a href="{!#weburl#!}goods/{!$g.g_id+10000!}"><img src="{!$g.pic!}" border="0" /></a>
</div>
<div class="intro-word clearfix">
<span class="good-star fl"></span><font class="blue fl">({!$g.g_like_num!})</font><span class="good-tl fr"></span>
</div>
    <dl class="intro-man clearfix">
    <dt class="man-img fl">
    <img src="images/pic17.jpg" height="33" width="33" border="0" />
    <div class="man-ceng1 clearfix">
    <div class="clearfix">
    <p class="ceng1-img"><img src="images/pic18.jpg" height="60" width="60" border="0" /></p>
    <div class="ceng1-word">
    <h3 class="blue">牙牙</h3>
    <p class="ceng1-p"><span>浙江</span>杭州</p>
    <p class="ceng1-fx"><b class="blue">好多好多</b>分享<b class="blue mar5">0</b>粉丝<b class="blue mar5">111111</b>喜欢</p>
    </div>
    </div>
    <div class="ceng-admin"><a href="">用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用..</a></div>
    </div>
    </dt>
    <dd class="man-word fl">
    <p><a  class="blue" href=""><b>{!$g.uid!}</b></a></p>
    <p><a href="">{!$g.g_intro!}</a>{!$g.g_title!}</p>
    </dd>
    </dl>
</div>
<div class="good-foot1"></div>
</div>
{!/foreach!}
{!/if!}
</div>

<div class="introduce1 fl">
{!if !empty($rec_goods.2)!}
{!foreach key=k item=g from=$rec_goods.2!}
<div class="intro1a">
<div class="good-top1"></div>
<div class="good-lc2">
<div class="intro-img">
    <a href="{!#weburl#!}goods/{!$g.g_id+10000!}"><img src="{!$g.pic!}" border="0" /></a>
</div>
<div class="intro-word clearfix">
<span class="good-star fl"></span><font class="blue fl">({!$g.g_like_num!})</font><span class="good-tl fr"></span>
</div>
    <dl class="intro-man clearfix">
    <dt class="man-img fl">
    <img src="images/pic17.jpg" height="33" width="33" border="0" />
    <div class="man-ceng1 clearfix">
    <div class="clearfix">
    <p class="ceng1-img"><img src="images/pic18.jpg" height="60" width="60" border="0" /></p>
    <div class="ceng1-word">
    <h3 class="blue">牙牙</h3>
    <p class="ceng1-p"><span>浙江</span>杭州</p>
    <p class="ceng1-fx"><b class="blue">好多好多</b>分享<b class="blue mar5">0</b>粉丝<b class="blue mar5">111111</b>喜欢</p>
    </div>
    </div>
    <div class="ceng-admin"><a href="">用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用..</a></div>
    </div>
    </dt>
    <dd class="man-word fl">
    <p><a  class="blue" href=""><b>{!$g.uid!}</b></a></p>
    <p><a href="">{!$g.g_intro!}</a>{!$g.g_title!}</p>
    </dd>
    </dl>
</div>
<div class="good-foot1"></div>
</div>
{!/foreach!}
{!/if!}
</div>

<div class="introduce2 fr">
{!if !empty($rec_goods.3)!}
{!foreach key=k item=g from=$rec_goods.3!}
<div class="intro1a">
<div class="good-top1"></div>
<div class="good-lc2">
<div class="intro-img">
    <a href="{!#weburl#!}goods/{!$g.g_id+10000!}"><img src="{!$g.pic!}" border="0" /></a>
</div>
<div class="intro-word clearfix">
<span class="good-star fl"></span><font class="blue fl">({!$g.g_like_num!})</font><span class="good-tl fr"></span>
</div>
    <dl class="intro-man clearfix">
    <dt class="man-img fl">
    <img src="images/pic17.jpg" height="33" width="33" border="0" />
    <div class="man-ceng1 clearfix">
    <div class="clearfix">
    <p class="ceng1-img"><img src="images/pic18.jpg" height="60" width="60" border="0" /></p>
    <div class="ceng1-word">
    <h3 class="blue">牙牙</h3>
    <p class="ceng1-p"><span>浙江</span>杭州</p>
    <p class="ceng1-fx"><b class="blue">好多好多</b>分享<b class="blue mar5">0</b>粉丝<b class="blue mar5">111111</b>喜欢</p>
    </div>
    </div>
    <div class="ceng-admin"><a href="">用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用户简介用..</a></div>
    </div>
    </dt>
    <dd class="man-word fl">
    <p><a  class="blue" href=""><b>{!$g.uid!}</b></a></p>
    <p><a href="">{!$g.g_intro!}</a>{!$g.g_title!}</p>
    </dd>
    </dl>
</div>
<div class="good-foot1"></div>
</div>
{!/foreach!}
{!/if!}
</div>
</div>
<div class="page-list"><span class="bluebg">1</span><a href="">2</a><a href="">3</a><a href="">4</a><a href="">下一页></a></div>
</div>
</div>
<!--相关推荐结束-->

</div>
<!--content end-->

<!--footer begin-->
{!include file="inc/footer.tpl"!}
<!--footer end-->
</body>
</html>
