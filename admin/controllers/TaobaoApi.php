<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
require_once(APPPATH.'third_party/taobao-sdk-php/TopSdk.php');

/**
 * taobaoAPI 获取商品类,对商品制作缩略图
 * @author michael
 */
class TaobaoApi extends Common {
    static private $t_appkey = '12490529';
    static private $t_secret = '516dfc0892d70d61062fee6b53fea70f';
    static private $t_nick = 'kxd0608';

    static public $top;
    static public $taobaokeItemsDetailGet;
    static public $itemcatsGet;
    static public $taobaokeItemsGet;

    public $error_msg = '';

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }

	/**
	 * 实例化淘宝公共类
	 *
     */
    private static function instanceTop() {
        if(!self::$top instanceof TopClient) {
            self::$top = new TopClient;
            self::$top->appkey = self::$t_appkey;
            self::$top->secretKey = self::$t_secret;
            self::$top->format = 'json';
        }
    }

	/**
	 * 实例化 taobao.taobaoke.items.detail.get
	 * 获取单个商品信息
     */
    private static function taobaokeItemsDetailGet($num_iid) {
        if(!self::$taobaokeItemsDetailGet instanceof TaobaokeItemsDetailGetRequest) {
            self::$taobaokeItemsDetailGet = new TaobaokeItemsDetailGetRequest;
        }
        self::$taobaokeItemsDetailGet->setFields('detail_url,num_iid,title,cid,price,props,pic_url,click_url');   //props可以获取属性
        self::$taobaokeItemsDetailGet->setNumIids($num_iid);
        self::$taobaokeItemsDetailGet->setNick(self::$t_nick);
        return self::$top->execute(self::$taobaokeItemsDetailGet);
    }

	/**
	 * 实例化 taobao.itemcats.get
	 * 获取大类的id,name
     */
    private static function itemcatsGet($cid) {
        if(!self::$itemcatsGet instanceof ItemcatsGetRequest) {
            self::$itemcatsGet = new ItemcatsGetRequest;
        }
        self::$itemcatsGet->setFields("cid,parent_cid,name,is_parent,status");
        self::$itemcatsGet->setCids($cid);
        return self::$top->execute(self::$itemcatsGet);
    }

	/**
	 * 获取taobao商品数据
	 * TaobaokeItemsDetail
	public function taobaoApiGet($num_iid='15333940927') {
        self::instanceTop();
        $rs = self::taobaokeItemsDetailGet($num_iid);

        $json = json_encode($rs);   //将商品信息的stdClass Object转换成json字符串
        $rs = json_decode($json,true);

        if(isset($rs['code'])) {
            die('API数据有误');
        }

	    $itemdetail = $rs['taobaoke_item_details']['taobaoke_item_detail']['0']['item'];
        $itemdetail['click_url'] = $rs['taobaoke_item_details']['taobaoke_item_detail']['0']['click_url'];

        //制作商品图片缩略图
        $datetime = date('Y-m-d H:i:s');
        $resize_img = $this->resizeGoodsImg($itemdetail['pic_url'], $datetime, '_g');
        $itemdetail['resize_img'] = $resize_img;
        $itemdetail['datetime'] = $datetime;

        //时间测试
        $this->benchmark->mark('code_end');
        $test_time = $test_time = $this->benchmark->elapsed_time('code_start', 'code_end');
        //时间测试end
        $itemdetail['test_time'] = $test_time;

        //在这里直接返回ajax商品信息，下面继续进行大类cid信息获取
        echo json_encode($itemdetail);
	}
     */

	/**
	 * 获取淘宝大类cid，确定 sex，type，label
	 * ItemcatsGet
     */
    public static function getBigclass($cid,$tit) {

        //允许的大类
        $parent = array('s1'=>'50010404','s2'=>'1625','s3'=>'50006842','50005700','50013864','50023282','50011699','30','1801','50020808','50008163','16','50012029','50010788','50011740','50011397','50006843','25','50016349','28','50010728');

        //分性别的数组
        $sex = array(
            'man'=>array('50011740','30'),  //男性的
            'woman'=>array('50011397','50010788','16','50008163','1801','50023282','50006843'), //女性的
            'all'=>array('50013864','50020808','50005700','50011699','50012029','25','50016349','28','50010728'),   //不分男女的
            'program'=>array('50010404','1625','50006842'), //需要程序分的
        );

        //分type的数组
        $type = array(
            'clothes'=> array('1625','50011699','16','30'), //衣服
            'shoes'=> array('50012029','50006843','50011740'),  //鞋
            'bag'=> array('50006842'),                          //包
            'ornament'=> array('50010404','50013864','50005700','50011397','28'),    //配饰
            'home'=> array('50020808','50008163','25','50016349','50010728'),              //居家
            'makeup'=> array('50023282','1801','50010788')      //美妆
        );

        self::instanceTop();
        //掉获取大类的api
        $cats = self::itemcatsGet($cid);

        $json = json_encode($cats);
        $rs = json_decode($json,true);

        if(isset($rs['code'])) {
            $this->error_msg = 'API--大类';echo('API误--大类');var_dump($rs);return;
        }

        $label = trim($rs['item_cats']['item_cat']['0']['name']).'#';
        $parent_cid = $rs['item_cats']['item_cat']['0']['parent_cid'];

        //如果第一次查出的类id不是大类id,就循环再次查，直到查出该商品的大类id
        if($parent_cid != '0') {

            while($parent_cid != '0') {

                $cats = self::itemcatsGet($parent_cid);

                $json = json_encode($cats);
                $rs = json_decode($json,true);

                if(isset($rs['code'])) {
                    $this->error_msg = 'API--循环找大类';echo('API有误-循环找大类');var_dump($rs);return;
                }

                $parent_cid = $rs['item_cats']['item_cat']['0']['parent_cid'];
                if($parent_cid != '0') {
                    $label .= trim($rs['item_cats']['item_cat']['0']['name']).'#';
                } else {
                    $topcid = $rs['item_cats']['item_cat']['0']['cid'];
                }
            }
        }

        //大类,性别 判断
        if(in_array($topcid, $parent)) {
            $title = ','.$tit;

            if(in_array($topcid, $sex['woman'])) {   //女性
                $g_sex = 2;
            } else if(in_array($topcid, $sex['man'])) {  //男性
                $g_sex = 1;
            } else if(in_array($topcid, $sex['all'])) {    //中性
                $g_sex = 3;
            } else if(in_array($topcid, $sex['program'])) {    //需要程序分性别的

                if(strpos($title,'女') && !strpos($title,'男')) {
                    $g_sex = 2;
                } else if(!strpos($title,'女') && strpos($title,'男')) {
                    $g_sex = 1;
                } else {
                    $g_sex = 3;
                }
            }

            if(in_array($topcid, $type['clothes'])) {       //衣服
                $g_type = 1;
            } else if(in_array($topcid, $type['shoes'])) {    //鞋
                $g_type = 2;
            } else if(in_array($topcid, $type['bag'])) {    //包
                $g_type = 3;
            } else if(in_array($topcid, $type['ornament'])) {    //配饰
                $g_type = 4;
            } else if(in_array($topcid, $type['home'])) {    //居家
                $g_type = 5;
            } else if(in_array($topcid, $type['makeup'])) {    //美妆
                $g_type = 6;
            }

        } else {
            //不在允许的大类里面直接插入垃圾库laji_goods
            return array('label'=>$label);die;
        }

        if(strpos($title,'情侣')) {
            $g_sex = 9;
        }

        return array('g_sex'=>$g_sex,'g_type'=>$g_type,'name'=>$label);
    }


	/**
	 * 实例化 taobao.taobaoke.items.get
	 * 用于批量获取淘宝数据
     * condtion: 1, start_price --- end_price
     *           2, page_no --- page_size 分页
     *           3, start_commissionRate -- end_commissionRate 佣金比例
     *           4, keyword  商品标题中包含的关键字. 注意:查询时keyword,cid至少选择其中一个参数
     *                        以后可以用关键字来抓
     *           5, sort 排序
     *
     */
    private static function taobaokeItemsGet() {
        TaobaoApi::$taobaokeItemsGet = new TaobaokeItemsGetRequest;

        TaobaoApi::$taobaokeItemsGet->setFields("num_iid,title,nick,pic_url,price,click_url,commission,commission_rate,commission_num,commission_volume,shop_click_url,seller_credit_score,item_location,volume");
        TaobaoApi::$taobaokeItemsGet->setNick(self::$t_nick);
        TaobaoApi::$taobaokeItemsGet->setCid('16');
        TaobaoApi::$taobaokeItemsGet->setStartPrice("300");
        TaobaoApi::$taobaokeItemsGet->setEndPrice("400");

        TaobaoApi::$taobaokeItemsGet->setSort("price_desc");
        TaobaoApi::$taobaokeItemsGet->setKeyword("裙子");
        TaobaoApi::$taobaokeItemsGet->setStartCommissionRate("0100");
        TaobaoApi::$taobaokeItemsGet->setEndCommissionRate("3000");

        return TaobaoApi::$top->execute(TaobaoApi::$taobaokeItemsGet);
    }

	/**
	 * 获取一批数据
	 *
    public function getDatas() {
        self::instanceTop();    //淘宝公共属性
        $rs = self::taobaokeItemsGet();    //批量获取数据api

        $json = json_encode($rs);
        $rs = json_decode($json,true);

        //这是获取的40条数据
        $taobaoke_items = $rs['taobaoke_items']['taobaoke_item']; //这个接口不能返回cid，怎么确定标签

        foreach($taobaoke_items as $key => $value) {
            $data = array(
                'uid' => '2',
                'seller' => 'taobao',
                'num_iid' => $value['num_iid'],
                'g_title' => $value['title'],
                'g_intro' => 'shoe',
                'g_label' => $value['item_location'],
                'g_type' => '2',
                'g_sex' => '1',
                'price' => $value['price'],
                'from' => '1'
            );

            $this->db->insert('goods', $data);
        }
        //
        redirect('/admin.php','location');
    }
     */



	/**
     * 批量获取淘宝数据
     *
	 * 实例化 taobao.taobaoke.items.get
	 * 用于批量获取淘宝数据
     * condtion: 1, start_price --- end_price
     *           2, page_no --- page_size 分页
     *           3, start_commissionRate -- end_commissionRate 佣金比例
     *           4, keyword  商品标题中包含的关键字. 注意:查询时keyword,cid至少选择其中一个参数
     *                        以后可以用关键字来抓
     *           5, sort 排序
     */
    public function getManyGoods() {
//        set_time_limit(0);
        //淘宝大类cid  21个类

        $cid = $this->input->post('gcid');
        $p = $this->input->post('price');

        self::instanceTop();

        TaobaoApi::$taobaokeItemsGet = new TaobaokeItemsGetRequest;
        TaobaoApi::$taobaokeItemsGet->setNick(self::$t_nick);
        TaobaoApi::$taobaokeItemsGet->setFields("num_iid,title,nick,pic_url,price,click_url,commission,commission_rate,commission_num,commission_volume,shop_click_url,seller_credit_score,item_location,volume");

        /*
        foreach($cids as $key=>$cid) {
            foreach($price as $k=>$p) {
            }
        }
        */

        TaobaoApi::$taobaokeItemsGet->setCid($cid);
        TaobaoApi::$taobaokeItemsGet->setStartPrice($p['0']);
        TaobaoApi::$taobaokeItemsGet->setEndPrice($p['1']);
        /*
        TaobaoApi::$taobaokeItemsGet->setSort("price_desc");
        TaobaoApi::$taobaokeItemsGet->setKeyword("裙子");
        */
        TaobaoApi::$taobaokeItemsGet->setStartCommissionRate("0100");
        TaobaoApi::$taobaokeItemsGet->setEndCommissionRate("5000");

        $goods_data = TaobaoApi::$top->execute(TaobaoApi::$taobaokeItemsGet);

        echo '开始处理数据-  类：'.$cid.' 价格:'.$p['0'].'-'.$p['1'],'<br/>';

        $start_time = date('Y-m-d H:i:s');

        //在这里处理数据
        $this->dealManyGoods($goods_data);


        //记录抓起日志
        $log = array(
            'get_type'=>$cid,
            'get_price'=>$p['0'].'-'.$p['1'],
            'start_time'=>$start_time
        );
        if(!empty($this->error_msg)) {
            $log['get_error'] = $this->error_msg;
        }
        $this->db->insert('get_goods', $log);

        echo 'sleep 了','<br/>###########<br/>';

        //暂停
        //sleep(5);
    }

	/**
     * 批量获取淘宝数据后的数据处理
     *
     */
    private function dealManyGoods($goods_data) {

        $json = json_encode($goods_data);
        $rs = json_decode($json,true);

        if(isset($rs['code'])) {
            $this->error_msg = 'API--批量商品';echo('API--批量商品');var_dump($rs);return;
        }

        //这是获取的40条数据
        $taobaoke_items = $rs['taobaoke_items']['taobaoke_item']; //这个接口不能返回cid，怎么确定标签
        $num_iids_0 = $num_iids_1 = $num_iids_2 = $num_iids_3 = '';
        foreach($taobaoke_items as $key=>$value) {
            if($key < 10) {
                $num_iids_0 .= $value['num_iid'].',';
            } else if($key < 20) {
                $num_iids_1 .= $value['num_iid'].',';
            } else if($key < 30) {
                $num_iids_2 .= $value['num_iid'].',';
            } else if($key < 40) {
                $num_iids_3 .= $value['num_iid'].',';
            }
        }

        //这是40个num_iid 的字符串
        for($i=0;$i<4;$i++) {
            $iids = 'num_iids_'.$i;

            $num_iids = substr($$iids, 0 , -1);
//            echo '<pre>';var_dump($num_iids);die;

//            echo 'start 批量获取详细商品xx num_iids_'.$i,'<br/>';

            $this->getDetailGoods($num_iids);
        }

    }

	/**
	 * 批量获取详细商品详细
	 * TaobaokeItemsDetail
     */
	public function getDetailGoods($num_iids='') {
        self::instanceTop();
        $rs = self::taobaokeItemsDetailGet($num_iids);

        $json = json_encode($rs);   //将商品信息的stdClass Object转换成json字符串
        $rs = json_decode($json,true);

        if(isset($rs['code'])) {
            $this->error_msg = 'API--商品详细';echo('API有误--商品详细');echo '<pre>';var_dump($rs);return;
        }
        //40条商品数据
        $itemdetail = $rs['taobaoke_item_details']['taobaoke_item_detail'];

//        echo 'start 将40条数据循环处理','<br/>';

        //将40条数据循环处理
        foreach($itemdetail as $key=>$value) {
            $per_goods = $value['item'];
            $per_goods['click_url'] = $value['click_url'];

            $mt = explode(' ', microtime());
            //制作商品图片缩略图
            $datetime = date('Y-m-d H:i:s').'#'.$mt['0'];
            $resize_img = $this->resizeGoodsImg($per_goods['pic_url'], $datetime, '_g', '2');

            $per_goods['datetime'] = $datetime;
            $this->insertManyGoods($per_goods);
        }

	}



	/**
	 * 添加商品，将商品插入数据库
	 *
     */
    public function insertManyGoods($goods) {
//        $this->benchmark->mark('code_start');

        if(!empty($goods['num_iid']) && !empty($goods['cid']) && !empty($goods['title']) && !empty($goods['datetime']) && !empty($goods['price'])) {

            //没有淘宝客连接，将商品的直接链接存起来
            $url = empty($goods['click_url'])?$goods['detail_url']:$goods['click_url'];

            //对数据过滤
            $itemcatsGet = TaobaoApi::getBigclass($goods['cid'],$goods['title']);

            if(!empty($itemcatsGet)) {
                //确定lable
                $labels = substr($itemcatsGet['name'],0,-1);
                $labels = explode('#',$labels);

                $this->load->model('GoodsModel');
                $label_id = $this->GoodsModel->labels($labels);

                $label_idstr = '';
                foreach($label_id as $key=>$value) {
                    $label_idstr .= '#'.$value;
                }

                //如果不是垃圾，就判断标签，去label表中将label查出，如果存在就获取label的id，不存在的就插入数据库并且返回id

                $data = array(
                    'uid' => '2',
                    'seller' => '1',
                    'num_iid' => $goods['num_iid'],
                    'g_title' => $goods['title'],
//                    'g_intro' => $goods_info,
                    'g_show' => '2',
                    'g_label' => $label_idstr,
                    'g_type' => $itemcatsGet['g_type'],
                    'g_sex' => $itemcatsGet['g_sex'],
                    'price' => $goods['price'],
                    'g_url'=> $url,
                    'g_addtime'=> $goods['datetime']
                );

                $this->db->insert('goods', $data);

    //            $this->benchmark->mark('code_end');
    //            $ttiem = $this->benchmark->elapsed_time('code_start', 'code_end');
//                    echo 'start 数据入库','<br/><br/><br/>';

            }

        }

    }


}
?>