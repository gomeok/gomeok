<?php
/**
 * 公共模型类
 *
 * @author  michael
 */
class CommonCrud extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 查询一张表的全部数据
     * @author  michael
	 * @param	string
	 * @param 	bool
	 * @return	array
     */
    public function selects($tb) {
        $rs = $this->db->get($tb);
        return $rs->result_array();
    }

    /**
     * 插入数据
     * @author  michael
	 * @param	string
	 * @param 	array
	 * @return	bool
     */
    public function inserts($tb, $data) {
        return $this->db->insert($tb, $data);
    }

    /**
     * 删除数据
     * @author  michael
	 * @param	string
	 * @param 	array or null
	 * @return	bool
     */
    public function deletes($tb, $condition = null) {
        return $this->db->delete($tb, $condition);
    }


    /**
     * 修改数据
     * @author  michael
	 * @param	string
	 * @param 	array or null
     * @param 	array or null
	 * @return	bool
     */
    public function updates($tb, $data = null, $condition = null) {
        return $this->db->update($tb, $data, $condition);
    }

}
?>