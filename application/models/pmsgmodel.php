<?php
require_once('CommonCrud.php');
/**
 * 用户修改模型
 * @author  xingpeidong
 */
class PmsgModel extends CommonCrud {
    private $lg_email = 'email';


    public function __construct() {
        parent::__construct();
    }




    /**
     * 根据用户名搜索用户信息条数
     * @author  xingpeidong
	 * @param	string $nick
	 * @return	int
     */
    public function selMsg($uid) {
    	$query = $this->db->group_by('uid')->order_by('e_id desc')->distinct('uid')->get_where($this->lg_email, array('is_look' => 1,'o_uid' => $uid));
    	return $query->result_array();
    }




}
?>