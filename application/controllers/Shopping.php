<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('ShoppingAndCenter.php');
/**
 * 逛街类
 * @author  michael
 */
class Shopping extends ShoppingAndCenter {
    public $sex = 0;
    public $sexSelectArr;
    private $lg_comment = 'comment';
    private $lg_like = 'like';

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
        //加载表情
        $this->makeSmileys();
        if(!empty($this->user_info['sex'])) {
            $this->sex = $this->user_info['sex'];
        }
        $this->sexSelectArr = empty($this->sex)?array('1','2','3','9'):array($this->sex,'3','9');
    }

    /**
     * 默认入口
     */
    public function index() {
        die('teste');
    }

	/**
	 * 逛街
     * 进行标签点击
	 * 按钮分页160每页
     * 1,总的  2,分类型
     * sphinx 检索 1，对商品的标题检索  2，对商品标签进行//todo:标签进行商品检索还需要考虑
     */
	public function shopping() {
//        $this->benchmark->mark('code_start');   //ci基准测试

        //uri的第二段是商品类型标示
        $type = $this->uri->segment(2);

        //偏移量    没有时返回false
        $page = $this->uri->segment(3);
        if(empty($page)) {
            $page = 0;
        }
        $num = 20;

		$this->load->model('ShoppingModel');

        if($type == 9) {    //情侣(特殊类型)        //todo:情侣没有加redis
            //情侣商品总数
            $goods_total = $this->ShoppingModel->totalLoversGoods();
            //情侣商品
            $data = $this->ShoppingModel->getLoversGoods($page, $num);

        } else {
            //商品总数
            $goods_total = $this->ShoppingModel->totalGoods($this->sexSelectArr, $type, $this->sex);
            //查出与登录用户性别想匹配的数据   默认查……条
            $data = $this->ShoppingModel->selGoods($this->sexSelectArr, $type, $this->sex, $page, $num);
        }


        //分页start 每页180条
        $p = $this->pages('shopping/'.intval($type), $goods_total, '180');
        $this->smart->assign('page_str',$p);
        //分页end

        //将偏移量抛到页面，供ajax加载数据使用
        $this->smart->assign('page_offset',$page+20);

        //商品数据分为4个数组
        $goods_arr = $this->splitArr($data);

        $this->smart->assign('goods_type',$type);
        $this->smart->assign('goods',$goods_arr);
        $this->smart->display('shopping.tpl');

        //时间测试
//        $this->benchmark->mark('code_end');
//        echo $this->benchmark->elapsed_time('code_start', 'code_end');
	}

	/**
	 * 获取喜欢的商品
	 *
     */
	public function getLike() {
        $like = array();

        if(!empty($this->user_info['uid'])) {
            //$file_cache = new FileCache();

            $key_like = $this->user_info['uid'].'_like_goods';
            //调用文件缓存
            $like_goods = $this->filecache->get($key_like);     //todo:这个还不能用

            if($like_goods) {
                die('teste鈥斺€擽!');
                return $like_goods;

            } else {
                //获取该用户的喜欢的商品g_id
                $like_goods = array();
                $like_goods = $this->ShoppingModel->getLikeGoods($this->user_info['uid']);

                //将我like的商品的gid存入文件缓存
                //$this->filecache->set($key_like, $like);   //sae的文件夹没有写的权限
            }

        }
        return $like;
    }

	/**
	 * ajax加载商品
	 * 40一页
     */
	public function shopAjax() {

        $page_offset = $this->uri->segment(3);      //todo；数据验证
        $type = $this->uri->segment(2);

        //$sex_arr = empty($this->sex)?array('1','2','3','9'):array($this->sex,'3','9');

        $this->load->model('ShoppingModel');

        if($type == 9) {    //情侣(特殊类型)
            //情侣商品总数
            $goods_total = $this->ShoppingModel->totalLoversGoods();
            //情侣商品
            $goods = $this->ShoppingModel->getLoversGoods($page_offset);

        } else {
            //商品总数
            $goods_total = $this->ShoppingModel->totalGoods($this->sexSelectArr, $type, $this->sex);    //商品总数可以只查一次，然后缓存起来（mem），能提高速度
            //分页查商品
            $goods = $this->ShoppingModel->selGoods($this->sexSelectArr, $type, $this->sex, $page_offset);
        }

//        $s = new SaeStorage();
        //这里要将数组分为4个数组
        if($goods) {
            $goods = $this->splitArr($goods);

            echo json_encode($goods);die;

            //这里要将ajax查数据后的偏移量输出
//            $goods['page_offset'] = $page_offset+40;

        }
        echo 0;
	}

	/**
	 * 获取商品图片
	 *
     */
	public function getGoodsPic($data, $type='') {

        $s = new SaeStorage();

        if($type == '1') {  //返回单个商品图片
            /*
            $pic_dir = $this->makeUserDir($data['uid'], $data['g_addtime'], '_g');//返回图片路径和部分图片名（继续把图片的大小标示和后缀拼上）
            $pic = $s->getUrl( 'lovegoimg' , $pic_dir.'_b.jpg' );
            $data['pic'] = $pic;
            */

            $data['pic'] = $this->getTestImg();        //这个是本地sae的代码


        } else {    //返回多个商品图片

            foreach($data as $key=>$value) {
                /*
                $pic_dir = $this->makeUserDir($value['uid'], $value['g_addtime'], '_g');//返回图片路径和部分图片名（继续把图片的大小标示和后缀拼上）
                $pic = $s->getUrl( 'lovegoimg' , $pic_dir.'_m.jpg' );
                $data[$key]['pic'] = $pic;
                */

                $data[$key]['pic'] = $this->getTestImg();        //这个是本地sae的代码
            }
        }
        return $data;
    }

	/**
	 * 单个商品页       //todo:这个页面的加载时间有点长
	 *
     */
	public function goods() {
        $this->benchmark->mark('code_start');   //ci基准测试

        $g_id = $this->uri->segment(2);                             //todo:数据验证
        $g_id-=10000;   //商品gid-10000  是真实的g_id

        $webUrl = $this->config->item('base_url');
        $this->load->model('ShoppingModel');

        //根据商品g_id获取hash目录
        $goodsTplCacheDir = $this->makeHashDir($g_id);
        //smarty会自动创建缓存目录
        $cacheDir = APPPATH.'cache/goodshtml/'.$goodsTplCacheDir.'/';
        //设置tpl静态化的目录
        $this->smart->setCacheDir($cacheDir);

        //将单个商品页部分静态化（评论不静态）
        $this->smart->caching = true;
        if(!$this->smart->isCached('goods.tpl', $g_id)) {

            //查商品数据
            $data = $this->ShoppingModel->selOneGoods($g_id);
            $data = $this->getGoodsPic($data,'1');

            $this->smart->assign('goods',$data);

            $label_arr = array_filter(explode('#',$data['g_label']));
            //查标签，返回标签名称
            $labels = $this->ShoppingModel->selLabel($label_arr);
            $label = array();
            if(!empty($labels)) {
                foreach($labels as $key=>$value) {
                    $label[] = $value['label'];
                }
            }
            $this->smart->assign('label',$label);

            //查看我是否like该商品  //todo:所有喜欢的商品gid，在用户登录的时候，一次查出来，存到缓存里。 =》目前是在查数据库
            $isLike = $this->ShoppingModel->isLike($this->user_info['uid'], $g_id);
            $this->smart->assign('isLike',$isLike);

            //推荐商品
            $arr = $this->ShoppingModel->selLabelGoods($g_id, $this->sexSelectArr, $label_arr);      //, $page, $num
            $arr = $this->getGoodsPic($arr);

            $rec_goods = $this->splitArr($arr);
            $this->smart->assign('rec_goods',$rec_goods);
            //推荐商品 end

            //如果用户登录了，就查他的关注的人
            $myAttention = array();
            if(!empty($this->user_info['uid'])) {
                $myAttention = $this->redisAttention($this->user_info['uid']);
            }
            //查商品的所有者（用户）资料
            $userInfo = $this->ShoppingModel->getUserInfo($data['uid'], $this->user_info['uid'], $myAttention);
            $this->smart->assign('userInfo', $userInfo);

        }

        //查评论
        $comment = $this->ShoppingModel->selComment('1',$g_id);
        foreach($comment as $key => $value) {
            $comment[$key]['c_content'] = $this->getSmileys($value['c_content']);   //获取评论内容中的表情
        }

        //评论总数
        $c_total = $this->ShoppingModel->totalComment('1',$g_id);
        $page_total = ceil($c_total/8);  //评论总页数
        $this->smart->assign('c_total',$page_total);
        $this->smart->assign('comment',$comment);
        //评论 end

        $this->smart->display('goods.tpl', $g_id);

        //todo:以下是时间测试
        $this->benchmark->mark('code_end');
        echo $this->benchmark->elapsed_time('code_start', 'code_end');
	}

	/**
	 * 喜欢商品 ajax
     * @echo    int    数据库中喜欢数
     */
	public function likeGoods() {
        //判断用户是否登录
        $is_login = $this->isLogin(true);
        if($is_login) {

            $g_id = intval($this->input->get('gid'));    //todo:******数据过滤********
            $uid = intval($this->input->get('uid'));    //******数据过滤********

            $g_id -= 10000;

            $this->load->model('ShoppingModel');

            //统计表 1, 修改当前用户的统计  2，修改被喜欢用户的统计
            $this->ShoppingModel->updateTongJi($this->user_info['uid']);   //喜欢
            $this->ShoppingModel->updateTongJi($uid, false);   //被喜欢

            //insert 喜欢表
            $this->db->insert($this->lg_like, array('uid' => $this->user_info['uid'], 'x_id' => $g_id));

            //商品喜欢数 +1
            $like_num = $this->ShoppingModel->updateGoodsLikeNum($g_id);

            echo $like_num;
        } else {
            echo 0;
        }
	}


}
?>