    Ext.onReady(function() {

        //定义的数据字段
        Ext.define('users', {
            extend: 'Ext.data.Model',
            fields: ['g_id', 'uid','seller','g_title','g_show','g_comment_num','g_like_num','g_type','g_sex','g_addtime','price','color']
        });

        Ext.define('System.Grid', {
            extend: 'Ext.grid.Panel',
            alias: 'widget.writergrid',
            requires: [
            'Ext.form.field.Text',
            'Ext.toolbar.TextItem',
            'Ext.toolbar.Paging',
            'Ext.data.*'
            ],
            initComponent: function(){

                Ext.apply(this, {
                    iconCls: 'icon-grid',
                    frame: true,
                    columns: [
                    {
                        header: '商品id',
                        sortable: false,
                        dataIndex: 'g_id',
                        flex: 1
                    },{
                        header: '用户id',
                        dataIndex: 'uid',
                        flex: 1
                    },{
                        header: '卖家',
                        dataIndex: 'seller',
                        flex: 1
                    },{
                        header: '商品名称',
                        dataIndex: 'g_title',
                        flex: 1
                    },{
                        header: '是否显示',
                        dataIndex: 'g_show',
                        flex: 0
                    },{
                        header: '评论数',
                        dataIndex: 'g_comment_num',
                        flex: 1
                    },{
                        header: '喜欢数',
                        dataIndex: 'g_like_num',
                        flex: 1
                    },{
                        header: '类型',
                        dataIndex: 'g_type',
                        flex: 1
                    },{
                        header: '性别',
                        dataIndex: 'g_sex',
                        flex: 1
                    },{
                        header: '价格',
                        dataIndex: 'price',
                        flex: 1
                    },{
                        header: '颜色',
                        dataIndex: 'color',
                        flex: 1
                    },{
                        header: '发布时间',
                        dataIndex: 'g_addtime',
                        flex: 1
                    }
                    ],
                    selModel: new Ext.selection.CheckboxModel(),
                    columnLines: true   //为true是，复选框只能选一行
                });
                this.callParent();
                this.getSelectionModel().on('selectionchange', this.onSelectChange, this);
            }

        });

    //ajax加载数据
        store = Ext.create('Ext.data.Store', {
            model: 'users',
            autoLoad: true,
            autoSync: true,
            params: {
                start:0,
                limit:25
            },
            proxy: {
                type: 'ajax',
                api: {
                    read: a_global.a_weburl+'GoodsAction/goodsList'
                },
                reader: {
                    type: 'json',
                    totalProperty: 'total',
                    successProperty: 'success',
                    root: 'data',
                    messageProperty: 'message'
                },
                listeners: {
                    exception: function(proxy, response, operation){
                        Ext.MessageBox.show({
                            title: '错误信息',
                            msg: operation.getError(),
                            icon: Ext.MessageBox.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            }
        });

//        var store = Ext.get('json');
//        alert(store);

        //将数组展示
        var itemsPerPage = 25;
        main = Ext.create('Ext.container.Container', {
            layout:'fit',
            renderTo: 'center1',
            items: [{
                itemId: 'grid',
                xtype: 'writergrid',
                title: '统计',
                flex: 1,
                store: store,
                bbar: Ext.create('Ext.PagingToolbar', {
                    store: store,
                    displayInfo: true,
                    displayMsg: '显示 {0} - {1} of {2}',
                    emptyMsg: "无数据显示"
                }),
                listeners: {
                    selectionchange: function(selModel, selected) {
                    }
                }
            }]
        });


});
