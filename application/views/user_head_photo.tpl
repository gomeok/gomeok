{!config_load file="web.inc.conf"!}
{!include file="inc/header.tpl"!}

{!include file="inc/other.tpl"!}

<script src="{!#js_path#!}jqxj/jquery.Jcrop.min.js" type="text/javascript"></script>
<link href="{!#css_path#!}jqxjcss/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{!#js_path#!}imagecrop.js"></script>

<div class="clear"></div>
<!-- <div class="banner in-width1">
<img src="images/banner1.jpg"  height="301" width="955" border="0" />
</div> -->
<div class="footbg in-width"></div>
</div>
<!--top end-->

<!--content begin-->
<div id="content">
<div class="topbg"></div>

    <div class="content1-m middlebg clearfix">

        <div class="ziliao-l blue-borderl fl">
            <ul>
            <li><a href="">基本资料</a></li>
            <li><a class="bluebg" href="">修改头像</a></li>
            <li><a href="">修改密码</a></li>
            <li><a href="">应用授权</a></li>
            <li><a href="">买件认证</a></li>
            <li><a href="">手机认证</a></li>
            </ul>
        </div>


        <div class="ziliao-r fr">
        <div class=" clearfix">
        <div class="update-l fl">
        <div class="update-big" style="width:280px;height:280px;overflow:hidden;">{!if isset($img_url)!}<img src="{!$img_url!}" id="target"/>{!/if!}</div>


        <form action="{!#weburl#!}modhead" enctype="multipart/form-data" method="post" id="uphead">
                
            <div class="update-btn">
                <input value="上传图片" class="bluebg" name="" type="button" id="upload"/>
            </div>
            <p class="line24">支持jpg、gif、png格式的图片，且文件小于2M</p>
            <input name="userfile" type="file" id="upload_file"/><style>body{ overflow-x:hidden;}</style>
                <!--head_photo multiple  style="position:absolute; left:999px;" onchange="up()"-->

        </form>

        </div>
        <div class="update-r fr">
        <p class="font14 blue"><b>预览图像</b></p>
        <div class="update-img" style="width:200px;height:200px;overflow:hidden;">{!if isset($img_url)!}<img src="{!$img_url!}" id="preview"/>{!/if!}</div>
        <p class="update-word">个人头像预览，其他尺寸头像系统将为您自动生成为保证头像的清晰度，请注意预览区域的效果</p>
        </div>
        </div>
        <div class="baocun">
        <form method="post" action="{!#weburl#!}modhead">
            <input type="text" name='w' id="w" value='200'/>
            <input type="text" name='h' id="h" value='200'/>
            <input type="text" name='mleft' id="mleft" />
            <input type="text" name='mtop' id="mtop" />
            <input type="text" name='width' id="width" value=''/>
            <input type="text" name='height' id="height" value=''/>
            <input value="保存头像" class="baocuninput bluebg" name="crop_head" type="submit" />
        </form>
        </div>
        </div>

    </div>

<div class="footbg"></div>
</div>
<!--content end-->

<!--footer begin-->
{!include file="inc/footer.tpl"!}
<!--footer end-->
</body>
</html>