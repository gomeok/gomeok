//var forma;
//
//Ext.require([
//             'Ext.form.*',
//	         'Ext.window.Window',
//	         'Ext.window.MessageBox',
//	         'Ext.tip.*'
//]);

Ext.onReady(function(){
	 Ext.QuickTips.init();
	 forma = Ext.create('Ext.form.Panel',{
         border: false,
         defaultType: 'textfield',
         fieldDefaults: {
            labelWidth: 60,
            anchor: '85%'
         },

         bodyStyle:'padding:35px 0  35px  20px',
//         url: submit_url,
         method: 'POST',
//         items: [{
//    	 fieldLabel: '用户名',
//	          name: 'loginname',
//	          allowBlank:false,
//	          blankText:'用户名不能为空',
//	          value: username
//	     },{
//	     fieldLabel: '密码',
//	          name: 'password',
//	          allowBlank:false,
//	          inputType:'password',
//	          blankText:'密码不能为空',
//	          value: pwd
//	     },{
//			  xtype:'checkbox',
//	    	  name:'remind',
//	    	  hideLabel:true,
//	    	  boxLabel:'在此计算机上保存我的信息'
//	    }]
    });

//    var win = Ext.create('Ext.window.Window', {
//        title: '后台系统登陆',
//        width: 350,
//        height:200,
//        minWidth: 200,
//        minHeight: 150,
//        layout: 'fit',
//        plain: true,
//        items: forma,
//        closable : false,
//        buttons: [{
//            text: '登陆',
//            handler: function() {
//            	if(win.items.items[0].getForm().isValid())
//	            {
//            		var form = win.items.items[0].getForm().submit(
//	                        {
//	                           	waitTitle : '请稍候',
//	                          	waitMsg : '正在登录......',
//	                          	clientValidation: true,
//	                          	url: submit_url,
//	                          	method: 'POST',
//	                            success : function(form, action) {
//	                            	win.items.items[0].body.mask('登入中，请稍等...', 'x-mask-loading');
//	                            	window.location = action.result.url;
//	                            },
//	                            failure : function(form, action) {
//	                                Ext.MessageBox.alert('信息', action.result.error);
//	                            }
//	                        }
//                    );
//
//	            }
//            }
//        }//,{
////            text: '重置',
////            handler: function() {
////            	win.items.items[0].getForm().reset();
////            }
////        },{
////        	text:'忘记密码?',
////        	handler: function(){
////        		Ext.MessageBox.prompt('忘记密码', '请输入您的邮箱地址:', showResultText);
////        }
//        }]
//    });

//    win.show();
//    forma.render('main');
//
//    function showResultText(btn, text){
//    	Ext.showmsg.msg('Button Click', 'You clicked the {0} button and entered the text "{1}".', btn, text);
//	};

    Ext.create('Ext.window.Window', {
        title: 'login',
        height: 200,
        width: 400,
        layout: 'fit',
//        items: {  // Let's put an empty grid in just to illustrate fit layout
//            xtype: 'grid',
//            border: false,
//            columns: [{header: 'World'}],                 // One header just for show. There's no data,
//            store: Ext.create('Ext.data.ArrayStore', {}) // A dummy empty data store
//        },
        //items:forma,
        buttons: [{
            text: '登陆',
            handler: function() {
            	if(win.items.items[0].getForm().isValid())
	            {
            		var form = win.items.items[0].getForm().submit(
	                        {
	                           	waitTitle : '请稍候',
	                          	waitMsg : '正在登录......',
	                          	clientValidation: true,
	                          	url: submit_url,
	                          	method: 'POST',
	                            success : function(form, action) {
	                            	win.items.items[0].body.mask('登入中，请稍等...', 'x-mask-loading');
	                            	window.location = action.result.url;
	                            },
	                            failure : function(form, action) {
	                                Ext.MessageBox.alert('信息', action.result.error);
	                            }
	                        }
                    );

	            }
            }
        },{
            text: '重置',
            handler: function() {
            	win.items.items[0].getForm().reset();
            }
        },{
        	text:'忘记密码?',
        	handler: function(){
        		Ext.MessageBox.prompt('忘记密码', '请输入您的邮箱地址:', showResultText);
        }
        }]

    }).show();

    forma.render('main');

});