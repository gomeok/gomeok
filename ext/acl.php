<?php
// $Id$

/**
 * Acl资源管理类 封装来自 lewan_members 数据表的记录及领域逻辑
 */
class Acl extends QDB_ActiveRecord_Abstract
{
    /**
     * 返回对象的定义
     *
     * @static
     *
     * @return array
     */
    static function __define()
    {
        return array
        (
            // 指定该 ActiveRecord 要使用的行为插件
            'behaviors' => '',

            // 指定行为插件的配置
            'behaviors_settings' => array
            (
                # '插件名' => array('选项' => 设置),
            ),

            // 用什么数据表保存对象
            'table_name' => 'acl',

            // 指定数据表记录字段与对象属性之间的映射关系
            // 没有在此处指定的属性，QeePHP 会自动设置将属性映射为对象的可读写属性
            'props' => array
            (
                // 主键应该是只读，确保领域对象的“不变量”
                'id' => array('readonly' => true),

                /**
                 *  可以在此添加其他属性的设置
                 */
                # 'other_prop' => array('readonly' => true),

                /**
                 * 添加对象间的关联
                 */
                # 'other' => array('has_one' => 'Class'),

            ),

            /**
             * 允许使用 mass-assignment 方式赋值的属性
             *
             * 如果指定了 attr_accessible，则忽略 attr_protected 的设置。
             */
            'attr_accessible' => '',

            /**
             * 拒绝使用 mass-assignment 方式赋值的属性
             */
            'attr_protected' => 'uuid',

            /**
             * 指定在数据库中创建对象时，哪些属性的值不允许由外部提供
             *
             * 这里指定的属性会在创建记录时被过滤掉，从而让数据库自行填充值。
             */
            'create_reject' => '',

            /**
             * 指定更新数据库中的对象时，哪些属性的值不允许由外部提供
             */
            'update_reject' => '',

            /**
             * 指定在数据库中创建对象时，哪些属性的值由下面指定的内容进行覆盖
             *
             * 如果填充值为 self::AUTOFILL_TIMESTAMP 或 self::AUTOFILL_DATETIME，
             * 则会根据属性的类型来自动填充当前时间（整数或字符串）。
             *
             * 如果填充值为一个数组，则假定为 callback 方法。
             */
            'create_autofill' => array
            (
                # 属性名 => 填充值
                # 'is_locked' => 0,
            ),

            /**
             * 指定更新数据库中的对象时，哪些属性的值由下面指定的内容进行覆盖
             *
             * 填充值的指定规则同 create_autofill
             */
            'update_autofill' => array
            (
            ),

            /**
             * 在保存对象时，会按照下面指定的验证规则进行验证。验证失败会抛出异常。
             *
             * 除了在保存时自动验证，还可以通过对象的 ::meta()->validate() 方法对数组数据进行验证。
             *
             * 如果需要添加一个自定义验证，应该写成
             *
             * 'title' => array(
             *        array(array(__CLASS__, 'checkTitle'), '标题不能为空'),
             * )
             *
             * 然后在该类中添加 checkTitle() 方法。函数原型如下：
             *
             * static function checkTitle($title)
             *
             * 该方法返回 true 表示通过验证。
             */
            'validations' => array
            (
            ),
        );
    }
	
    /**
     * @description: 获取给定id所确定的权限在其所在的层次关系中的等级
     * @param	int		$id		权限id
     * @return	int				代表层次的数字, 若给定的id不存在, 返回0, 否则返回正整数
     */
	static function getMenuLevelById ($id) {
		$level    = 0;
		$menuInfo = self::getMenuById($id);
		if ($menuInfo) {
			$level = 1;
			while ($menuInfo['pid']!=0) {
				$menuInfo = self::getMenuById($menuInfo['pid']);
				$level++;
			}
		}
		return $level;
	}
	
	/**
	 * @description: 获取给定的 id 所确定的纪录的所有父纪录作为数组返回
	 * @param	int		$id		给定的纪录的id值
	 * @return	array			所有的父级纪录的数组
	 */
	static function getMenuParent ($id) {
		$item        = self::getMenuById($id);
		$returnArray = array();
		$pid         = $item['pid'];
		
		while ($pid != 0) {
			$parentMenu    = self::getMenuById($pid);
			$pid           = $parentMenu['pid'];
			$returnArray[] = $parentMenu;
		}
		
		return $returnArray;
	}
    	
	static function getCategory_menu()
	{
		return Acl::find()->where( "acl_type='menu' and pid=0" )->asArray()->getAll();
	}

	static function getMenus($pid,$search)
	{
		$str = '';
		if( !empty($search) ) $str = " and acl_name='%".$search."%'";
		$acl_table = Acl::meta()->table->getFullTableName();
		$acl_table_self = Acl::meta()->table->getFullTableName();
		return Acl::find()
						->joinLeft(array('m'=>$acl_table),'','m.id = '.$acl_table_self.'.pid')
						->where( $acl_table_self.".acl_type='menu' and ".$acl_table_self.".pid=".$pid.$str )
						->setColumns("id,acl_name,acl_value,description,m.acl_name as p_acl_name,m.id as p_acl_id")
						->asArray()
						->getAll();
	}
	static function getMenusByPid( $pid, $page, $num, $search  )
	{
		$str = '';
		$acl_table = Acl::meta()->table->getFullTableName();
		$acl_table_self = Acl::meta()->table->getFullTableName();
		if( !empty($search) ) $str = " and {$acl_table_self}.acl_name like '%".$search."%'";
		if( $pid!='' ) 
		{
			return Acl::find()
								->joinLeft(array('m'=>$acl_table),'','m.id = '.$acl_table_self.'.pid')
								->where( $acl_table_self.".acl_type='menu' and ".$acl_table_self.".pid=".$pid.$str )
								->limitPage($page, $num)
								->setColumns("id,acl_name,acl_value,description,m.acl_name as p_acl_name,m.id as p_acl_id")
								->asArray()
								->getAll();
		}
		else
		{
			$obj = Acl::getCategory_menu();
			$str_ids = '';
			foreach( $obj AS $key=>$val )
			{
				$str_ids .= $val['id'].',';
			}
			$str_ids = trim( $str_ids,',' );
			return Acl::find()
							->joinLeft(array('m'=>$acl_table),'','m.id = '.$acl_table_self.'.pid')
							->where( $acl_table_self.".acl_type='menu' ".$str."  and ".$acl_table_self.".pid in (".$str_ids.")" )
							->limitPage($page, $num)
							->setColumns("id,acl_name,acl_value,description,m.acl_name as p_acl_name,m.id as p_acl_id")
							->asArray()
							->getAll();		
		}
	}
	
	/**
	 * @description: 获取给定id所确定的权限的全部信息并返回
	 * @param	int		$id		表主键
	 * @return					代表一项表格纪录的hash表
	 */
	static function getMenuById ($id) {
		return Acl::find( 'id=?',$id )->asArray()->getOne();	
	}
	
	static function getMenunameById( $id )
	{
		return Acl::find( 'id=?',$id )->setColumns('acl_name')->asArray()->getOne();		
	}
	
	static function getMenunameByPid( $id )
	{
		return Acl::find( 'pid=?',$id )->order("id ASC")->asArray()->getAll();		
	}
	
	static function getTotal( $id )
	{
		if( $id!='' )
			return Acl::find( 'pid=?',$id )->getCount();
		else
			return Acl::find()->getCount();
	}
	
	static function del( $id )
	{
		if( empty($id) )
    	{
    		throw new Exception( '传入参数有误，请检查.' );
    	}
    	try {
			Acl::meta()->deleteWhere( 'id = ?',$id );
    		return true;    		
    	} catch (Exception $e) {
    		return $e;
    	}
	}
	
	static function edit( $arr,$id )
	{
		Acl::meta()-> updateDbWhere( $arr,'id =?',$id );
	}
	
	static function add( $arr )
	{
		$obj = new self();
		$obj->pid = $arr['pid'];
		$obj->acl_name = $arr['acl_name'];
		$obj->acl_value = $arr['acl_value'];
		$obj->description = $arr['description'];
		$obj->acl_type = $arr['acl_type'];
		$obj->save();
		return $obj->id;
	}
	
	/**
	 * 获取系统菜单
	 * Enter description here ...
	 */
	static function getSystemMenus( $type )
	{
		$syslist = self::find("acl_type='menu' AND pid=?",$type)->asArray()->getAll();
		$list = array();
		foreach( $syslist AS $key=>$val )
		{
			$c_list = self::getMenus2( $val['id'] );
			$list[] = array(
								'text'=>$val['acl_name'],
								'id'=>$val['acl_type'].'_'.$val['id'],
								'cls'=>"folder",
								"expanded"=>true,
								"children"=>$c_list
							);			
		}
		return $list;
	}
	/**
	 * 获取二级菜单
	 * Enter description here ...
	 */
	static function getMenus2( $pid )
	{
		$menulist = self::find("pid=? AND acl_type='menu'",$pid)->asArray()->getAll();
		$list = array();
		foreach( $menulist AS $key=>$val )
		{
			$c_list = self::getMenus3( $val['id'] );
			$list[] = array(
								'text'=>$val['acl_name'],
								'id'=>$val['acl_type'].'_'.$val['id'],
			 					'cls'=>'folder',
								'expanded'=>false,
								'children'=>$c_list
							);
		}
		return $list;
	}
	/**
	 * 获取三级菜单
	 * Enter description here ...
	 */
	static function getMenus3( $pid )
	{
		$menulist = self::find("pid=? AND acl_type='menu'",$pid)->asArray()->getAll();
		$list = array();
		foreach( $menulist AS $key=>$val )
		{
			$list[] = array(
								'text'=>$val['acl_name'],
								'id'=>$val['acl_type'].'_'.$val['id'],
								'leaf'=>true,
								'action'=>array(
													'url'=>url($val['acl_value']),
													'target'=>'tab'
												)
							);
		}
		return $list;
	}
    
	/**
	 * 根据级别展示acl资源
	 * Enter description here ...
	 */
	static function showAclByLevel()
	{
		return self::cat_list(0);		
	}
	
	static function cat_list($deptid=0,$re_type = false, $level = 0, $is_show_all = true)
	{
		$db = QDB::getConn();
		static $res = NULL;
		if ($res === NULL)
    	{
    		$sql = "SELECT	c.id,
    						c.acl_name,
    						c.acl_value,
    						c.acl_type,
    						c.description,
    						c.pid,
    						COUNT(s.id) AS has_children
    				  FROM 	lewan_acl AS c 
    			 LEFT JOIN 	lewan_acl AS s ON s.pid=c.id
    			  GROUP BY 	c.id
    			  ORDER BY 	c.pid ASC
    			 ";
            $res = $db->getAll($sql);
       	}
		if (empty($res) == true)
	    {
	        return $re_type ? '' : array();
	    }
		$options = self::cat_options($deptid, $res); // 获得指定分类下的子分类的数组
		
		$children_level = 99999; //大于这个分类的将被删除
		if ($is_show_all == false)
	    {
	        foreach ($options as $key => $val)
	        {
	            if ($val['level'] > $children_level)
	            {
	                unset($options[$key]);
	            }
	        }
	    }
	    
		/* 截取到指定的缩减级别 */
	    if ($level > 0)
	    {
	        if ($deptid == 0)
	        {
	            $end_level = $level;
	        }
	        else
	        {
	            $first_item = reset($options); // 获取第一个元素
	            $end_level  = $first_item['level'] + $level;
	        }
	
	        /* 保留level小于end_level的部分 */
	        foreach ($options AS $key => $val)
	        {
	            if ($val['level'] >= $end_level)
	            {
	                unset($options[$key]);
	            }
	        }
	    }
		if ($re_type == true)
	    {
	        $select = '';
	       	//这里传递json格式的数据到Extjs
	
	        return $select;
	    }
	    else
	    {
	        return $options;
	    }
	    
	}
	/**
	 * 过滤和排序所有分类，返回一个带有缩进级别的数组
	 *
	 * @access  private
	 * @param   int     $cat_id     上级分类ID
	 * @param   array   $arr        含有所有分类的数组
	 * @param   int     $level      级别
	 * @return  void
	 */
	static function cat_options($spec_cat_id, $arr)
	{
		static $cat_options = array();
		if (isset($cat_options[$spec_cat_id]))
	    {
			return $cat_options[$spec_cat_id];
	    }
	   /*
        	初始化关键参数:
        	$level:当前子节点深度
        	$last_cat_id:当前父节点ID
        	$options:带有缩进级别的数组
        	$cat_id_array:沿同一路径的父节点依次进驻
        	$level_array:该节点的子节点深度,也是依次进驻
       */
	   if (!isset($cat_options[0]))
	   {
	   	
	   		$level = $last_cat_id = 0;
	   		$options = $cat_id_array = $level_array = array();
	   		while (!empty($arr))//如果还有待构造的节点则继续遍历
	   		{
	   			foreach ($arr AS $key => $value)
	   			{
					
	   				$cat_id = $value['id'];
	   				//一级分类结点
	   				if ($level == 0 && $last_cat_id == 0)
                	{
	                	if ($value['pid'] > 0)
	                    {
	                        break;
	                    }
	                    $options[$key] = $value;
                    	$options[$key]['level'] = $level;
                    	$options[$key]['id']    = $cat_id;
                    	$options[$key]['acl_name'] = '<b>'.str_repeat('&nbsp;', $level * 5).$value['acl_name'].'</b>'; 
                    	//遍历过了就不再遍历
                    	unset($arr[$key]);
                    	if ($value['has_children'] == 0)
                    	{
                    		continue;
                    	}
                    	$last_cat_id  = $cat_id;//下层结点的父亲结点
                    	$cat_id_array = array($cat_id);
                    	$level_array[$last_cat_id] = ++$level;
                    	continue;
                	}
                	//当前结点的父亲结点ID等于它的上一级结点ID
                	if ($value['pid'] == $last_cat_id)
                	{
                		$options[$key]          = $value;
                    	$options[$key]['level'] = $level;
                    	$options[$key]['id']    = $cat_id;
                    	$options[$key]['acl_name'] = '<b>'.str_repeat('&nbsp;', $level * 5).$value['acl_name'].'</b>'; 
                    	unset($arr[$key]);//遍历过了就不再遍历
                    	//如果当前结点有孩子则当前结点要进驻,但不再遍历;反之不进驻也不再遍历
                    	if ($value['has_children'] > 0)
                    	{
	                    	if (end($cat_id_array) != $last_cat_id)
	                        {
	                            $cat_id_array[] = $last_cat_id;
	                        }
	                        $last_cat_id    = $cat_id;//当现结点做为下一级结点的新的父亲结点
	                        $cat_id_array[] = $cat_id;//进驻
	                        $options[$key]['acl_name'] = '<b>'.str_repeat('&nbsp;', $level * 5).$value['acl_name'].'</b>'; 
	                        $level_array[$last_cat_id] = ++$level;//当前结点的下一级结点深度
                    	}
                	}
                	elseif ($value['pid'] > $last_cat_id)
                	{
                		//如果当前结点父亲深度大于目前父亲结点的深度则进行下一轮循环
                		break;
                	}
	   			}
	   			//endforeach
	   			
	   			$count = count($cat_id_array);
		   		if ($count > 1)
	            {
	                //取出最后进驻的父亲节点作为当前父亲节点
	                $last_cat_id = array_pop($cat_id_array);
	            }
		   		elseif ($count == 1)
	            {
	                if ($last_cat_id != end($cat_id_array))
	                {
	                	//进驻的父亲结点只有一个时并且没有作为当前父亲节点时把它取出
	                    $last_cat_id = end($cat_id_array);
	                }
	                else
	                {   
	                	//否则最后取出的父亲结点一定是一级分类结点
	                    $level = 0;
	                    $last_cat_id = 0;
	                    $cat_id_array = array();
	                    continue;
	                }
	            }
	            
		   		if ($last_cat_id && isset($level_array[$last_cat_id]))
	            { 
	               //取出当前结点的深度
	                $level = $level_array[$last_cat_id];
	            }
	            else
	            { 
	                $level = 0;
	            }
	   		}//end while,此时已完成非递归前序遍历构造树的工作,其中$options已保存了从根结点开始的所有结点带有分层性质的数组
	   		$cat_options[0] = $options;
	   	}
		else
	    {
	        $options = $cat_options[0];
	    }
	    
	    //如果从0开始即取整个树则直接返回不再处理.
		if (!$spec_cat_id)
	    {
	        return $options;
	    }
	    //否则开始从指定结点截取
	    /*
    		$spec_cat_id_level:截取结点的深度
    		$spec_cat_id_array:最终返回的以该结点为根结点的一棵商品分类树
   			 最终返回的数组是这样排序的:按父亲结点大小,按直接父亲结点,按同一父亲结点这样的先根遍历,具个例子:
   			 一级结点有1,5 二级结点有2,6,7 三级结点有8,9,如果1的直接孩子是2,6而2的直接孩子是8,9;另外
    		5的直接孩子是7那么最终的数组是这样排列的1->2->8->9->6->5->7
    	*/
		else
	    {
	        if (empty($options[$spec_cat_id]))
	        {
	            return array();
	        }
	        $spec_cat_id_level = $options[$spec_cat_id]['level'];
	   
	        foreach ($options AS $key => $value)
	        {
	            if ($key != $spec_cat_id)
	            {
	                unset($options[$key]);
	            }
	            else
	            {
	                break;
	            }
	        }
	        $spec_cat_id_array = array();
	        foreach ($options AS $key => $value)
	        {
	            if (($spec_cat_id_level == $value['level'] && $value['id'] != $spec_cat_id) ||
	                ($spec_cat_id_level > $value['level']))
	            {
	                break;
	            }
	            else
	            {
	                $spec_cat_id_array[$key] = $value;
	            }
	        }
	        $cat_options[$spec_cat_id] = $spec_cat_id_array;
	        return $spec_cat_id_array;
	    }
	}
	
	
/* ------------------ 以下是自动生成的代码，不能修改 ------------------ */

    /**
     * 开启一个查询，查找符合条件的对象或对象集合
     *
     * @static
     *
     * @return QDB_Select
     */
    static function find()
    {
        $args = func_get_args();
        return QDB_ActiveRecord_Meta::instance(__CLASS__)->findByArgs($args);
    }

    /**
     * 返回当前 ActiveRecord 类的元数据对象
     *
     * @static
     *
     * @return QDB_ActiveRecord_Meta
     */
    static function meta()
    {
        return QDB_ActiveRecord_Meta::instance(__CLASS__);
    }


/* ------------------ 以上是自动生成的代码，不能修改 ------------------ */

}

