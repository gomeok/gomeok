// common js fun
var global = {
	weburl:'http://love.sinaapp.com/',
    ajaxget:function(uri, datas, callback, types) {
		var types = (!types)?'text':types;
		$.get(global.weburl+uri, datas, callback, types);
	},
    ajaxpost:function(uri, datas, callback, types) {
		var types = (!types)?'text':types;
		$.post(global.weburl+uri, datas, callback, types);
	}
}

//sae本地测试随机获取图片
//Math.round(Math.random());


//默认头像
var head_img;

$(function() {
    head_img = $('#head_img').val();    //todo:已不用，待删除

    //用户信息弹框div           最外面的class 有个‘clearfix’ ，我去掉了，要不ie7不行，clearfix是干什么的？
    info_div = '<div class="man-ceng1 showInfo" style="position:absolute;"><span class="cor-top1"><span class="cor-top2"></span></span><img src="'+global.weburl+'/public/images/wait.gif" id="wait" class="hidden" /><div class="clearfix userInfo hidden"><p class="ceng1-img"><img src="#" height="60" width="60" border="0" id="head_img"/></p><div class="ceng1-word"><h3 class="blue nick">牙牙</h3><p class="ceng1-p"><span class="pro"></span><span class="city"></span></p><p class="ceng1-fx"><b class="blue" id="share">0</b>分享<b class="blue mar5" id="pink">0</b>粉丝<b class="blue mar5" id="like">0</b>喜欢</p></div><div class="ceng-admin info"></div><a class="fl bluebg att_btn" href="javascript:;">关注</a></div></div>';
    $('body').append(info_div);


/////////////用户信息弹框////////////
    //鼠标悬浮，获取用户信息 使用live,是有的类是后面通过ajax加载进来的，需要事件委派
    //4中类会取用户信息
    $('.getuserinfo,.nopeople,.boy,.girl,.GG,.MM,.NP').live('mouseover', function() {
        var me = $(this);

        var uid = me.attr('rel');

        var uid_info = $('body').data(uid);
        var pos = me.offset();
        var height = me.height();

        //弹框top 和 left
        var top = pos.top+height+6;
        var left = pos.left;

        var cor = $('.cor-top1');   //尖角的元素对象
        $('.showInfo,#wait').css({'top':top, 'left':left, 'border-color':'#CCCCCC'}).show();
        cor.css('border-bottom-color','#CCCCCC');    //尖角的颜色

        if (typeof(uid_info) == 'undefined' && typeof(uid) != 'undefined') {
            global.ajaxget('getUInfo?uid='+uid, '', function(uid_info) {

                $('body').data(uid, uid_info);  //将json数据缓存到文档中
                showUserInfo(uid_info, top, left);

            },'json');

        } else if(typeof(uid) != 'undefined') { //已存在缓存
            showUserInfo(uid_info, top, left);

        } else {
            cor.css('border-bottom-color','orange');    //尖角的颜色
            var error = '不存在';
            showUserInfo('', top, left, error);
        }

    });

    //鼠标移开隐藏
    $('.getuserinfo,.nopeople,.boy,.girl,.GG,.MM,.NP').live('mouseout', function() {
        $('.showInfo').hide();
        //弹框隐藏的时候需要将存放用户信息的div再次隐藏掉
        $('.userInfo').hide();
    });
    //弹框悬浮显示隐藏
    $('.showInfo').hover(
        function() {
            $(this).show();
            if ($('#wait').css('display') == 'none') {      //wait的图片隐藏了，用户info的div才显示
                $('.userInfo').show();
            }
        },
        function() {
            $(this).hide();
            //弹框隐藏的时候需要将存放用户信息的div再次隐藏掉
            $('.userInfo').hide();
        }
    );


    //用户资料弹框信息
    function showUserInfo(uid_info, top, left, error) {
        var userInfoBox = $('.showInfo');

        if (typeof(error) != 'undefined') {    //用户不存在
            $('.nick').html(error);
            $('.info').html(error);

            $('.att_btn').hide();
            userInfoBox.css({'border-color':'orange'});

        } else if (typeof (error) == 'undefined') {     //用户存在

            $('.nick').html(uid_info.nick);
            $('.pro').html(uid_info.province);
            $('.city').html(uid_info.city);
            $('.info').html(uid_info.sign);
            $('.att_btn').attr('rel',uid_info.uid);
            $('#share').html(uid_info.share);
            $('#pink').html(uid_info.pink);
            $('#like').html(uid_info.like);
            //alert(uid_info.head_img);
            $('#head_img').attr('src',uid_info.head_img);

            //$('#att_uid').val(uid_info.uid);    //要关注用户的uid

            //判断是否是自己
            var h_uid = $('#uid').val();
            if (h_uid == uid_info.uid) {
                $('.info').html('连自己也不认识了wwwwwwwwwwwwwwwwwwwwwwwwwww');
                $('.att_btn').hide();
            } else {
                $('.att_btn').show();
            }

            if (uid_info.att_uid == null) {   //att_uid == null ,说明我没有关注对方
                $('.att_btn').attr({id:"attention", value:"关注"});
            } else {
                $('.att_btn').attr({id:"del_attention", value:"取消关注"});
            }

            if (uid_info.sex == 1) {
                var b_color = '#78BBE6';
            } else if (uid_info.sex == 2) {
                var b_color = 'pink';
            }

            userInfoBox.css({'border-color':b_color});
            userInfoBox.find('.blue').css({'color':b_color});
            $('.cor-top1').css('border-bottom-color',b_color);

        }
        //放用户信息的div , 前一个元素是img放着wait图片, 他们是同辈元素
        $('.userInfo').show();
        $('#wait').hide();  //wait图片
    }

/////////////用户信息弹框end////////////



    //这里先判断是否登录
    var h_uid = $('#uid').val();
    //加关注 attention
    $('#attention,.attention').live('click',function () {

        if (h_uid == '') {
            alert('请先登录！');

        } else {
            //var att_uid = $(this).next().val();
            var me = $(this);
            var att_uid = me.attr('rel');
            var user_info = $('body').data(att_uid);
            //alert(user_info.att_uid);

            if (typeof(user_info) == 'undefined' || user_info.att_uid == null || typeof(user_info.att_uid) == 'undefined') {

                global.ajaxget('attention',{'uid':att_uid},function(is_attention) {
                    if (is_attention == 1) {

                        if (typeof(user_info) == 'undefined') {
                            user_info = {};
                        }
                        user_info.att_uid = '1';
                        $('body').data(att_uid, user_info);

                        me.html('取消关注');
                        me.attr('id','del_attention');

                        //alert('关注成功！');

                    } else if (is_attention == 2) {
                        alert('不能关注自己！');

                    } else if (is_attention == 0) {
                        alert('你未登录！');

                    }

                });

            } else {
                alert('您已经关注他！');
            }
        }
    });
    //加关注 attention end

    //取消关注
    $('#del_attention,.del_attention').live('click',function () {

        if (h_uid == '') {
            alert('请先登录！！！');

        } else {
            //var att_uid = $(this).next().val();
            var me = $(this);
            var att_uid = me.attr('rel');
            var user_info = $('body').data(att_uid);    //取缓存
            //alert(user_info.att_uid);

            if (typeof (user_info) == 'undefined' || user_info.att_uid != null) {

                global.ajaxget('delattention',{'uid':att_uid},function(is_attention) {
                    if (is_attention) {

                        if (typeof (user_info) == 'undefined') {
                            user_info = {};
                        }
                        user_info.att_uid = null;
                        $('body').data(att_uid, user_info); //存缓存

                        me.html('+关注');
                        me.attr('id','attention');

                        //alert('取消关注成功！');
                    }
                });

            } else if (user_info.att_uid == null) {
                alert('您没有关注他！');

            }
        }
    });
    //取消关注 end

});
