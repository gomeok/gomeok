<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');

/**
 * 发布商品，晒或，秀搭配类
 * 1，商品分好多商城，在这里做分发，判读是那个商城的
 * 2，晒或
 * 3，搭配
 * question：
 *   1，taobaoapi类引入并且实例化后，model不能载入，原因：引入了自己的子类
 */
class GoodsAction extends Common {
     private $lg_goods = 'goods';

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
        if(empty($this->user_info['a_id'])) {
//            die('login common!');
//            redirect('admin.php/login', 'location');die;
        }
    }




	/**
	 * 后台商品列表
	 *
     */
	public function selGoodsList($g_show='-1') {
		$this->load->model('AGoodsModel');

        //$type = $_REQUEST['type']?$_REQUEST['type']:'-1';
        $type='-1';
        //商品总数
        $goods_total = $this->AGoodsModel->totalGoods($g_show,$type);

        //偏移量    没有时返回false
        $page = $this->uri->segment(2);
        if(empty($page)) {
            $page = 0;
        }//var_dump($page);die;
        $num = 60;
		$goodslist = $this->AGoodsModel->selGoodsList($g_show,$type,$page,$num);
        //var_dump($goodslist);die;
        //分页start 每页60条
        $p = $this->pages('admin.php/goodslist/', $goods_total, '60');
        //分页end

        //将偏移量抛到页面，供ajax加载数据使用
        $this->smart->assign('page_offset',$page+60);
        $this->smart->assign('page_str',$p);
        $this->smart->assign('goods_type',$type);

        $this->smart->assign('goodslist',$goodslist);
        $this->smart->display('goodslist.tpl');
	}

	/**
	 * 后台商品列表
	 *
     */
	public function goodsList($g_show='-1') {

        if($this->input->is_ajax_request()) {

            $this->load->model('AGoodsModel');

            //$type = $_REQUEST['type']?$_REQUEST['type']:'-1';
            $type='-1';
            //商品总数
            $goods_total = $this->AGoodsModel->totalGoods($g_show,$type);

            //偏移量    没有时返回false
            /**/
//            $page = $this->uri->segment(2);
//            if(empty($page)) {
                $page = 0;
//            }
            $num = 10;
            $goodslist = $this->AGoodsModel->selGoodsList($g_show,$type,$page,$num);

            $goodsData = array(
                'data'=>$goodslist,
                'total'=>$goods_total,
                'message'=>'ok',
                'success'=>'1'
            );

            echo json_encode($goodsData);die;

            //分页start 每页60条
            //$p = $this->pages('admin.php/goodslist/', $goods_total, '60');
            //分页end

            //将偏移量抛到页面，供ajax加载数据使用
            /*
            $this->smart->assign('page_offset',$page+60);
            $this->smart->assign('page_str',$p);
            $this->smart->assign('goods_type',$type);

            $this->smart->assign('goodslist',$goodslist);
            */
        }

        $this->smart->display('gl.tpl');
	}

}
?>