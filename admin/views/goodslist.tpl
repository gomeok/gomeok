{!config_load file="web.inc.conf"!}
{!include file="inc/header.tpl"!}

<link href="{!#acss_path#!}common.css" rel="stylesheet" type="text/css" />
<link href="{!#acss_path#!}skin.css" rel="stylesheet" type="text/css" />

<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #EEF2FB;
}
</style>
<body>
<table class="table">
	<tr class="title">
		<th width="6%">商品id</th>
		<th>商品标题</th>
		<th>商品类型</th>
		<th>商品价格</th>
		<th>添加时间</th>
		<th>状态</th>
		<th>标签</th>
		<th>操作</th>
	</tr>
	{!foreach from=$goodslist item=item!}
	<tr class="main_tab">
		<td>{!$item.g_id!}</td>
		<td>{!$item.g_title!}</td>
		<td>{!$item.g_type!}</td>
		<td>{!$item.price!}</td>
		<td>{!$item.g_addtime!}</td>
		<td>
		<select>
			<option value="0" {!if $item.g_show==0!}selected{!/if!}>未通过</option>
			<option value="1" {!if $item.g_show==1!}selected{!/if!}>通过</option>
			<option value="2" {!if $item.g_show==2!}selected{!/if!}>抓取</option>
			<option value="3" {!if $item.g_show==3!}selected{!/if!}>用户</option>
		</select>
		</td>
		<td>{!$item.g_label!}</td>
		<td>修改|删除</td>
	</tr>
	{!/foreach!}
	
	<tr>
	<td class="page-list" colspan="8">
	{!$page_str!}
	</td>
	</tr>
	
<input type="hidden" value="{!$goods_type!}" id="goods_type"/>
<input type="hidden" value="{!$page_offset!}" id="page_offset"/>
</table>
</body>
