
    var flag;
    //验证规则
    var	rule = {
        //正则规则
        "eng" : /^[A-Za-z]+$/,
        "chn" :/^[\u0391-\uFFE5]+$/,
        "email" : /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/,
        "url" : /^http[s]?:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/,
        "currency" : /^\d+(\.\d+)?$/,
        "number" : /^\d+$/,
        "int" : /^[0-9]{1,30}$/,
        "double" : /^[-\+]?\d+(\.\d+)?$/,
        "username" : /^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){4,19}$/,
        "password" : /^(\w){6,20}$/,
        "safe" : />|<|,|\[|\]|\{|\}|\?|\/|\+|=|\||\'|\\|\"|:|;|\~|\!|\@|\#|\*|\$|\%|\^|\&|\(|\)|`/i,
        "dbc" : /[ａ-ｚＡ-Ｚ０-９！＠＃￥％＾＆＊（）＿＋｛｝［］｜：＂＇；．，／？＜＞｀～　]/,
        "qq" : /[1-9][0-9]{4,}/,
        "date" : /^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$/,
        "year" : /^(19|20)[0-9]{2}$/,
        "month" : /^(0?[1-9]|1[0-2])$/,
        "day" : /^((0?[1-9])|((1|2)[0-9])|30|31)$/,
        "hour" : /^((0?[1-9])|((1|2)[0-3]))$/,
        "minute" : /^((0?[1-9])|((1|5)[0-9]))$/,
        "second" : /^((0?[1-9])|((1|5)[0-9]))$/,
        "mobile" : /^(13[0-9]|15[0-9]|18[0-9])\d{8}$/,
        "phone" : /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/,
        "zipcode" : /^[1-9]\d{5}$/,
        "idcard" : /^((1[1-5])|(2[1-3])|(3[1-7])|(4[1-6])|(5[0-4])|(6[1-5])|71|(8[12])|91)\d{4}((19\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(19\d{2}(0[13578]|1[02])31)|(19\d{2}02(0[1-9]|1\d|2[0-8]))|(19([13579][26]|[2468][048]|0[48])0229))\d{3}(\d|X|x)?$/,
        "ip" : /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/,
        "file": /^[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/,
        "image" : /.+\.(jpg|gif|png|bmp)$/i,
        "word" : /.+\.(doc|rtf|pdf)$/i

    }

    function ckform(item1, item2) {
        $.each(item2 , function() {
            var field = $("[name = '"+this.name+"']");  //jquery获取得对象
            if(field.is(":hidden")) return;
            var obj = this;     //当前的对，就是item2中的一个数组元素，是一个json对象
            var tocheck = function() { return ck_from(obj , field);};

            if (typeof(this.sp) != 'undefined') {   //存在sp，就用下面的样式
                field.after('<font class="cor"><font></font><span class="cor1"><span class="cor2"></span></span></font>');
            } else {
                if (!field.next().is('font')) {
                    field.after('<font></font>');
                }
            }

            if(field.is(':file') || field.is('select') || field.is(':radio') || field.is(':checkbox') ) {
                field.bind('change', tocheck);

            } else {
                //这种是text, password == 的情况
            	field.bind('blur', tocheck);
            }

            //submit    绑定提交
            $('#'+item1).submit(tocheck);

            /****************************完成默认提示文字显示*******************************/
            //字符串形式的代码
            var dai = "if (obj.defInput) {field.bind('focus', function () {return showInput(field, obj.defInput, false);});field.bind('blur', function () {return showInput(field, obj.defInput, true);});}";

            //如果存在默认提示的文字，就绑定一个聚焦事件
            if (typeof (obj.defMsg) != 'undefined') {
                field.bind('focus', function () {

                    //默认提示文字，只显示一次
                    var d = $('body').data(obj.defMsg);
                    if (typeof(d) == 'undefined') {
                        message(field, obj.defMsg, true, obj.sp);
                    }
                    $('body').data(obj.defMsg,'1');

                });
            }

            //绑定默认显示文字      //todo: 将代码做为字符串，然后然字符串可执行， 这样行吗？？
            eval(dai);
            /***************************完成默认提示文字显示end********************************/
        });
    }

    //框内默认显示的文字
    function showInput(obj, defInput, isShow) {
        //为空 或者 等于默认显示文字是执行
        if (obj.val() == '' || obj.val() == defInput) {
            obj.val(isShow?defInput:'');
        }
        obj.css('color',isShow?'#ccc':'black');
    }

    function ck_from(obj , jobj) {
        var w = obj.msg;
        var min = obj.min;
        var max = obj.max;
        var types = obj.type;
        var match = obj.to;
        var isajax = obj.isajax;
        var sp = obj.sp;

        var val = jobj.val();   //encodeURI($.trim(jobj.val()));            todo:中文才会别编码encodeURI
        var ok = 'OK';

        //修改加入全角
        //var len = ck_from_len(jobj.val());
		var len = jobj.val().length;

        //判断输入框中的文字是否为默认文字，如果是设为 ''
        if (val == '输入'+w) {
            val = '';
        }

        if (val == '') {
            if (jobj.is(':file') || jobj.is('select') || jobj.is(':radio') || jobj.is(':checkbox')) {
                return message(jobj , w+'没有选择！' , false , sp);
            } else {
                return message(jobj , w+'不能为空！' , false, sp);
            }

        } else if (typeof(min) != 'undefined' && typeof(max) != 'undefined' && len<min || len>max) {
            if (min == max) {
                return message(jobj , w+'的长度应该等于'+min , false , sp);
            } else {
                return message(jobj , w+'长度应在'+min+'-'+max+'位之间' , false , sp);
            }

        } else if (typeof(match) != "undefined" && match) {
            if ($("[name = '"+match+"']").val() != val) {
                return message(jobj , '密码输入不一致！' , false , sp);
            } else {
                return message(jobj , ok , true , sp);
            }

        } else if (typeof(types) != 'undefined' && types) {

            if (rule[types].test(val)) {
                flag = message(jobj , ok , true , sp);
            } else {
                var msg = '格式不正确';
                if (types == 'number') {
                    msg = '必须是数字';
                }
                return message(jobj , w+msg, false , sp);
            }

        } else if (typeof(isajax) == 'undefined' && !isajax) {
            return message(jobj , ok , true , sp);
        }

        //ajax验证
        if (typeof(isajax) != 'undefined' && isajax) {

            $.get(isajax+val,{},function(msg) {
                    if (msg == 0) {
                        flag = message(jobj , w+'已经被注册！' , false , sp);
                    } else {
                        flag = message(jobj , ok , true , sp);
                    }
                }
            );
        }
        return flag;
    }

    //
    /*
    function ck_from_len(val) {
    	var len = 0;
		for (var i = 0; i < val.length; i++) {
            if (val.charAt(i).match(/[^\x00-\xff]/ig) != null) //全角
            len += 2;
            else
            len += 1;
		}
		return len;
    }*/

    //提示信息
    function message(obj , msg , rs , sp) {
        //获取当前对象的位置
        var pos = obj.offset();

		//var pattern =/year|month|day/i;
        if (typeof(sp)!='undefined' && sp) {
            var sp = $('.'+sp);
            //obj.next().css({'top':pos.top-135});    //,'right':pos.left-192
            var obj = obj.next().show().children('font').html(msg);
            return colors(obj , rs);

            //return colors(sp.html(msg) , rs);

        }/* else if (pattern.test(obj.attr('name'))) { 			//判断是否是出生日期    //todo:为什么要这么做？？（生日的提示信息用特殊的id框?）

			var na = obj.attr('name');
			var obj = obj.siblings('#msg'+na).show().html(msg);
            return colors(obj , rs);
		}*/ else {

            //obj.next().css({'top':pos.top-135});    //,'right':pos.left-192
            var obj = obj.next('font').html(msg);

            //var obj = obj.next().show().children('font').html(msg);
            return colors(obj , rs);
        }
    }

    //改变字体颜色
    function colors(obj , rs) {
        (rs)?obj.css('color','gray'):obj.css('color','#f935b6');
        return rs;
    }