<?php
require_once('CommonCrud.php');
/**
 * 标签模型
 * @author  michael
 */
class LabelModel extends CommonCrud {
    private $lg_user = 'labels';


    public function __construct() {
        parent::__construct();
    }

    /**
     * 注册检查邮箱和手机号是否已被使用
     * @author  michael
	 * @param   array
	 * @param   array
	 * @return	array
     */
    public function regCheck($where1, $where2) {
        $this->db->where($where1);
        $this->db->or_where($where2);
        $rs = $this->db->get($this->lg_user);
        return $rs->result_array();
    }

    /**
     * 用户注册
     * @author  michael
	 * @param	array
	 * @param 	array
	 * @return	array
     */
    public function reg($where1, $where2) {
        /*
        $this->db->where($where1);
        $this->db->or_where($where2);
        $rs = $this->db->get($this->lg_user);
        return $rs->result_array();*/
    }



}
?>