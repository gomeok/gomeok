$(function(){

    $('#upload').click(function() {
        $('#upload_file').click();
    });

    $('#upload_file').change(function() {
        $('#uphead').submit();
    });

      // Create variables (in this scope) to hold the API and image size
      var jcrop_api, boundx, boundy;

      $('#target').Jcrop({
        boxWidth: 280,
    	boxHeight: 280,
        onChange: updatePreview,
        onSelect: updatePreview,
        minSize: [20,20],
        setSelect: [0, 0, 200, 200],
        aspectRatio: 1
      },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        $('#width').val(boundx);
        $('#height').val(boundy);
        // Store the API in the jcrop_api variable
        jcrop_api = this;
      });

      function updatePreview(c)
      {
        if (parseInt(c.w) > 0)
        {
          var rx = 200 / c.w;
          var ry = 200 / c.h;

          var w = Math.round(rx * boundx);
          var h = Math.round(ry * boundy);
          var mleft = Math.round(rx * c.x);
          var mtop = Math.round(ry * c.y);

          $('#preview').css({
            width: w + 'px',
            height: h + 'px',
            marginLeft: '-' + mleft + 'px',
            marginTop: '-' + mtop + 'px'
          });

          $('#w').val(c.w);
          $('#h').val(c.h);
          $('#mleft').val(mleft);
          $('#mtop').val(mtop);
        }
      };

});