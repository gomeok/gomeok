<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends CI_Controller {
	/**
	 * 公共属性
     */
//    public $weburl = 'http://lovego.sinaapp.com/';
    public $user_info;
    public $redis;

	/**
	 * 公共控制器的构造函数  common __construct
	 *
     */
    public function __construct() {
        header("Content-type:text/html;charset=utf-8");
        parent::__construct();
        //基本的用户信息从session中取出来存到了user_info中，下面所有的子类都用user_info取用户信息

        $this->user_info = $this->ksession->getAll();
        if(!isset($this->user_info['uid'])) {
            $this->user_info['uid'] = '';
            $this->user_info['nick'] = '';
            $this->user_info['sex'] = '';
            $this->user_info['head_img'] = '';
        }

        //$s = new SaeStorage();
        //$head_img = $s->getUrl( 'lovegoimg' , 'i.jpg' );

        $img = 'http://lovego-lovegoimg.stor.sinaapp.com/i.jpg';
        $head_img = empty($this->user_info['head_img'])?$img:$this->user_info['head_img'];
        $this->smart->assign('head_img',$head_img);

        /*//将用户的nick，sex，跑到页面，js全局使用
        if($this->user_info) {
            $this->smart->assign('g_nick',$this->user_info);
            $this->smart->assign('g_sex',$this->user_info);
        }*/

        if(isset($this->user_info['sex']) && $this->user_info['sex'] == 2) {
            $color = '#FC6B96';
        } else {
            $color = '#78BBE6';
        }
        $this->smart->assign('color',$color);

        //载入redis
        $this->load->library('MyRedis');
        $this->redis = $this->myredis;
    }

	/**
	 * Index Page for this controller.
	 *
     */
	public function index() {
        echo('!');
	}

	/**
	 * 判读用户是否登录
     * @author  michael
     *
     */
	public function isLogin($return=false) {
        if(empty($this->user_info['uid'])) {
            if($return == true) {
                return 0;
            } else {
                redirect('login', 'location');die;
            }
        }
        return true;
	}

	/**
	 * 商品图片缩略以及存贮
	 * sae
     */
    public function resizeGoodsImg($img_path, $datetime, $extfilename='', $params='') {
        //存图片  SaeStorage
        $s = new SaeStorage();
/*
        if(!empty($params) && is_array($params)) {
            $fdir = $this->makeUserDir($this->user_info['uid'],'','','1');
            $tmp_img = $fdir.$this->user_info['uid'].'/tmp_img/'.md5($date).'.jpg';
            $rs = $s->upload('lovegoimg',$tmp_img,$params['tmp_file']);
            $tmp_url = $s->getUrl( 'lovegoimg' , $tmp_img );

            $img_path = $tmp_url;
            $uid = $this->user_info['uid'];
            $datetime = $params['date'];
            $extfilename = $params['ext'];
        }*/

        $f = new SaeFetchurl();
        $img_data = $f->fetch($img_path);
        $img = new SaeImage();
        $img->setData( $img_data );

        //生成图片路径+名称
        $user_dir = $this->makeUserDir($this->user_info['uid'], $datetime, $extfilename, false);

        $img->resize(320); // 等比缩放到320宽
        $img_b = $img->exec(); // 执行处理并返回处理后的二进制数据
        $filename_b = $user_dir.'_b.jpg';     //320*……
        $s->write('lovegoimg' , $filename_b , $img_b);  //320*……的存了

        $img->resize(200); // 等比缩放到200宽
        $img_m = $img->exec(); // 执行处理并返回处理后的二进制数据
        $filename_m = $user_dir.'_m.jpg';      //200*……
        $s->write('lovegoimg' , $filename_m , $img_m);  //200*……的存了

        $img->resize(100,100); // 等比缩放到100宽
        $img_s = $img->exec(); // 执行处理并返回处理后的二进制数据
        $filename_s = $user_dir.'_s.jpg';     //100*100
        $s->write('lovegoimg' , $filename_s , $img_s);  //将100*100的存了

        return $s->getUrl( 'lovegoimg' , $filename_s );
    }

	/**
	 * 将用户散列到不同的目录中
	 *
     */
    public function makeUserDir($uid, $datetime='', $extfilename='', $type=true) {
        /*散列目录/uid/year/day/XXX.jpg*/
//        $uid = strval($uid);
//        $user_dir = $uid{'0'};
//        $user_dir .= substr(md5(substr($uid, strlen($uid)-2, 2)), 0, 6).'/'.$uid.'/';

        //返回uid的hash目录
        $user_dir = $this->makeHashDir($uid);
        $user_dir .= $uid.'/';

        if($type) {
            return $user_dir;die;
        }

        $year = substr($datetime, 0, 4);
        $day = substr($datetime, 0, 10);
        $file = md5($uid.$datetime);

        $user_dir .= $year.'/'.$day.'/';
        //这个是需要特殊返回的值，在获取商品信息时用了
        $imgDir = $user_dir.$file;

        if(!empty($extfilename)) {
            $file .= $extfilename;
        }
        $user_dir .= $file;

        return array(
                'big'=>$user_dir.'_b.jpg',
                'middle'=>$user_dir.'_m.jpg',
                'small'=>$user_dir.'_s.jpg',
                'imgdir'=>$imgDir
                );
    }

	/**
	 * 创建散列目录
	 *
     */
    public function makeHashDir($var) {
        $var = strval($var);
        $dir = $var{'0'};
        $dir .= substr(md5(substr($var, strlen($var)-2, 2)), 0, 6).'/';
        return $dir;
    }

  	/**
	 * 分页 程序中的所有的分页都是传的查询偏移量
	 * @params  $uri            分页路径
     * @params  $total          数据总条数
	 * @params  $per_page       每页显示条数
	 * @params  $num_links      前后显示连接数
     * @params  $usePageType    如果是int数字，使用uri路由段。 如果是bool false ，使用传统&取地址
     * @return  string          分页字符串
     */
    public function pages($uri, $total, $per_page, $usePageType=3, $usePageNumbers=false) {
        $this->load->library('pagination');

        $config['base_url'] = base_url().$uri;
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
//        $config['num_links'] = 5;
        //true=传页码 false=传偏移量
        $config['use_page_numbers'] = $usePageNumbers;

        if(is_int($usePageType)) {
            //分页所属uri段
            $config['uri_segment'] = $usePageType;

        } else {
            $config['page_query_string'] = true;
            $config['query_string_segment'] = 'page';
        }

        //$config['enable_query_strings'] = true;

        $config['prev_link'] = '上一页';
        $config['next_link'] = '下一页';

        $config['first_link'] = '第一页';
        $config['last_link'] = '末页';

        //自定义“当前页”链接1
        $config['cur_tag_open'] = '<span class="bluebg">';
        $config['cur_tag_close'] = '</span>';

        $this->pagination->initialize($config);

        return $this->pagination->create_links();
    }

  	/**
	 * 匹配表情, 文字<=>互相转换
	 *
     *
     */
    public function getSmileys($c_content,$type=false) {
        //加载表情数组
        $this->config->load('smileys');
        $smileys_config = $this->config->item('smileys');

        if($type) {
            foreach($smileys_config as $key => $value) {
                $sm = '<img src="/public/smilies/smileys/'.$value['0'].'">';

                $i = strpos($c_content,$sm);
                if(is_int($i)) {
                    $c_content = str_replace($sm, $key, $c_content);
                }
            }

        } else {
            foreach($smileys_config as $key => $value) {
                $i = strpos($c_content,$key);
                if(is_int($i)) {
                    $sm = '<img src="/public/smilies/smileys/'.$value['0'].'">';  //'.base_url().'
                    $c_content = str_replace($key, $sm, $c_content);
                }
            }
        }

        return $c_content;
    }


	/**
	 * 生成表情页面
	 *
     */
	public function makeSmileys() {
        //加载表情数组
        $this->config->load('smileys');
        $smileys_config = $this->config->item('smileys');

        $this->smart->assign('smileys',$smileys_config);
        //$this->smart->display('inc/smileys.tpl');
	}

    /**
	 * 图片上传
     *
	 * @author  michael
     */
    public function uploadImg($path, $fileName, $maxSize='', $maxWidth='', $maxHeight='') {

        if(!file_exists($path)) {
            if(!$this->mkManyDir($path)) {
                return '目录创建失败';
            }
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = empty($fileName)?time():$fileName;

        $config['max_size'] = $maxSize;
        $config['max_width'] = $maxWidth;
        $config['max_height'] = $maxHeight;

        $this->load->library('upload', $config);

        //todo:上传完一张，重新上传另一张时
        if($this->upload->do_upload()) {
            $imgByUid = $config['upload_path'].'/'.$config['file_name'];

            //图片上传成功之后返回的图片属性
            $imgAttr = $this->upload->data();

            $imgPath = $this->config->item('base_url').strstr($imgAttr['full_path'], 'upload');

            //在这里判断是否之前有以用户uid命名的图片，有的话删除，然后将刚刚上传的图片名称改为以用户
            if($imgAttr['file_name'] != $config['file_name'] && file_exists($imgByUid)) {
                //删除原有的图片
                unlink($imgByUid);

                //重命名图片            //todo:浏览器有缓存，上传的图成功了，但是还是显示上一张。因为前后两张的名字是一样的
                if(rename($imgAttr['full_path'], $imgByUid)) {
                    $imgPath = $this->config->item('base_url').$imgByUid;
                }
            }
            return $imgPath;

        } else {
            return array('error'=>$this->upload->display_errors());//'上传失败';
        }
    }

    /**
	 * 图片处理
     *
	 * @author  michael
     */
    public function imgDealWith($type, $sourceImage, $newImage, $maintainRatio=true, $width=null, $height=null, $x=null, $y=null) {
        $path = dirname($newImage);
        if(!file_exists($path)) {
            if(!$this->mkManyDir($path)) {
                return '目录创建失败';
            }
        }

        $config['image_library'] = 'gd2';
        $config['source_image'] = $sourceImage;
        $config['new_image'] = $newImage;
        $config['create_thumb'] = false; //产生预览图像
        $config['maintain_ratio'] = $maintainRatio;

        $config['width'] = $width;
        $config['height'] = $height;
        $config['x_axis'] = $x;
        $config['y_axis'] = $y;

        static $instance='';
        if(empty($instance)) {      //todo:用单例，载入一次有可能实例化一次
            $instance = 1;
            $this->load->library('image_lib');
        }

        $this->image_lib->clear();
        $this->image_lib->initialize($config);

        switch($type) {
            case 'resize':
                if($this->image_lib->resize()) {
                    return true;
                }
                break;
            case 'crop':
                if($this->image_lib->crop()) {
                    return true;
                }
                break;
        }
        return array('error'=>$this->image_lib->display_errors());
    }

	/**
	 * 获取用户所有关注的人的uid，存到redis
	 * 先取缓存，没有就查数据库，然后存入redis
	 * @author  michael
     */
    public function redisAttention($uid) {
        if(!empty($uid)) {
            $attentionKey = $uid.'_attention';
            $attentioners = $this->redis->get($attentionKey);

            if(empty($attentioners)) {
                $this->load->model('UserInfoModel');
                $attentioners = $this->UserInfoModel->getAttentioners($uid);
                $this->redis->set($attentionKey, $attentioners);
            }
            return $attentioners;
        } else {
            return array();
        }
    }

    /**
	 * 创建多层目录
     *
	 * @author  michael
     */
    public function mkManyDir($path) {
        $dirArr = array_filter(explode('/',$path));
        $p = '';
        foreach ($dirArr as $dir) {
            $p .= $dir.'/';
            if(!file_exists($p)) {
                mkdir($p);      //todo:这里创建目录时没有设置权限，在服务器上会不会出问题？？
            }
        }
        return file_exists($path)?true:false;
    }








  	/**
	 * 获取本地sae测试随机商品图片
	 *
     *
     */
    public function getTestImg() {
        $rand = rand(0,36);
        return base_url().'/public/images/testimg/'.$rand.'.jpg';
    }

  	/**
	 * 获取用户信息  暂时不用，因为可能多实例化一个model
	 *
     *

    public function selUserInfo($heUid) {

        $this->load->model('UserInfoModel');
		$userInfo = $this->UserInfoModel->getUserInfo($heUid, $this->user_info['uid']);
        echo '<pre>';var_dump($userInfo);die;
    }     */

}
?>