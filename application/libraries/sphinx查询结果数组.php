<?php
/**
 * Editplus4PHP PHP Template
 *
 * This is a Template of PHP Script for Editplus4PHP.
 *
 * @copyright HentStudio (c)2008
 * @author Leo <Zergdo@gmail.com>
 * @package Editplus4PHP
 * @version $Id: template.php, v 0.0.1 2008/09/21 12:16:23 uw Exp $
 */

array(10) {
  ["error"]=>
  string(0) ""
  ["warning"]=>
  string(0) ""
  ["status"]=>
  int(0)
  ["fields"]=>
  array(6) {
    [0]=>
    string(5) "goods"
    [1]=>
    string(5) "email"
    [2]=>
    string(4) "stat"
    [3]=>
    string(3) "ttt"
    [4]=>
    string(4) "ttt2"
    [5]=>
    string(4) "ttt3"
  }
  ["attrs"]=>
  array(1) {
    ["oid"]=>
    int(1)
  }
  ["matches"]=>
  array(10) {
    [1]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [2]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [4]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [5]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [6]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [7]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [8]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [9]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [13]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
    [14]=>
    array(2) {
      ["weight"]=>
      string(1) "1"
      ["attrs"]=>
      array(1) {
        ["oid"]=>
        string(10) "4294967295"
      }
    }
  }
  ["total"]=>
  string(2) "10"
  ["total_found"]=>
  string(2) "10"
  ["time"]=>
  string(5) "0.000"
  ["words"]=>
  array(1) {
    ["qq"]=>
    array(2) {
      ["docs"]=>
      string(2) "10"
      ["hits"]=>
      string(2) "10"
    }
  }
}