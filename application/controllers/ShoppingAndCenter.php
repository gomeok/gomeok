<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Common.php');
/**
 * shopping and center 的父类
 *
 */
class ShoppingAndCenter extends Common {
    //public $sm;

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 默认入口
     */
    public function index() {
        die('!');
    }

	/**
	 * 将商品数组，分为4个
	 * @params  $arr    需要分的数组
     * @params  $splitNum   要分为多少个
     */
	public function splitArr($arr, $splitNum=3) {
        $new_data = array();

        if(is_array($arr) && !empty($arr)) {
            //喜欢的商品
            $like = $this->redisLikeGoods($this->user_info['uid']);

            //将数组分为指定的个数
            $i = 0;
            foreach($arr as $key => $value) {
                //确定当前登录用户是否喜欢过商品
                $value['like'] = in_array($value['g_id'], $like)?'islike':'';
                //本地sae测试，随机获取图片
                $value['pic'] = $this->getTestImg();

                //将商品的发布者的头像获取
                $userDir = $this->makeUserDir($value['uid']);
                $value['head_img'] = $this->config->item('base_url').'upload/'.$userDir.$value['uid'].'_50.jpg';

                $new_data[$i][] = $value;
                $i==$splitNum?$i=0:$i++;
            }
        }
        return $new_data;
	}

	/**
	 * 获取用户所有喜欢的商品的gid 存到redis
	 * 先取缓存，没有就查数据库，然后存入redis
	 * @author  michael
     */
    public function redisLikeGoods($uid) {
        if(!empty($uid)) {
            $likeGoodsKey = $uid.'_likeGoodsGid';
            $likeGoodsGid = $this->redis->get($likeGoodsKey);

            if(empty($likeGoodsGid)) {
                $this->load->model('ShoppingModel');
                $likeGoodsGid = $this->ShoppingModel->getLikeGoods($uid);
                $this->redis->set($likeGoodsKey, $likeGoodsGid);
            }
            return $likeGoodsGid;
        } else {
            return array();
        }
    }

}
?>